%% This is file `raweb.cls',
%% generated with the docstrip utility.
%% If the file is called raweb-2005.cls, rename it to raweb.cls
%% Copyright (C) 1998-2001 INRIA/MIAOU
%% RCS original: Id: raweb.cls,v 1.9 2003/12/22 10:43:36 grimm Exp 
%% RCS $Id: raweb.cls.in,v 1.2 2005/09/07 09:26:27 bmarmol Exp $
%% 
\def\filedate{2005/07/29}
\def\fileversion{3,1}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{raweb}
       [\filedate\space v\fileversion\space
         Copyright INRIA/MIAOU/APICS 2001-2005]

\PassOptionsToClass{a4paper,11pt,twoside}{article}
\LoadClass{article}
%\RequirePackage{html}
\RequirePackage{url}
\RequirePackage[latin1]{inputenc}
\ifx\pdfoutput\undefined
\RequirePackage[T1]{fontenc}
\RequirePackage{graphicx}
\else
%%\RequirePackage[OT1]{fontenc}
\RequirePackage[pdftex]{graphicx}
\RequirePackage[pdftex]{hyperref}
\AtBeginDocument{\def\url{\begingroup \Url}}
\fi
\@ifpackageloaded{hyperref}{
\def\htmladdnormallink{%
   \bgroup\ifFrench\@DFP\fi
    \expandafter\egroup\ra@add}%
\def\ra@add#1#2{\href{#2}{#1}}
}{\global\let\addhyperlinkline=\relax}
\def\chapter{}%
%\RequirePackage[frenchb]{babel}
\def\numero{n\textsuperscript{o}}
\let\no\numero
\def\Numero{n\textsuperscript{o}}
\def\ieme{\textsuperscript{e}}
\let\@ra@url\url
\textwidth=15.65cm
\textheight=21cm
\addtolength\oddsidemargin{-0.8cm}
\addtolength\evensidemargin{-2.3cm}
\addtolength\oddsidemargin{2.5mm}
\let\RAignorename\@gobble
\newcommand\RAstartmodule[5]{%
   \ifx @#5@\else \subsection{#5}\fi}
\let\RAlabel\label
\begingroup
   \catcode`\!\active
   \uccode`\!=`\,%
\uppercase{\endgroup
 \def\ra@specialcomma{: %
    \raggedright
    \ignorespaces
    \catcode`\,\active
    \def!{\string, \ignorespaces}}}
\let\ra@partfont\bf
\def\participants@name{Participants}
\def\participant@name{Participant}
\newcommand\ra@finpart{\unskip.\par\nopagebreak\vspace{3mm}}
\newenvironment{participants}
    {\par{\ra@partfont \participants@name}\ra@specialcomma}{\ra@finpart}
\newenvironment{participantes}
    {\par{\ra@partfont \participants@name}\ra@specialcomma}{\ra@finpart}
\newenvironment{participante}
    {\par{\ra@partfont \participant@name}\ra@specialcomma}{\ra@finpart}
\newenvironment{participant}
    {\par{\ra@partfont \participant@name}\ra@specialcomma}{\ra@finpart}
\newenvironment{motscle}
    {\par{\bf Key words}\ra@specialcomma}{\ra@finpart}
\newcommand\pers[1]{#1
   \@ifnextchar[\pers@part\pers@nom}
\def\pers@part[#1]{#1
   \pers@nom}
\def\pers@nom#1{#1%
 \@ifnextchar[\space\relax}
\newcommand\ra@newpers[1]{\nopagebreak\item[]#1
   \@ifnextchar[\pers@part\pers@nom}
\newenvironment{catperso}[1]%
  {\let\pers\ra@newpers
   \begin{itemize}\pagebreak[2]\item[]{\hspace*{-1cm}\bf #1}}%
  {\end{itemize}}
\let\glossaire\relax
\newenvironment{glossaire}{\par {\bf Glossary:}\nopagebreak}{\par}
\newcommand\glo[2]{\par {\bf #1} #2\par}
\newenvironment{moreinfo}{\small\it}{}
\let\ps@plain\ps@empty
\let\abstract\relax
\let\endabstract\relax
%\newenvironment{abstract}{\quotation
%\noindent{\bf Abstract: \nopagebreak}\it}{\endquotation\par}
%\newenvironment{body}{\par}{\par}
\def\RAlefthead#1{\def\ra@lefthead{#1}}
\def\RArighthead#1{\def\ra@righthead{#1}}
\RAlefthead{Activity report INRIA 2005}
\RArighthead{ }

\def\choseRAsection#1{\ifcase#1\or
Team\or 
Overall Objectives\or
Scientific Foundations\or
Application Domains\or
Software\or
New Results\or
Contracts and Grants with Industry\or
Other Grants and Activities\or
Dissemination\or
Bibliography\else unknown section\fi}

\global\let\@mkboth\@gobbletwo
\def \RAbibXXname {Major publications by the team in recent years}
\def \RAbibSSname {Standards}
\def \RAbibAAname {Books and Monographs}
\def \RAbibCCname {Doctoral dissertations and ``Habilitation'' theses}
\def \RAbibDDname {Articles in referred journals and book chapters}
\def \RAbibEEname {Publications in Conferences and Workshops}
\def \RAbibHHname {Internal Reports}
\def \RAbibJJname {Miscellaneous}
\let\RAsc\textsc
\newcounter{myenumi} \setcounter{myenumi}{0}
\newcommand{\etalchar}[1]{$^{#1}$}
\let\endthebibliography\endlist
\def\thebibliography#1[#2]{
   \subsection*{#2}{\markboth{}{}}
   \list { {[\arabic{myenumi}]}}
        {\settowidth\labelwidth{[#1]}
          \leftmargin\labelwidth   \advance\leftmargin\labelsep
          \@nmbrlisttrue\def\@listctr{myenumi}}
   \def\newblock{\hskip .11em plus.33em   minus.07em}
  \sloppy\clubpenalty10000\widowpenalty10000 \sfcode`\.=1000\relax}
\def\Url@xxdo{% style assignments for tt fonts or T1 encoding
 \def\UrlBreaks{\do\.\do\@\do\\\do\/\do\!\do\_\do\|\do\;\do\>\do\]%
   \do\)\do\,\do\?\do\'\do\+\do\=\catcode`\%=14 }%
 \def\UrlBigBreaks{\do\:\do@url@hyp}%
 \def\UrlNoBreaks{\do\(\do\[\do\{\do\<}% (unnecessary)
 \def\UrlSpecials{\do\ {\ }}%
 \def\UrlOrds{\do\*\do\-\do\~}}% any ordinary characters that aren't usually
\def\url@xxstyle{%
 \def\UrlFont{\ttfamily}\Url@xxdo
}
\def\RAindex#1{\index{#1}#1}
\def\@lbibitem[#1]{\@bibitem}
\newcommand\moduleref{\@ifnextchar[\@xmoduleref\@moduleref}
\def\@xmoduleref[#1]#2#3#4{\def\@tempa{2005}\def\@tempb{#1}%
 \ifx\@tempb\@tempa \@moduleref{#2}{#3}{#4}%
 \else[projet~#2, ann^^e9e #1, section~#3\if @#4@\else, module~#4\fi]\fi}

\def\@tocrmarg{2.55em plus 1fil \relax}

\def\ra@place@inria{
   {%
   \parskip=0pt
   \@tempdima=5cm
   \advance \@tempdima -1in
   \advance\@tempdima -\topmargin
   \advance\@tempdima -\headheight
   \advance\@tempdima -\headsep
   \advance\@tempdima -\topskip
   \null \vskip\@tempdima
   \setbox0\hbox to 14cm{%
     \noindent\hfill
     {\tentm INSTITUT NATIONAL DE RECHERCHE
            EN INFORMATIQUE ET EN AUTOMATIQUE}%
      \hfill}%
   \@tempdima=1in\advance\@tempdima\evensidemargin
   \advance\@tempdima-3.5cm
   \noindent\kern-\@tempdima\hbox to0pt{\box0\hss}%
   }%
}
\newbox\@myatxybox%
\newif\if@myatxy\@myatxyfalse
\long\def\ra@atxy(#1,#2)#3{\global\setbox\@myatxybox=\hbox
 {\unhbox\@myatxybox
  \vtop to 0pt{\kern #2\hbox to 0pt{\kern #1\relax #3\hss}\vss}}%
 \global\@myatxytrue}
\def\@usemyatxy{\if@myatxy{%
  \if@twoside
    \ifodd\count\z@
         \let\@themargin\oddsidemargin
    \else \let\@themargin\evensidemargin
    \fi
  \fi
  \vtop to 0pt{\kern-\headsep \kern-\topmargin \kern-\headheight
               \kern-1in \kern-\voffset
    \hbox to 0pt{\kern-\@themargin \kern-1in \kern-\hoffset
\unhbox\@myatxybox \hss}\vss}}%
            \fi\global\@myatxyfalse}% --bg
\toks0={\setbox255=\vbox{\@usemyatxy \unvbox255}}
\output=\expandafter{\the\toks0\the\output}
\bibliographystyle{raweb}
\newcommand\@moduleref[3]{%
   \def\@tempa{#1}\ifx\@tempa\RAprojet
   \ref{#1@#2@#3}\else [projet~#1, section~#2\if @#3@\else, module~#3\fi]\fi}
\def\project@name{Project-Team}
\newcommand\RAstartprojet[5]{%
  \ifx0#2\gdef\project@name{Team}\fi
  \def\ra@righthead{ \project@name\ #3}%
  \footbibliography{\RAprojetlow_foot2005}%
  \ra@place@inria
\ifx\pdfoutput\undefined
  \ra@atxy(52mm,173mm){\includegraphics{LogoRA2005.eps}}%
  \ra@atxy(7.8cm,2.5cm){\includegraphics[width=5.7cm]{Logo-INRIA-couleur.epsi}}%
\else
  \ra@atxy(52mm,173mm){\includegraphics{LogoRA2005}}%
  \ra@atxy(7.8cm,2.5cm){\includegraphics[width=8.2cm]{Logo-INRIA-couleur}}%
\fi
   \ifx!\@thetheme!\else
     \ra@atxy(70mm,174mm) {\hbox to 72mm{%
       \hrulefill\tenhv \hspace{8mm}THEME \MakeUppercase{\@thetheme}%
             \hspace{8mm}\hrulefill}}\fi
  \thispagestyle{empty}%
  \null\vfil
  \vskip 60\p@
  \begin{center}%
    {\twentytb \@title \par}%
    \vskip 3em%
    {\large\proj@font
     \lineskip .75em%
        \@author\par}%
  \end{center}
  \vspace{5cm}
  \vfil\null
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
  \newpage
  \thispagestyle{empty}%
  \pagestyle{ra}%
  \hbox{}\newpage
  \setcounter{page}{1}%
}
\def\loadbiblio{%
    \let\url\@ra@url
%    \@input@{\jobname.refer.bbl}%
    \urlstyle{xx}}%
\def\ps@ra{%
     \def\@oddhead{\vbox{\hbox to \textwidth{%
          \normalsize\it{\ra@righthead}\hfil\rm\thepage}%
            \hbox{\rule[-1ex]{\textwidth}{.03cm}}}}%
     \def\@oddfoot{}%
     \def\@evenhead{\vbox{\hbox to \textwidth{%
            \rm\thepage\normalsize\it\hfil{\ra@lefthead}}%
          \hbox{\rule[-1ex]{\textwidth}{.03cm}}}}%
     \let\@evenfoot\@oddfoot}%
\def\mytableofcontents{{\raggedright\tableofcontents}\clearpage}
\newcommand\tentm{\fontencoding{T1}\fontfamily{ptm}\fontseries{m}%
   \fontshape{n}\fontsize{10pt}{12pt}\selectfont}
\newcommand\twelvetr{\fontencoding{T1}\fontfamily{ptm}\fontseries{m}%
   \fontshape{n}\fontsize{12pt}{14pt}\selectfont}
\newcommand\fifteentb{\fontencoding{T1}\fontfamily{ptm}\fontseries{b}%
   \fontshape{n}\fontsize{15pt}{20pt}\selectfont}
\newcommand\tenhv{\fontencoding{T1}\fontfamily{phv}\fontseries{m}%
   \fontshape{n}\fontsize{10pt}{12pt}\selectfont}
\newcommand\twentytb{\fontencoding{T1}\fontfamily{ptm}\fontseries{m}%
   \fontshape{it}\fontsize{25pt}{30pt}\selectfont}
\newcommand\proj@font{\fontencoding{T1}\fontfamily{ptm}\fontseries{m}%
   \fontshape{it}\fontsize{17.28pt}{25pt}\selectfont}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                 {-3.25ex\@plus -1ex \@minus -.2ex}%
                 {1.5ex \@plus .2ex}%
                 {\normalfont\large\bfseries\raggedright}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                 {-3.25ex\@plus -1ex \@minus -.2ex}%
                 {1.5ex \@plus .2ex}%
                 {\normalfont\normalsize\bfseries\raggedright}}
\def\aparaitre{to appear}
\let\toappear\aparaitre
\endinput

