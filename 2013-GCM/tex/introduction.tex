\section{Introduction}



Software components have been designed to provide composition
frameworks raising the level of abstraction compared to objects.
Components split the application programming into two phases: the
writing of basic business code, and the composition phase consisting
in plugging together the basic blocks programmed above. To scale
better and ease the programming of large applications, 
\emph{hierarchical} component models  allow each component to be 
composed of other components.  In the context of the CoreGrid European Network of
Excellence,  a component model called GCM~\cite{gcm} (Grid Component
Model) has been designed; GCM is an adaptation of Fractal~\cite{bruneton04fractal-spec} for large
scale distributed computing. It defines structurally the way to
compose components.
The GCM component model, its API, ADL and its deployment capacities
has been standardised by  ETSI~\cite{ETSI:GCM-DD-08-TR,ETSI:GCM-AD-08-TR,ETSI:GCM-ADL-09-TR,ETSI:GCM-API-10-TR}. 
However, getting a component-oriented programming model not only requires a specification, but
also a well-thought implementation: components behaviour  depends of their  underlying
semantics resulting from the chosen implementation technology. This paper adds an
original piece of work around the GCM  efforts by diving into that aspect 
given a specific implementation technology.

Indeed, one of the original aspects of this paper is that it focuses on the
interplay between the component model and the underlying programming
model. The distributed programming model we rely on is based on the
notion of active objects. 
Active object~\cite{CHS:POPL04} is a programming pattern adapted to distributed
computing.  The principle of active objects is very simple: an object
is said to be active if it has its own thread.   Every
call to an active object will be some form of remote method invocation;
we call such an invocation a \emph{request}. 
An active object handles invocations asynchronously: when a request is
received it is put in a queue, and will be served later by the active object.
One crucial point of active objects is that they are mono-threaded and
there is no shared memory between two active objects. Consequently,
active objects prevent data race-conditions without having to use locks or
synchronised blocks. 
The only race-condition that exists in this programming model is the
concurrent request invocations toward the same object; this is the
only concurrent aspect that exist in the model.
 An active object can act as an
access point to a set of objects but two different active objects
cannot share memory.  As a consequence, this programming model is particularly adapted
to develop distributed applications because each active object can be
deployed at a different location and is by construction loosely
coupled with the other active objects.

We identify three main contributions for this paper.
\subsection*{\textbf{Contribution 1}. Autonomous Components: A Programming Model for Distributed Components}

Most research contributions on component models focus on the component
structure~\cite{bruneton04fractal-spec,sca-spec,CCA}, its control and
evolution~\cite{Sofa2,Frascati,Fscript09}, or the
specification of adequate coordination/communication modes between
components,
 e.g.  parallel communications between components
 \cite{bertrand05MxN,GridCCM},  
 orchestration of services exposed
by components for SOA (Service Oriented Architecture)~\cite{Stkm}.
Other works in the component community aim to
dig into efficient ways to implement the component model in specific contexts
leveraging well chosen  supporting environment or middleware 
 for deploying and running the components. These later approaches are bottom-up: first, authors select the middleware they want to rest upon, then they design a software component model
in which they introduce features and related implementation in a way
that efficiently uses the desirable features from the underlying
middleware. Some of those
component models have been proposed for  distributed application
programming and execution, like for example: CCA and Assist component models on top of
web service based grid standards  \cite{CCA04,AssistWS}, PaCO and GridCCM on top of Corba
object request brokers \cite{ParCCM}, CCA on top of H2O  \cite{Mocca}.
% \TODO{FB: I tried to rewrite completely  , see partly above and below if it is OK now. LH: please write a
%    sentence from this (i failed): (e.g. in
% distributed computing,  web services-based grid standards \cite{CCA04,AssistWS} or other middleware such as Corba Object request brokers, \cite{ParCCM}, H2O \cite{Mocca}). Building
% such component-oriented frameworks require to decide how the component model implementation  complies to
% the features of the selected supporting middleware I am lost:
%   why does the component model has to comply to the fetures of the
%   middleware that was chosen to support it: if it is difficult then it
% means the middleware was bad chosen ... CLARIFY}.
The drawback of this approach  is that the features of the underlying middleware  impact too much the
resulting component model, its implementation, and its runtime behaviour,
leading to entangled, hard to maintain code.
We advocate another, more self-contained, approach. Given the features we
believe are needed at the component model level, we carefully devise a component model implementation
 independently of the underlying middleware  selected
 to effectively deploy and run the application. Instead, we underline
 the relation between the component model and its programming and
 execution model, i.e.,
 the behaviour of the component-based application. In other words, we  make explicit the resulting runtime behaviour of the
component model implementation without having to consider the effective supporting environment.
 To the best of our knowledge, the only examples of such kinds of discussion relating
a component model with the programming model chosen to implement it are around:
aspects \cite{Aokell},  mixins \cite{fractal}, and direct enhancements of
the core programming language as Java \cite{JavaA} to implement
component constructions. %Even  in those examples, the authors relate a
%program composition model with the implementation of components.
Consequently, \emph{we are among the first ones to study the relation
between a programming model for distributed computing and a component
model targeting distributed systems}. 
%\comment{thanks Francoise for the new version: much clear now!} 


% It is not common that  implementers of
% the component frameworks  discuss  the impact of the programming model should be
% chosen for implementing a given component model.
%  Rare examples of such discussions exist as e.g.  use of 

In this paper,  our goal is
to illustrate the benefit of chosing an adequate programming model,
based on our own experience implementing a 
sophisticated component model.
%, what can be the benefit of having chosen an adequate programming model. 
In particular, we rely on a very powerful programming model,
the active object model, to implement our component model. Also,
contrarily to the research contributions on component models  mentioned 
above, our programming model
is a runtime model, expressing how programs run, and not only how to
compose pieces of code. The study of the interplay between a component model used
to compose applications and a programming model specifying how
applications run is the main originality of our approach. 
%% On the contrary, the originality of this paper resides in 
%% the interaction between the programming model and the component model
%% we use. Indeed we rely on a strong programming model and a strongly
%% structured component model, and
We have put a particular effort to integrate our programming model and our
component model in a common structured programming environment: our
components are active objects, they communicate by asynchronous
requests, and each of them can be placed on a different location.  We
think that implementing the component model using this
programming model provides a good
%Our programming plus component model provides a good
\emph{abstraction for distribution and concurrency}, and we claim here
 that this new integrated model provides \emph{a more powerful development,
composition, and execution environment 
than other distributed component frameworks}.
%than other programming or component models}.

 

GCM/ProActive is the reference implementation of the GCM component
model. It is based on the ProActive Java library \cite{proactive} and
relies on the notion of active objects: each component corresponds at
runtime to an active object and consequently each component can easily
be deployed on a separate JVM and can be migrated. Of course, this
implementation relies on design and implementation choices relative
to the purely structural definition provided by the definition of GCM.
One of the main advantages of using active objects to implement
components is their adaptation to distribution.  Indeed, active
objects communicate by an asynchronous request/reply mechanism and
enforce that each execution thread is isolated in a single active
object, and thus a single component. Our programming model provides a
natural way to write \emph{loosely coupled components} that do not share
memory nor control, and can thus be considered as \emph{independent
entities}. We call those loosely coupled components \emph{autonomous components}.

This article also highlights the impact of the programming model as
it makes it easy to combine components  within a \emph{service oriented environment}, and to
 program  \emph{autonomic component
systems}.
\subsection*{\textbf{Contribution 2}. A Programming and Composition Model
  for Service Oriented Architectures}
Indeed, until only recently, component based software engineering
had not addressed a number of modern software development problems
such as interoperability and the diversity of platforms and protocols.  Service
Oriented Architectures (SOA) aim to cope with these limitations. SOA is
an architectural style where applications are conceived as a composition
of loosely coupled, coarse grained, technology agnostic, remotely
accessed, and interoperable 
services, bearing thus strong similarities with software
components. Adapting component models and corresponding platforms
in order to support SOA is thus very relevant for modern
software. A typical example of such a marriage of reason \cite{Collet}
has been provided by the SCA (Service Component
Architecture) \cite{sca-spec} specification supported by industry. 
%REMOVED : TOO RISKY!, and has to be considered, at least
%partly, by any "modern" component framework: e.g., 
Successful integration of SOA interactions into component
platforms have been recently illustrated by 
utility interfaces in the version 2 of the SOFA component model
\cite{Sofa2SOA}, and within our own GCM implementation (as detailed in Section~\ref{exp}).


\subsection*{\textbf{Contribution 3}. Autonomic Support for Autonomous Components}

Using components to compose applications constrains the programmer to identify the
dependencies between components statically. While this limits %a little
the programming model expressivity, it eases the design of large applicative
architectures, as it provides a static view of the code dependencies.  This
static view  is quite often too
restrictive, as application structure needs to evolve dynamically, in
particular when facing changes in the execution environment. Some
component models, including GCM, address these issues of
adaptiveness as a separated concern relative to the business
logic: the business code of the component does not deal with
the structural changes of the component application. Those structural
changes, called \emph{reconfiguration} of the component assembly, are
programmed separately and considered as non-functional concerns.
Both the decision making and the enactment of structural changes are
triggered at the non-functional level. This separation of concerns
increases the re-usability of the applicative code, and it also eases the
programming of large scale highly adaptive applications. 
Adaptation procedures can even become smart and generic enough to allow the
component system to take reconfiguration decisions in an autonomic
manner, where each
entity is able to adapt itself to changes in the runtime environment,
or in the desired service quality.

% Present the problematics that we want to solve with
% GCM/ProActive. explain GCM, ASP, ProActive

% Mention that components are an advantage from the software development point of view and that can be used to build adaptable software.

% Components are also a good way to compose asynchronous decoupled entities.

% Relate it with Active Objects / Actors. 

% \TODO{NEW} Advantage: the component structure is also the execution structure.

% Idea of decoupling components.

%This decoupling idea also helps in the design of autonomic behaviour.

In our approach, each component is both autonomous and
autonomic  in the following sense:
\begin{itemize}
  \item  
\emph{Each component runs autonomously.}
Components are self-contained entities with their own execution thread
and service policy.
The composition structure is also the execution structure; it also
defines the data distribution and the interacting entities. We will describe the
programming and execution model in Section~\ref{sec:programming}.
  \item 
\emph{Each component is able to evolve in an autonomic manner.}
This is due to the combination of two features we developed in
GCM/ProActive. First, each component can locally execute
reconfigurations; this will be described in Section~\ref{dynamic:reconf}.
Second, each component can take adaptation decisions locally, thanks
to an implementation of the autonomic MAPE model \cite{IBM:autonomic:2006}, see Section~\ref{dynamic:autonomic}.
Both features can be combined to design autonomic behaviour in which decisions
are taken in one component, and executed distributedly using uniform
interfaces. Compared to previous works~\cite{Aokell,david09fscript},
the combination of a programming model enforcing decoupling of
components and of a precisely defined component architecture for
autonomic aspects enables the programming of distributed adaptation
procedures, and eases the programming of  distributed systems.
\end{itemize}
Those two aspects compose well together: it is simpler to design
generic distributed (self-)adaptation procedures between loosely coupled
components.
% \TODO{do we want to speak somewhere about the problem of stopping components?}


This paper is organized as follows. Section~\ref{prog:compsota}
presents a state of the art about the distributed component models,
and the related works. Section~\ref{sec:programming} presents the
programming and execution model of
GCM/ProActive. Section~\ref{dynamic} describes adaptation and
dynamic aspects we developed for GCM components. 
Section~\ref{exp} describes the implementation of our 
component platform; it includes deployment aspects and integration with
SCA.  An autonomic
reconfiguration use-case is presented in Section~\ref{use}.
 Finally, after a comparison with the closest related works in
 Section~\ref{sec:comparison}, we conclude this article in Section~\ref{sec:conclusion}.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../spe2013"
%%% End: 
