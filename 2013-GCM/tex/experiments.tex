\section{Implementation}
\label{exp}
%\TODO{rewrite the beginning here once the section Usecase gets finalized}
The purpose of this section is to discuss aspects related to the implementation of
GCM components.
The first part of this section focuses on the
implementation of GCM/ProActive and its autonomic capabilities.
% Then, we illustrate the practical effectiveness of our approach
% through a use-case that integrates the features presented
% in the previous sections.
%showing all the features presented in this paper, and focusing on
%component autonomic adaptation.
Then, we describe  the integration of
GCM/ProActive components into SCA assemblies. Our aim in this last part 
is to show that the GCM/ProActive framework is generic enough to integrate
smoothly with other component models like SCA, but also that our
component model is indeed adapted to the integration within a
service-oriented environment.


\subsection{GCM Implementation}
\label{exp:gcmimplem}
 GCM/ProActive is an implementation of GCM over the ProActive middleware.
ProActive is a Java middleware that provides an API to instantiate and
run active objects; it implements 
the mechanisms used by our asynchronous components, like request queues
and futures. Whereas the basic programming features like futures and
request service is provided transparently to the programmer,
additional non-functional features of active objects are accessible
through the ProActive API (for instance to migrate a component or to
check if a request has been served).
GCM components are implemented in ProActive using active objects in such a way that each component
is implemented by exactly one active object, and serves requests as described in Section~\ref{prog:gcm-requests}.

GCM/ProActive provides an API to instantiate components. Components are instantiated by defining
a {\em ComponentType} object that specifies the set of F and NF {\em InterfaceType} objects that will be exposed on the membrane of the component.
An {\em InterfaceType} object contains the attributes of a single component interface: name, signature (a Java Interface),
role (client or server), contingency (mandatory or optional),
and cardinality (singleton, multicast, gathercast).
An {\em Interface} in GCM/ProActive is defined by an {\em InterfaceType} and an actual implementation.
The implementation of an {\em Interface} is any object that provides the signature and may be initially
undefined and be assigned at runtime using the component binding mechanism ; in that case, its contingency
value must be ``optional''.

GCM/ProActive components may also be defined in a simpler way by using a GCM-ADL file that is compiled
by an ADL Factory, which instantiates the application.
A GCM-ADL file is an XML descriptor that extends Fractal ADL to allow
the definition of collective interfaces, and of NF components in the membrane.
The composition of the membrane follows the same rules as any other
composition, and may be defined either inside the {\tt <controller>} tags 
or in a separate GCM-ADL file specified as an attribute of the {\tt
<controller>} tag.

Figure \ref{dynamic:gcm-instantiation} shows a simple example of a GCM/ProActive application and a part of the instantiation process.
Figure \ref{dynamic:gcm-instantiation} (b) shows the instantiation of component
{\bf A} through the GCM API by defining their {\em InterfaceType} and {\em
ComponentType} objects, and then using the {\em MembraneController} to insert
component {\bf C} inside the membrane of {\bf A}.
Prior to initiate the lifecycle of {\bf A}, its membrane must be started.
Figure \ref{dynamic:gcm-instantiation} (c) shows the ADL for the instantiation of the entire
application of Figure~\ref{dynamic:gcm-instantiation}~(a); it consists
of
the main file {\em GCMApp.fractal}, and the additional file {\em
NFCompA.fractal} that defines  the membrane of component {\bf A}.
The architectural definition using the GCM-ADL allows for more concise
descriptions in terms of coding effort and is less error-prone as the ADL
Factory translates the descriptions to the appropriate GCM API invocations.
Also, the ADL description eliminates the
need to recompile the application code for creating a different component
assembly, thus facilitating its manipulation by external tools.
Being able to specify the F and NF parts using two
separate ADLs enforces the separation of concerns that we advocate
in the design of GCM applications; the two ADLs are merged  at instantiation
time.
Finally, Figure \ref{dynamic:gcm-instantiation} (d) shows the only two lines of
application code required to instantiate
the application, which is much shorter compared to Figure
\ref{dynamic:gcm-instantiation} (b).

\begin{figure}
\centering
\includegraphics[width=5.5in]{figs/gcm-instantiation2}
\caption{(a) A sample GCM application; (b) Partial instantiation of composite A
using the GCM-API; (c) Instantiation of the application using two GCM-ADL
files; (d) Instantiation  using GCM-ADL}
\label{dynamic:gcm-instantiation}
\end{figure}

Deployment of functional and non-functional components is handled by the ProActive
middleware. ProActive enforces a separation between the programming of
the distributed application and the actual deployment by using a
Virtual Node (VN) abstraction \cite{gcm}.  GCM components are
associated to a VN, either using the API or using the GCM-ADL file.
When actual deployment takes place, GCM/ProActive uses an underlying
mechanism that allows us to map the specified VNs to an actual
infrastructure. The mechanism to access the infrastructure ranges from
simple access to remote machines using {\em ssh}, to complex resources
managers and job schedulers. Once the remote resources are acquired, a
set of JVMs are instantiated on the VNs, according to the mapping
mentioned above. The active objects that implement the
GCM/ProActive components are hosted on these JVMs.
This separation between the application description, specified in terms of VNs, and the mapping to the actual infrastructure,
allows any ProActive based application to be deployed in a variety of
infrastructures like clusters, grids or clouds, without changing or recompiling
the application. 

\subsection{MAPE Framework in GCM/ProActive}
\label{exp:mapeimplem}

GCM/ProActive provides a basic implementation of the MAPE loop as described in 
Section~\ref{dynamic:autonomic}. This implementation provides an API for each phase of the MAPE loop  and a working implementation of one component for each phase. The insertion of components
in the membrane is done by a provided console application which is no more than a front-end that uses the NF API of GCM
to introspect the application and automatically insert or remove the MAPE components as needed.
The API for the MAPE loop is shown in Figure~\ref{dynamic:mape-api}.

\begin{figure}
\centering
\includegraphics[width=3in]{figs/mape-api}
\caption{Basic API for the MAPE components}
\label{dynamic:mape-api}
\end{figure}

The internal composition of the MAPE components in GCM/ProActive is shown in
Figure~\ref{dynamic:mape-internals}.
This set of predefined components that implement each phase is just one of
several possible implementations that illustrates well how to provide
adaptability.

\begin{figure}
\centering
\includegraphics[width=6in]{figs/mape-internals}
\caption{Internal composition of the MAPE components}
\label{dynamic:mape-internals}
\end{figure}

In this implementation, the {\em Monitoring} component receives a
Metric that is a Java object with a {\em compute} method, and inserts
it in the {\em Metric Store}.  The {\em Metric Store} provides the
connection to the sources necessary for the computation of the Metrics; namely, the {\em Record Store} to get already sensed
information, the {\em Event Listener} to receive sensed information
directly, or the {\em Monitoring} component of other components,
allowing access to the distributed set of monitors.  The {\em Event
  Listener} collects JMX events that are produced by the ProActive
middleware.  These events include the sending, starting, and finishing
time of requests, as well as the update of futures.  The {\em Record Store} stores records of
monitored data that can be used for later analysis.  To gather
external information, we have also designed a set of JMX probes for CPU
load and memory use, and incorporated them along with the events
produced by ProActive within the store.

%For example, a simple {\em respTime} metric to compute the response
%time of requests, requires to acess the {\em Record Store} for
%retrieving the events related to the start and finish times of the service of a
%request.

%Use the example . . .
%Consider, for instance, that the Tourism Service needs to know
%the decomposition of the time spent while serving a specific request $r_0$.
%For this, a metric called {\em requestPath} for a given request $r_0$ can ask
%the {\em requestPath} to the {\em Monitoring} components of all the services
%involved while serving $r_0$, which can repeat the process themselves; when no
%more calls are found, the composed path is returned with the value of
%the {\em respTime} metric for each one of the services involved in the path.
%Once the information is gathered in the {\em Monitoring} component of the
%Tourism Service, the complete path is built and it is possible to identify the
%time spent in each service.
%% Would like to make a figure of this, but it may become too long

The {\em Analyzer} component  queries the
{\em Monitoring} component. The {\em Analyzer} consists of 
(1) a {\em Rules Analyzer}, which transforms the Rule description to a common
internal representation, (2) a {\em Rule Store} that maintains the list of Rules,
(3) a {\em Rules Verifier} that collects the required information from the {\em
Monitoring} interface and generates alarms, and (4) a {\em Rules Manager} that
manages all the process.
A Rule is described as a triple 
$\langle \mathit{metricName}, \mathit{comparator},$ $\mathit{value} \rangle$,
where {\em metricName} is the name of a metric.
The Rule Verifier subscribes to the {\em metricName} from the
{\em Monitoring} component (either in a push or in a pull mode)
to get the updated values and check the compliance of the Rule.

%For example, the Tourism Service includes the SLO: ``All requests must
%be served in less than 30 secs'', described as 
%$\langle \mathit{respTime}, <, \mathit{30} \rangle$. The {\em SLA Manager}
%receives this description and sends a request to the {\em Monitoring} component
%for subscription to the {\em respTime} metric. The condition is then stored in
%the {\em SLO Store}. Each time an update on the metric is received,
%the {\em SLA Manager} checks all the SLOs associated to that metric. In case
%one of them is not fulfilled, a notification is sent, through the {\em alarm}
%interface including the description of the faulting SLO.


The {\em Planning} component includes a {\em Strategy Manager} that receives an
alarm message and, depending on the content of the alarm, triggers one
{\em Planner} component, among a set of bound \emph{Planners}.
Each one of the {\em Planner} components implements a planning algorithm that
can create a plan to modify the state of the application. Each {\em Planner}
component can access the {\em Monitoring} components to retrieve any additional
information they may need; the output is expressed as a list of actions in a
predefined language.

In our implementation we profit from the capability to dynamically select one of the 
bound interfaces of  a one-to-many (multicast) interface provided
by GCM to decide which {\em Planner} component will be triggered. For
example, if the rule violated is related to response time, we may trigger a
planner that generates a performance-oriented recomposition; or if a given cost
has been surpassed, we may trigger a cost-saving algorithm. The decision of what
planner to use is taken in the {\em Strategy Manager} component. Nevertheless, the
possibility of having multiple strategies might be a source for conflicting
decisions; while we do not provide a method to solve these kinds of conflicts, we
assume that the conflict resolution, if required, is provided by the
{\em Strategy Manager}.

As an example, we have implemented a simple planning strategy that, given a
particular request $r$, computes the {\em requestPath} of $r$,
i.e. the list of sub-requests that were called during the service of $r$
along with the time spent by each sub-request.
%contributes to the achievement of the main one 
% \TODO{LH to CR: check, requestpath was undefined. ; FB: WDYT if we put `` the flow of related requests that ...'', I think it defines what requestPath is. CR:checked}.
The strategy finds the component most likely responsible for
having broken the Rule that triggered the alarm; this can be done
examining the result of the {\em requestPath} computation and determining
the component $c$ that took the largest time serving the sub-requests.
% \TODO{LH to CR: can you give an idea of how you find
% this component ; FB: doing that you can also give as example what the SLO could be preciselyCR: added, but I avoid to talk about SLO which I had not mentioned before},
The strategy then creates a plan that  replaces $c$ for
another component from a set of possible candidates. More complex
strategies could be designed, based on additional information from the
application, the execution environment (e.g., available resources), etc.

% This could be replaced by an algorithm . . .
%Applied to the Tourism Service, suppose a request has violated the
%SLO $\langle \mathit{respTime}, <, \mathit{30} \rangle$.
%The {\em Strategy Manager} activates the {\em Planner} component that obtains
%the {\em requestPath} for that request along with the corresponding response
%time, selects the component that has taken the highest time, then obtains a
%set of possible replacements for that component, and obtains for each of them
%the {\em avgRespTime} metric. The output is a plan expressed in a
%predefined language that aims to replace the slowest component by the chosen
%one.

The {\em Execution} component receives actions from the {\em Planning}
component. It includes an {\em Execution Manager} that manages the
execution , and a {\em Reconfiguration Engine} that embeds a GCMScript
interpreter.  As reconfiguration actions triggered by the planners may
be expressed in different formats, the {\em Execution Manager} may use
a {\em Translation} component to translate actions before transferring
them to the {\em Reconfiguration engine}.
The {\em Execution Manager}
may also discriminate between the actions that can be executed by the local
component and those that must be delegated to external {\em Execution}
components.

%For example, if a planner determines that the {\em Weather} service must be
%removed from the composition, it can be unbound from the {\em Tourism Service}
%by using a PAGCMScript command like the following

%\noindent \verb+unbind($tourism/interface::"attractions",+\\
%\verb+$attractionX/interface::"service")+

%\noindent \verb+unbind($tourism/interface::"weather")+


%----------------------------------------------------------------

%\TODO{add a short conclusion of this subsection / introduce next sec}

This framework exemplifies how NF components can be used in GCM/ProActive
to provide autonomic capabilities through MAPE loops.
By providing specific implementations of each phase of the loop while adhering
to the interaction means presented it is possible to provide other, more complex
autonomic features without modifying the functional part of the GCM application.
Below, we   describe our efforts to provide another level of flexibility
by showing how, using GCM/ProActive features, it is possible to build
GCM based applications interacting with applications based on SCA.






\subsection{SCA interoperability}
\label{exp:sca}
\input{tex/sca.tex}





\section{Use Case: Autonomic Reconfiguration}
\label{use}

%\subsection{Use-case: Autonomic Reconfiguration of SCA Assembly (2p - Cristian - 15/Jan)}

%Use material from Cristian's thesis and relate this experiment with Cloud/grid executions.

%\TODO{Francoise and Cristian: use ideas on VMs and migration}
GCM/ProActive has been used to implement several use-cases as real-world
applications, some of them being 
 briefly cited in the conclusion of this paper; each of those
use-cases benefit from some of the capabilities of our component
model.  The purpose of this section is so to illustrate, thanks to a
specifically built scenario,
the integration of all the
previously presented notions at the same time. We show below that our
framework is not only particularly convenient for programming
autonomically adaptable components, but also effective in practice.
More precisely, we describe a system performing some autonomic adaptation
actions,  able to also rely  on sub-components or external components
to take its adaptation decisions.



 Compared to some of the related works (e.g. autonomous home control system as an illustrative use case of FraSCAti~\cite{Frascati}), the fact that the GCM/ProActive implementation includes a MAPE compliant framework considerably drives the autonomicity
feature programming in practice. Other  frameworks, also inspired
by the MAPE model exist and target
similar goals (Behavioural Skeletons \cite{beske:ipdps:09}, Dynaco \cite{Dynaco05} for any sort of application, Safdis \cite{safdis} 
 for service-oriented applications). However, none pushed  the
efforts as far as we did, including from an engineering perspective, 
to demonstrate that it is of practical interest to 
 cleany and entirely separate the concern of autonomicity
management from  the functional code.

\subsection{Use case description}
\label{exp:scenario}


Consider a GCM/ProActive application as shown in Figure \ref{dynamic:gcm-farm}. 
Component {\bf G} provides a GUI 
that acts as a front-end for a user to submit computations to a farm of worker components contained
in the composite {\bf F}. The composite contains three {\em worker} components {\bf W1}, {\bf W2},
and {\bf W3}. These workers encapsulate
real or virtual instances that perform a computation and store their results in a common
repository, provided by the component {\bf R}.

\begin{figure}
\centering
\includegraphics[width=5in]{figs/gcm-farm}
\caption{GCM autonomic application example. A farm of workers that monitors its load
and can adjust the number of workers.}
\label{dynamic:gcm-farm}
\end{figure}


In this setting, each {\em worker} has a Monitoring component in its membrane. 
Each Monitoring component implements machine-dependent
metrics like {\em freeDisk} space, {\em freeMemory}, and {\em idleCPU} time, and also provides a logical metric 
{\em reqServPerSec} that counts the number of requests that have been served per second by this {\em worker}.

The autonomic behaviour is provided by a MAPE loop in the membrane of {\bf F}.
In this loop, the Monitoring component {\bf M$_f$} collects the metrics of each worker using
NF bindings to each {\em metrics} interface, shown as dashed lines in Figure \ref{dynamic:gcm-farm}.
This way, {\bf M$_f$} can compute an aggregated
{\em avgReqServPerSec} metric for the system and, at the same time, be aware of the machine status of each worker.
The Analysis component {\bf A$_f$} contains two rules that intend to regulate the load 
of the set of workers. The first one triggers an alarm when the amount
of operations per second is lower than the threshold $t_{L}$. The second one triggers
an alarm when the average idle time of the workers is bigger than $t_{I}$.
The Planning component {\bf P$_f$} reacts according to the kind of alarm received.
In the case of the first alarm, {\bf P$_f$} checks the amount of idle time of the 
workers to confirm that they are all busy most of the time and diagnoses that 
the low number of operations per second is due to overload of the workers,
so it creates a script (in GCMScript) that allocates a new worker
inside {\bf F} and creates the bindings to add it to the farm.
In the case of the second alarm, {\bf P$_f$} diagnoses that the workers are
being sub-utilised and decides to remove one, so it chooses the worker
that has had the least load in the last minute and creates a script
that removes that worker from {\bf P$_f$}, unless there is only
one worker inside, in which case it does nothing.
The Execution component {\bf E$_f$} is the interpreter that embeds
the GCMScript engine and is able to execute the instructions over the composition.
Figure \ref{dynamic:gcm-script-add}(a) shows an example of a GCMScript code that adds a new {\em worker} to the farm.

\begin{figure}
\centering
\includegraphics[width=5in]{figs/gcm-script-addworker}
\caption{GCMScript sample codes. (a) Adds a worker to the farm. (b) Changes external repository to {\bf RP}}
\label{dynamic:gcm-script-add}
\end{figure}

The metrics provided by {\bf M$_f$} are accesible by the membrane of component {\bf G}
which can poll it and use it to display to the user the current status of the farm.
{\bf G} does not have an autonomic behaviour, as it just implements a Monitoring component
{\bf M$_g$} that collects data from {\bf M$_f$}.
Component {\bf R} is also non-autonomic and it implements a Monitoring component {\bf M$_r$}
that checks the amount of free space on the repository. This metric, called {\em
freeRepositorySpace} is exposed by {\bf M$_r$} and read by {\bf M$_f$}. 
Component {\bf A$_f$} may include another rule that is activated when the {\em
freeRepositorySpace} reaches a low threshold, and sends an alarm to {\bf P$_f$}
that may decide to unbind {\bf R} and use another equivalent repository {\bf RP}.
The sample code is in Figure \ref{dynamic:gcm-script-add}(b).

This way the MAPE loop of {\bf F} monitors not only the internal behaviour of the workers,
but is also able to check the state of the repository. It is important to note that
the component based approach to the MAPE loop implementation allows
the modification of several parameters
of the autonomic loop, for example, one can adjust the thresholds in {\bf A$_f$} or 
changing the strategy in {\bf P$_f$} without affecting the behaviour of {\bf M$_f$} or {\bf E$_f$}.


\subsection{Experimental results}
\label{exp:results}

Experimentation was done over an implementation of the example described in
Section \ref{exp:scenario}, where component {\bf F} is used as a blocked matrix
multiplication solver that distributes a set of tasks to the workers.
The solver takes as input matrices of size $2^N \times 2^N$, and generates
$2^{2(N-b)}$ tasks to distribute on the workers, where $b$ means a $2^b$ block
size for the solver.
Thus, for a fixed $b$, the number of tasks grows exponentially on $N$.

An extract of the implementation code is shown in Figure \ref{exp:matmultcode}.
Component {\bf G} receives computation requests from an external caller
through the {\em front-end} interface.
{\bf G} acts as the master of the computation, creates a set of
tasks to solve, and then starts all workers {\bf W} using a multicast call.
Once started, workers asynchronously poll the repository of
tasks to get one task through their {\em master} interface, solve it, send the
results to {\bf R} through their {\em repo} interface and reenter a request in
their own queue unless there are no more tasks.
This behaviour is easily implemented using multicast
interfaces and asynchronous invocations. 
Note that, once workers have started, component {\bf G} can terminate
the service of method {\tt multiply()} and is free to accept new multiplication
requests that will be added to the tasks repository.
In a synchronous setting, component {\bf G} would block inside the {\tt
multiply()} method until all workers have finished their tasks.
Using asynchronous calls, the assignation of tasks is implicitly triggered each
time a worker requests a new one.

Results for a computation identified by {\tt id} are stored in component
{\bf R}. The caller can retrieve the results through {\bf G} which calls
{\tt getResult(id)} from {\bf R}. Again, in a synchronous setting, this call
would block until all tasks have been computed.
However, in this case the method returns immediately; if all tasks for the
matrix {\tt id} have been computed, the actual result is obtained;
otherwise, a future object is transparently sent to the caller.
Upon examination of the result,
the caller outside {\bf G} may block, but {\bf G} can continue its execution.


\begin{figure}
\centering
\includegraphics[width=4in]{figs/matrixmultiplier-code}
\caption{Implementation code extracts for component {\bf G} 
and a worker {\bf W}$i$.
{\bf G} creates a bag of tasks, while workers asynchronously poll the bag
and store results in {\bf R}.
}
\label{exp:matmultcode}
\end{figure}

\bigskip

Our baseline is the execution of the application with an initial number of
workers $W$ and without any part of the MAPE loop included in the membrane
of any component. Then we compare against a version with monitoring components
included in each membrane, as well as the analysis component {\bf A$_f$} in the
membrane of {\bf F} ({\em MA} version). This version provides information on the
execution of the farm. In particular we insert the {\em reqServPerSec} metric in every
worker, and the {\em avgReqServPerSec} metric in {\bf F}. Component {\bf A$_f$}
includes the rule $\langle$ {\em avgReqServPerSec}, $<$, $t_{Tr}$ $\rangle$,
where $t_{Tr}$ is a given threshold.
 In case an alarm is triggered, {\bf A$_f$} notifies the
  user, and no other action is taken.
The third version includes a {\bf P$_f$} and {\bf E$_f$},
rendering component {\bf F} autonomic ({\em MAPE} version). In this case, when
an alarm is triggered,
the planner {\bf P$_f$} adds another worker component to the farm,
using the GCMScript code shown in Figure \ref{dynamic:gcm-script-add}.

Figure \ref{dynamic:eval} shows the execution time for an initial
number of workers $W=\{1,2,4,8\}$ and different sizes of matrices.
%\TODO{($N$  the size of the matrix) ... considering the execution time
%I am afraid N is not really the time of the matrix}.
The version with monitoring and analysis components (MA) behaves slower than the
baseline version, which is expected as we are introducing the overhead of reading metrics and checking rules
but not taking any action in regard.
The overhead reaches $25\%$ in a case where we stress the monitoring
and analysis components by frequently checking the stored rules.
The autonomic version of {\bf F} adds one worker when it finds that the average
number of served requests reaches the low threshold. For small sizes of the
matrix and, consequently, short execution times, performance is worse than the
baseline version as we must stop the activity of component {\bf F} before adding 
the new worker and this reconfiguration time is too long to show a benefit.
For bigger sizes, the execution time is long enough and the addition of new
worker compensates the overhead of measuring and reconfiguring the application.

\begin{figure}
\centering
\includegraphics[width=4in]{figs/eval}
\caption{Execution of a matrix multiplication solver using the application of
Figure \ref{dynamic:gcm-farm} for three configurations: base (no component
in the membrane), MA (Monitoring and Analysis activities), and MAPE (fully
autonomic). For $N$ big enough, the overhead of executing the autonomic
loop is compensated by the addition of a new worker. }
\label{dynamic:eval}
\end{figure}


\bigskip
This example shows how the autonomic features of GCM/ProActive can be used to build an application
that autonomically adapts its architecture according to certain rules by modifying bindings
and compositions.
However, not only bindings and compositions can be dynamically modified.
In GCM/ProActive  active objects and  components can migrate transparently between machines
that have been acquired from grid or cloud infrastructures. It is possible, then, to define migration actions as GCMScript methods and enrich
the set of adaptations that can be executed as part of the autonomic loop.
For example, it is possible to define a rule that checks if the {\em freeMem} metric of a {\em worker}
reaches a lower bound; in case it does, a planner creates a GCMScript that acquires
a new machine with higher total memory, stops the {\em worker}, migrates the worker to the new machine
and restarts its lifecycle. From the point of view of the functional execution, the application
does not need to be aware that a component has been migrated.
This feature provides another level of flexibility to the autonomic control loop.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../spe2013"
%%% End: 
