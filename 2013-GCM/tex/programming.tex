
\section{Programming Distributed Components: GCM and Active Objects}
\label{sec:programming}
\subsection{Active Objects}
\label{prog:ao}

Programming distributed applications is a difficult task. The
distributed application developer has to face both concurrency issues
and location-related issues.    The active object paradigm
\cite{Lavender:1996:AO,CHS:POPL04}  provides a
solution for easing the programming of distributed applications by
abstracting away the notions of concurrency and of object location.
Active
objects are similar to actors \cite{Agha86-book}, but
better integrated with the notion of objects.
Active objects are isolated entities that are manipulated by a single
thread; they communicate between themselves by asynchronous method
invocations. Active objects act as the unit of
distribution and of concurrency, naturally abstracting away
communication between remote entities and local availability of data.
 An object is said to be
active if it is equipped with a thread and a queue for storing
invocations; it can be deployed on a remote machine. As a consequence,
every method call to such an object will be a remote method invocation; we
call such an invocation a \emph{request}. An active object is thus an
object that treats the requests it receives; it is an object together
with a thread. To ensure absence of sharing between activities, each
object belongs to a single activity led by a single active object.

To decouple the caller object from the invoked object, contrarily
to a classical remote invocation, the invoker is not blocked waiting
for the result, instead a {\em future} object is created and represents the
result of the remote invocation.
A
\emph{future}~\cite{Halstead85} is a placeholder for an
object that is not yet available; this construct is very convenient
for easily expressing concurrent or distributed programs. 
Futures can be stored and passed as arguments or results of (remote)
method invocations. 
In ProActive, we chose to
have transparent futures so that the programmer does not have to
explicitly manipulate futures: if a future object is accessed while
the object is not yet available, the program is blocked. The advantage
of this approach
is that while synchronisation between entities is simple, the
programmer does not have to worry about which objects can be a future, and the
result of a remote invocation is only waited at the moment when it is
really needed.

Next section describes the structure and execution model of GCM components.
It presents the active
object execution model in the context of the GCM component model.

\subsection{Asynchronous Components}
\label{prog:principles}


GCM is a component model adapted from Fractal. The structure of a
GCM component assembly and the terminology used by GCM
are shown in Figure~\ref{fig:assembly}. 
Services are offered through \emph{server interfaces}, and required
services should be bound to \emph{client interfaces}.
The first architectural particularity
of GCM components, compared  to Fractal, is the existence of
one-to-many (multicast), many-to-one (gathercast),  and many-to-many (MxN) interfaces. One-to-many interfaces transform a
single invocation into many invocations, potentially distributing the
invocation parameters. Many-to-one interfaces wait for several
invocations and transform them into a single one, potentially
aggregating the parameters received. MxN interfaces feature both
aspects. More details on collective
interfaces %implementation through groups of active objects with futures \cite{GroupIJPP}
 can be found in \cite{EuroPar09}. The second architectural particularity of GCM is
 the way non-functional concerns can be structured inside what is
 called a \emph{membrane}; this  will
 be detailed in Section~\ref{prog:membrane}.

 \begin{figure}[!ht]
 \centering
 \includegraphics[width=.6\linewidth]{figs/omnigraffle_GCM_assembly.pdf}
   \caption{A GCM component assembly} \label{fig:assembly}
 \end{figure}

GCM/ProActive is the implementation of the GCM component model based on
active objects.
In GCM/ProActive each component is implemented by an active object so
that the component architecture reflects the distribution and the
concurrency aspects. In the following, each time we mention component
we are referring to a GCM/ProActive component that contains an active object. 
% \TODO{FB: I would suggest here to put that GCM/PA is THE imple;entation of the GCM component model, because, we try here to sell that we
% devised the GCM and the nice implementation on active objects, in a
% coherent manner. Somehow the two are tighlgy integrated, and taking
% another pgm language for implementing the GCM would not be so nice!
% LH: I
% would not go into this details, and keep a by safety, TBD}
One of the main advantages of using active objects to implement
components is their adaptation to distribution. Indeed, 
active objects provide a natural way to provide loosely coupled
components.  By loosely coupled components, we mean components
responsible for their own state and execution, and only communicating
via asynchronous communications, i.e. asynchronous requests with
futures. Asynchronous communications increase and automate
parallelism; and absence of sharing eases the design of concurrent
systems.  Additionally, loose coupling reduces the impact of latency
and enforces the fact that each component is responsible for its own
execution. Finally, independent components also ease the autonomic
management of component systems, enabling systems to be more dynamic,
more scalable, and easily adaptable to different execution contexts.
That is why we think that a distributed component system should rely
on loosely coupled components communicating via asynchronous
communications, and not sharing any memory; consequently, we think
active objects are particularly adapted to implement a distributed
component model.

One of the interests of ProActive is that no specific syntax or
instruction is needed to program active objects compared to usual Java
code. Consequently, the content of primitive
GCM/ProActive components is similar to Julia components \cite{fractal}; it consists of classical Java code.  The interested
reader could find in \cite{proactiveB} examples of
ProActive programs, and of GCM/ProActive components. In ProActive, as no
code  is needed for distribution, the non-expert programmer
can reuse its application written in pure Java (or in Julia). However,
the expert programmer might also simply want to reorder the
instructions involving communications and synchronisations, e.g.
separating the remote invocation from the access to the result, in
order to increase parallelism.

\paragraph{Communication}
Active objects provide a convenient programming model for distributed
components; this implies that components communicate as follows.
Components are loosely coupled and communicate by
asynchronous message sending: when a component performs an invocation
on its client interface, this invocation is transmitted to a
destination component and the message, called \emph{request}, is
enqueued at the receiver side in a queue. The message will be treated
later by the destination component.  Request parameters are copied
when passed between components, so that each component remains
responsible for its own state.


Asynchronous treatment of requests is a crucial feature of our model as
it allows each component to execute independently. The use of futures
allows the invoker
to keep a future reference to the result and to only block if it needs the
computed value. Futures can be passed by reference between components,
as invocation parameters or results.

% ; however, when
% programming components, it reveals useful to obtain a result from an
% invocation. For this, the notion of futures used by active objects
% provide a convenient programming abstraction. Invocation from one
% component to another is performed asynchronously, but 

Components do not share memory. The only shared references are
references to futures that have a single-assignment semantics; their
value will be finally transmitted by a copy semantics. Consequently,
there is no concurrent memory access between any two components.
Overall, each component has its own thread, executing independently
from the other, in a much service-oriented manner.
Of course, several components can also be bound to the same destination
component and invoke its services in a concurrent manner.


\paragraph{Execution scenarios} Let us now illustrate the execution of
GCM/ProActive applications by a couple of typical scenarios.
 \begin{figure}[!tb]
 \centering
 \includegraphics[width=.99\linewidth]{figs/simpleScenario}
   \caption{A simple communication scenario} \label{fig:simpleScenario}
 \end{figure}

Figure~\ref{fig:simpleScenario} shows a simple communication scenario
between two GCM/ProActive components; it consists of four steps. 
The configuration simply
consists of two primitive components bound together.
\texttt{CI} is the name of the client interface of the component on the
left; it is bound to the single server interface of the component on
the right. Each component contains a request queue (set of boxes in the
top part).
 At the beginning (step~1), the component
on the left performs an invocation on the other one; this creates a
new future and enqueues a new request  in the destination request
queue (this is shown by a  circle at the first position in the request
queue of the  component on the right). Then, the component on the left can
perform local computations or other remote invocations; after a while the component on the right
will serve the request and execute the associated business code
(step~2). Possibly, the component on the left may access the future
\texttt{f}, blocking the execution until the result is computed and
returned; in the meantime, the component on the right executed the
request and computed a result for it (step~3); the request is now
finished and a result is associated to it. The left part of the
request queue is shown with dotted borders to indicate that the
requests in this part of the queue are finished and a result value is associated to them.
 Finally, in step~4, the
result is returned to the component on the left that can continue its
execution. 

This example shows the loosely coupled nature of the
component model and the asynchronous execution that results.
We call our component model asynchronous because communication does not
trigger computation on the receiver side immediately, it just enqueues
a request. However, such a mechanism can be implemented with synchronous or
asynchronous communications. In GCM/ProActive a rendezvous ensures the
causal ordering of messages~\cite{CMT-DisComp96}.



 \begin{figure}[!t]
 \centering
 \includegraphics[width=.72\linewidth]{figs/DelegationScenario.pdf}
   \caption{A delegation scenario} \label{fig:delegation}
 \end{figure}

Figure~\ref{fig:delegation} shows a slightly more complex scenario
where a component delegates the service of a request to another component,
illustrating the importance of futures and of the fact that they can be passed
as reference between components.  
The configuration consists of three  primitive components, \textbf{A},
\textbf{B}, and \textbf{C}, bound together.
In step~1, when the component \textbf{A} makes an invocation to
component \textbf{B}, this one delegates the invocation to \textbf{C}. The new request
is called \texttt{gee} with a modified
invocation parameter and the result of this delegated invocation is
directly returned as a result. Step~2 shows that, as future references can be
passed between components, the request service \texttt{foo} in \textbf{B} can terminate
and \textbf{B} can now serve other requests from its queue while \textbf{C} is
made responsible of replying to component \textbf{A}.
Finally, in step~3, the future reference is returned to \textbf{A} that now directly waits for the
result being computed by component \textbf{C}.
Eventually, \textbf{C} will serve the request and compute a result value for
it. The computed value will be transmitted to each component that holds a
reference to the corresponding future; in this case, to components
\textbf{B} and \textbf{A}.


\paragraph{Hierarchy}
GCM is a hierarchical component model. Primitive components are
the leaves of the composition tree; they contain the application logic. 
By contrast, composite components are made of other components, either
primitive or composite ones. Composite components have a predefined
behaviour because they are only used as composition tools: they serve all the requests
they receive and transmit them to their internal components or to external
components depending on the invocation and the bindings.
Composite components are also implemented by active objects and
equipped with a request queue,
but the service of a request is predefined: a
composite component keeps delegating invocations in a much similar way
to the component \textbf{B} of Figure~\ref{fig:delegation}. 
In other words, service methods are generated automatically.
Requests
follow bindings, going from an emitter primitive component to a
receiver primitive component that is bound to it; this binding can be
an indirect binding, passing through the bounding box of several composite components.
Implementing composite components with active objects allows us to have
a programming model (and a behaviour of components) common to all
the hierarchical levels. This is
particularly useful when implementing the control part of the components:
composite components can be monitored and controlled in the same way
as primitive ones. For example, a composite component can be
stopped and still accept incoming requests in its queue (in order to
serve them after when the component is started), thus preventing the
invoker from being blocked.


 \begin{figure}[!t]
 \centering
 \includegraphics[width=.9\linewidth]{figs/DelegationComposite.pdf}
   \caption{Delegation in a composite component} \label{fig:delegationcomposite}
 \end{figure}

Figure~\ref{fig:delegationcomposite} illustrates the flow of
invocations in a composite component~\textbf{A}
that surrounds a primitive component~\textbf{B}.
\textbf{B} has one server interface and one client
interface, both bound to~\textbf{A}.
A request is
received by~\textbf{A} on its server interface
\texttt{SI} (step~1). This request is handled by~\textbf{A}
and sent unchanged to the sub-component bound to the interface~\texttt{SI},
component~\textbf{B} (step~2). Then, suppose that~\textbf{B} relies on an external invocation to serve
the request. This invocation is sent to the surrounding composite~\textbf{A}
in step~3. In the meantime, component \textbf{A} has finished the
service of the first request and returned a future pointing to the
request in \textbf{B}, similarly to the delegation scenario above (in
Figure~\ref{fig:delegation}, the component \textbf{B} returned a
future as result). Then, symmetrically to step~2, the service of the request
by the composite only consists in sending the request to the external
world through its external client interface \texttt{CI}. Note that the
composite is able to serve the second request because the first one is
finished.
Note that, contrarily to \textbf{A}, component \textbf{B} is waiting for the
result of the invocation on \texttt{gee()} and will only be available
when a real result for the request on \texttt{gee} is available.

% \TODO{LUDO: if necessary, add an example similar to the delegation scenario}
% \TODO{CR: I think it is necessary. After these paragraphs about hierarchy and the similarity
% of the composite behaviour to that of component B in the example, it is worth to have a small
% figure showing the flow of invocations in the composite case. It has not be very complex, though,
% and it does not need a long explanation}

\paragraph{Granularity of the component model}

A general issue when designing a component model is the foreseen
granularity of the components.
In the case of a hierarchical component model, this question
can be refined into ``what is a good size for a primitive component?''
% When addressing distribution aspects of
% a component model, the same question arises again, but becomes more
% complex: ``what is the relation between the unit of composition (the
% primitive component) and the unit of distribution?''.

Fractal does not impose any granularity for the components, but the
existence of composite bindings and some of the features of the model
suggest a rather fine grained implementation: a primitive component
should contain a small number of objects.  
In GCM/ProActive  in order to allow primitive components to be the
unit of distribution, GCM/ProActive
components should  have a coarser granularity
than Fractal ones. 
Overall, the GCM has been conceived with a
granularity that is somehow in the middle between small grain Fractal
components and very coarse grain component models, like CCM where a component is of a size 
comparable to an application. Somehow, GCM has been conceived thinking of 
components of the size of an MPI process (or an active object);
however, it can also be used in a  finer or 
coarser grain way.


This difference of granularity between Fractal and GCM
partially explains why some of the features that could be implemented
by a small Fractal component and are highly used in a Grid setting
have been defined as first class citizens in the GCM.  For example,
multicast interfaces could be expressed in Fractal by binding components
that perform the broadcast, but such components would be too small to
be used as the unit of distribution.

This granularity argument also explains why we can afford to attach the structure of an
active object to each
component; if a component only contained a few objects, it would not be
reasonable to allocate a thread, a request queue, and a future pool to it.
Consequently, GCM/ProActive is not designed to create
a large number of components on the same machine. If a distributed application is to be composed of a very large set of
small components, then it is more efficient to design it as a
GCM/ProActive component system of medium-size components where each of
the GCM primitive component consists of a Fractal assembly of small components.


\paragraph{Advantages and drawbacks of the execution model}
By nature, our model enforces that each component owns a single
thread, which imposes a component structure that fits the distribution
and concurrency aspects.
Our execution model  enforces a
strong separation between the execution inside each
component. Consequently the design of the application is made easier.
Thanks to the mono-threaded nature of each component, no race
condition is possible, and there is no need for locks or synchronization
code. Components only interact by request-replies on well-defined
interfaces and thus each component can be programmed
really independently from the others.


Futures help in the transparent management of values returned by a
service invocation through a component interface. They
dramatically ease the programming task by allowing the content of a
component to appear as a non threaded code
relying only upon (remote) methods as in an object-oriented non concurrent program.
Futures by themselves do not improve performances. Getting
performances (high parallelisation) comes from the asynchronous (and
distributed) processing.  Asynchronous processing could be obtained
through a hand-written concurrent and distributed approach (e.g. using
explicit thread programming, making sure distributed calls are
asynchronous and corresponding replies come back to callers through
explicit callbacks as in the actor model).
With our approach, the code is looking
non concurrent and so it is more easy to understand, but it is still parallel thanks to asychronous calls with futures offered by the active object technology.  

The main drawback of this approach is that the synchronization that
occurs when accessing a future can create deadlocks in case of
circular dependencies. For example, if two components are mutually
dependent, and the first one is waiting for a result from the second,
while the second needs a reply from the first one before continuing
its execution, then a deadlock occurs.
The risk of such deadlocks is however limited by the fact that futures
can be transmitted as results or invocation parameters. Indeed, if in a
circular dependence
one of the requests directly returns a future, then the deadlock can be
avoided, as illustrated in the composite component case (see
Figure~\ref{fig:delegationcomposite} and its explanation above).
A deeper analysis of the use of transparent futures, their advantages
and drawbacks, and the design of a static deadlock detection framework
for components with transparent futures can be found in~\cite{CHM:FACS08}.

  We are investigating the possibility to allow some controlled
multithreading inside active objects~\cite{HHI-coordination2013}.
Applied to  components, this approach should remove some of the deadlocks (in the example above, the first component could
create a new thread in order to answer to the second one), but this aspect is
outside the scope of this article.  
%  \TODO{FB: peut etre simplement ici preciser de quel composant precis
%    on parle . as illustrated by the behaviour of the composite
%    component A of Figure 4 LH: ADDED and its explanation above: j ai
%    peur que ``the behaviour ...'' sonne comme si le composite avait un
%  comportement particulier}
Additionally, these  multithreaded components  partially overcome the limitation of one single
thread per component, allowing us to better scale on multicore
architecture: one component is able to run several threads on the same
machine.


Finally, one of the main
advantages of our model is that it is easier to design autonomic
strategies for the management of such loosely coupled components, as
we will show in Section~\ref{dynamic}.
 
 
\subsection{Componentized Membrane}
\label{prog:membrane}


Fractal  considers the control part of a component as a set of {\em object controllers}
contained in the membrane of a Fractal component. These controllers are defined in a static way when
the component is instantiated and they are accesible via {\em control} interfaces.

\paragraph{NF Components} GCM extends the notion of {\em controllers} to allow
the introduction of Non-Functional (NF) components \cite{naoumenko:nfmembrane}, in line with the  componentized membranes  introduced in \cite{Aokell}. 
NF components reside in the membrane (c.f. Figure \ref{fig:assembly}) of GCM components and have the same goal as object controllers:
they provide management features to GCM components and take charge of NF tasks.
We call such a construct a {\em componentized membrane}, it enables the component-based design of
component control.
Because NF tasks can be complex to program and even may need adaptation to
face to changing runtime
conditions, adopting a  component-based architecture is a very
flexible and powerful solution. It  also provides better reusability.
Other than that, NF components share the same features as regular functional (F) components.
They can be assembled, composed and introspected, and even can have NF components in their membranes as well.
The possibility of having a componentized membrane brings some advantages to GCM. 
Notably, (1) the separation of NF concerns from the functional part by decoupling the F and NF implementation,
and (2) the possibility of easily designing and implementing  complex and
adaptable NF behaviours.

The separation of concerns between F and NF parts allows us to design
the two behaviours in separate ways
and delay the integration of the two aspects until deployment time. Even then, after the component has been instantiated,
it is possible through introspection to analyse and dynamically modify only the F components, or only
the NF components without affecting the rest of the architecture.
This decoupling permits the management concerns to be designed by an expert that does not need to
know about the functional behaviour, and, conversely, the programmer of the functional part needs not to focus
on the non-functional concerns.
Concerns like composition, bindings, monitoring, reconfiguration, security, load balancing, 
and in general self-management behaviours can be designed without having to know
the details of the functional part, focusing only on the component management.
However, at execution time, both F and NF parts need to interact. In particular,
a NF concern related to self-management may need to know the composition 
or to observe the behaviour of the F part, and
it may need to modify some attribute or to carry on an action that affects the 
 F part.
For this effect, certain NF interfaces like the {\em attribute controller},
{\em binding controller}, or {\em content controller} allow the
observation  or the modification of the  characteristics
that affect the behaviour of the F part (but only the business code exposed by the component, i.e. only the one accessible through the component API).
It is also possible to add customized implementations of these NF interfaces or to add
additional NF interfaces.% to allow the effective interaction between the F and
%NF parts.

Some NF concerns like self-management behaviours may require
a complex implementation, and it becomes reasonable to separate the
application control in different components. By using a componentized
membrane we allow the implementation of complex
tasks using components that, similarly to the functional part, can be
assembled, composed, and replaced.

Of course, this does not forbid to have regular (non-active) objects
implementing NF concerns.
In general, the most basic tasks like the control of the life cycle of
components, the control of bindings, and the control 
of the compositions, do not need the complexity and overhead of a component
oriented approach.
The componentized membrane is flexible enough to allow different levels of complexity by implementing some
NF concerns using non-active objects as controllers and leaving others as  components.

GCM/ProActive provides an additional advantage.
%As GCM/ProActive components are implemented as active objects,
%each component has its own execution thread, serving policy, and lifecycle.
By implementing NF concerns as GCM components, we may have
parallelism and asynchronous communications inside the membrane itself improving
the efficiency of the NF tasks.
It is even possible to transparently distribute the NF components themselves.

Figure \ref{prog:gcm-nf} shows the GCM notation for the
componentized membrane.
The figure shows two components in the membrane of a composite component, and one component in the membrane
of a primitive component.

\begin{figure}
\centering
\includegraphics[width=4.55in]{figs/pos-gcm-notation}
\caption{Composite GCM component with components in the membrane}
\label{prog:gcm-nf}
\end{figure}



\paragraph{NF Interfaces and NF bindings}
If we want to develop NF tasks that require the coordination of different
membranes, this is not possible by the traditional Fractal model, where there is
no explicit binding between NF interfaces.
GCM  provides client NF interfaces, which enable
inter-membrane bindings and communications. We first review the
different kinds of NF interfaces that exist in GCM, and then detail the
set of bindings that can interconnect them, illustrating the necessity
of such NF bindings to realize non-trivial management procedures.


In many cases, management activities require a meaningful
collaboration of several tasks that can be spread transversally to other
components. For example, if we want to get a measure of the energy consumption
of an application, it is necessary to aggregate the energy consumption of all the
individual components. In a different case, if we want to provide a security
certificate for an application, this certificate must be updated through all
its components.

GCM defines control interfaces, referred to as {\em NF interfaces}.
Where Fractal components provides only {\em NF server interfaces} (called {\em controller} interfaces),
GCM also includes {\em NF client interfaces}.
NF client interfaces can be bound to NF server interfaces of other components, in order to
communicate with other membranes and allow collaborative NF tasks. 
Collaborative NF tasks can also be created between the membrane of a composite component
and the membranes of internal subcomponents following the hierarchy of GCM components.
For this case, GCM defines additional {\em internal NF interfaces}.
Internal NF interfaces allow components residing in a membrane
to communicate with the membrane of components residing in the functional content,
and conversely, it allows the membrane of internal components to communicate with  components
 in the membrane of the parent.

Figure~\ref{prog:gcm-nf} shows internal and external NF interfaces,
and the possible interconnections between them.
Figure \ref{prog:gcm-nf} also illustrates the possible bindings using NF client interfaces.
A client interface of a NF Component can connect to an {\em internal NF client interface} 
that pertains to the membrane (1). 
This {\em internal NF client interface} acts as a passage from the membrane to
the content. An {\em internal NF client interface} can bind to the {\em
external NF interface} of a sub-component (2). This allows the
propagation of management tasks
originating from the membrane to the membranes of sub-components. A simple
example is the sending of a stop signal from the membrane of a
composite to the internal components.
% Mention why the internal NF interfaces are useful?
% It's because we don't need to change the bindings if we replace the NF component.

The communication can also flow from the content to the membrane.
An {\em external NF client interface} of a subcomponent can connect to an
{\em internal NF server interface} of its parent component (3). This {\em
internal NF server interface} is a communication channel from the content to the
membrane. The {\em internal NF server interface} can also connect to the server
interface of a NF Component (4).
This allows the membranes of sub-components to access to management tasks on
the membrane of its parent component.
For example, a component that represents a storage resource can use a NF client
interface to communicate to its parent that the storage capacity is near
full, so that the membrane of the parent can take some action.

The other elements shown in Figure~\ref{prog:gcm-nf} show the
bindings between the {\em external NF server interface} of a component and the
server interface of a NF Component in the membrane (5); the bindings between
the client interface of a NF Component to an {\em external NF client interface}
(6) to allow communication with external components. Finally, an {\em external
NF client interface} can be bound to an {\em external NF server interface} of
another component (7), effectively interconnecting the membrane of two components. 
For example, a security manager residing in
the membrane can propagate a security certificate to all the components with
which it maintains a functional binding in order to authenticate itself.

\begin{figure}[!bt]
\centering
\includegraphics[width=.8\linewidth]{figs/PAGCM-states}
\caption{Lifecycle of a GCM component and request service policy}
\label{prog:gcm-LFC}
\end{figure}

\subsection{The Component Lifecycle, and the Request Service Policy}
\label{prog:gcm-requests}

The component lifecycle and the associated scheduling of requests
deserves some additional explanations. As explained in
Section~\ref{prog:ao}, a component has a single request queue.
This queue receives both F and NF requests and serves them in a sequential
way.
However, there are situations in which it is useful that the component serves
only NF requests while leaving the F requests in the queue; e.g., this is
 the case  for the dynamic adaptations
primitives that will be presented in Section~\ref{dynamic}.

% However, if a component is
% \emph{stopped}, whether it is a primitive or a composite one, it will
% not serve most of the requests in its queue, as further explained
% below. Note that there is a single request queue storing both
% functional and non-functional requests.


The lifecycle of a component, and the requests served in each state
are shown in Figure~\ref{prog:gcm-LFC}. Both the non-functional part and  the
functional part can be either in the \emph{started} or in the
\emph{stopped} state.
A component can only be in three
different states concerning its lifecycle: either it is fully started
(both membrane and functional part started),
in which case it serves all the requests in its queue one after the
other. On the contrary, a component can be fully stopped, i.e. both
its membrane and its functional content are stopped; in that case, the
only requests served are those on the following object controllers:
\emph{MembraneController}, \emph{ContentController}, \emph{NameController},
\emph{SuperController}, and \emph{BindingController}. There is an
intermediate state where only the membrane is started; in this state,
 all the non-functional requests are served,
including the ones targeting components in the membrane or
subcomponents. The lifecycle state of the component can be changed by
invoking the appropriate method (labeling the arrows in
Figure~\ref{prog:gcm-LFC}) on the \emph{LifeCycleController} or the
\emph{MembraneController}.

Additionally to this service policy, each non-functional request is
attached a priority level; three priority levels exist. Request of the
highest priority level will be served before any other request; this
is useful for enforcing the rapid enactment of a critical
reconfiguration step. Requests of the intermediate level will be
served before any functional request that is before it in the request
queue (maintaining the order relatively to the other non-functional
requests); this is useful for performing several non-functional
operations consecutively. Finally the lowest priority level is the
default one, it treats the requests in a FIFO order, even relatively
to functional ones maintaining the potential causality between the
different requests.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../spe2013"
%%% End: 
