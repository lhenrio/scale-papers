The purpose of this section is to describe how GCM/ProActive component
applications can be integrated within a service-oriented environment,
and more particularly an SCA assembly. We think that this contribution
is crucial as it shows that our programming and composition model is
indeed convenient for effectively building large-scale
service-oriented applications. GCM/ProActive framework is not only an
effective programming model, it is also  generic and open and allows
for the integration with other different composition models.


Our overall goal is to provide GCM components with the possiblity to
be integrated within SCA component based applications. I.e., our objective
is to be able to expose GCM components as SCA ones, and to compose applications made of GCM components and other
SCA components. Typically, services requiring high performance
computing and high scalability, through parallel execution on Cloud
resources will be instantiated as GCM/ProActive components, and a SOA
application will be composed from those high performance components and other
SCA components provided by third parties and running on other SCA
platforms.
 

To this aim, GCM components must feature additional aspects to be able
to be integrated as specific parts within an SCA application.
Among them, the inter-components communication mechanism is key, and we have chosen Web Services
as a communication protocol to support SCA bindings, it is the most
standard communication mechanism used for
 SOA applications. Besides,
SCA specification includes SCA intents and an ADL-driven approach to
describe  component assemblies.
We also made GCM/ProActive compliant to those aspects in order to
allow for a full integration of GCM components into SCA applications.
Notice  that our goal is not to implement a full-fledged SCA platform
on top of GCM/ProActive, as FraSCAti \cite{Frascati} for instance, but it is rather to
offer mechanisms that can ease the integration of GCM asynchronous
components, with their peculiar features, as SCA entities
able to connect with third parties SCA components. 
Overall, our objective is to be able to extend  SCA applications
with the possibility to integrate high-performing, large-scale distributed, and
autonomous  components.
%; to do this, we need support for intents for a
%full-fledged interaction between the two frameworks
% suggestion of Bastien to remove this explanation:
%also to allow the
%design of a GCM+SCA application as a whole, we allow the instantiation
%of GCM components from SCA ADL
% We describe below the GCM/SCA
% integration in details.




\subsubsection{Web service based component interaction}\label{sec:web-service-based}
As any ProActive active objects, GCM component interfaces
can be annotated as Web Service supported communication channels.
  A specific component controller is available for selecting
which server interface of the component to expose. 
A server interface exposed this way translates into the automatic
deployment of a web service\footnote{implemented thanks to the Apache
  CXF Framework \url{cxf.apache.org/}} (WS) at runtime.  The role of this WS is simply to delegate an incoming method
call on this interface to the effective GCM server interface object,
automatically blocking on the received future in case this method
intends to return a value, and sending back this value to the caller acting as the WS client.
The corresponding WS endpoint reference serves as the information to
be stored at the client side when binding an interface to this server
interface.

 Because invoking a web service method is blocking
 for the caller until the reply gets back, a GCM client interface which
is bound  to
 a WS exposed server interface is automatically attached a proxy which
 replaces the default ProActive proxy. The role of this proxy  is to trigger the WS
 invocation, wait for the reply, and only then, return this reply to
 the component code. As the method call is not based on
 ProActive requests, it is not possible to return a future reference
 to the caller, because it is impossible to support its later assignement. As using
 ProActive requests is transparent anyway, replacing the proxy that
 handles such requests by a proxy that plays the role of a usual WS
 client has no impact on the component code. Also, the 
implementation provided by default for this proxy can be easily replaced by
an ad-hoc written one (e.g. if deciding to connect to a WS-Rest instead
of WS-Soap service implementation): which proxy implementation class
to use is configurable through a specific syntactical construct  of the GCM ADL.

\subsubsection{SCA intents}
As specified by the SCA policy \cite{sca-policy}, each method 
offered or required by any SCA component interface can be augmented
with some intents. A typical intent can ensure
security of service invocation, by cyphering, authenticating,
or guaranteeing integrity of the exchanged messages. Note that
these intents are usually involving the caller and the callee of a method. This is
why GCM/ProActive interoperating with full-fledged third-party SCA components
must support SCA intents even if  our objective is not to 
provide a full-fledged SCA compliant framework.
%  Also,
% depending on the characteristics of the binding between a service
% consumer and a service provider, failures could cause messages to be
% lost, redelivered, or delivered in a different order than they were originally
% sent out \TODO{LH: and?  how is it specified in SCA?}. 

Through policies, specific implementations of intents are made
available, and consequently, the user composing the application is
offered the possibility to attach any intent to any interface method
by indicating which policy is chosen. The order in which the intents
are selected dictates the order in which they will be applied.
We added to GCM/ProActive a new NF controller acting as a SCA intent controller;
it offers methods to add, remove, and query intents relatively to a
method of a functional interface of the component. Notice that
similarly to FraSCAti \cite{Frascati} and beyond the SCA specification which only
deals with design time, we support the possibility to dynamically
modify the list of intents attached to a given method.

Internally, to attach an intent to a specific method of a specific
interface, we must intercept incoming and outgoing requests in order
to realize the specified intents. For this, we would need
GCM/ProActive to support the notion of input and output interceptors.
% and we raised the question if such interceptors could be
% available as components in the membrane or not. \TODO{explain either
%   ghere or in sec 3 that how interceptors are related to components in
% the membrane.} However such
% interceptors have a special role as they are pertaining both to the
% functional part (i.e. the functional interface from which service invocations
% have to be intercepted) and somehow to the non-functional one (as the
% code of an intent generally deals with non-functional matters).
% \TODO{LH: note that this is the case for any interceptor!!!}
%   As the GCM ADL factory ensures that the role of any
% component is clearly stated (it is either F, or NF, not both), we
% decided not to rely upon GCM components nor object controllers in the
% membrane of the host GCM component for supporting the code for SCA
% intents. We have as a perspective to conduct research to support the dual-role of interceptors
% and intents in an extension of GCM, consequently  provide capability to
% compose (and so, freely schedule) intents as components hosted by the membrane
% thus contemplating to give access to the other NF features provided by such a membrane.
% \TODO{LH: it looks like it was too difficult so we did not do
%   it now... which is not a good argument here I agree that
%   interceptors are one of the strongest polace where F and NF aspects
%   interconnect , but I do not see this as a good reason for not
%   creating components for them; I see this as  a necessity to
%   carefully study their status; here is a proposal for a new
%   paragraph; replacing ``internally ...'' BTW I would put this
%   paragraph later:}
An interceptor could be seen as a component in the membrane that would
be directly bound to (external and internal) functional interfaces that it connects (see Figure \ref{fig:assembly}); in Fractal, it
is possible to have an interceptor object in the membrane. However,
the precise design of such controllers as components is trickier than
for other NF components: interceptor components deal with non-functional aspects
(like security) but are mainly bound to functional interfaces, and
should have a restricted impact on the flow of functional requests. As
the design of interceptors as components is a complex issue, they are
not yet part of the GCM model, and consequently we could not use them
here.  Extending GCM to support component interceptors is outside of
the purpose of this article.

Instead, to support dynamic intents in Java-based GCM components, we dynamically
generate the bytecode corresponding to a subclass of the one
implementing the functional part of the component (using Javassist\footnote{Javassist
  \url{www.csg.is.titech.ac.jp/\~chiba/javassist/tutorial}}) .
In this subclass, each method is redefined in such a way that it will
sequentially run a list of additional codes corresponding to the
currently deployed intents, both at entry and at exit of the original
method call. 
%  If the method is exposed by a client interface of the
% component, then, it is a proxy to the bound object that is generated
% dynamically as a subclass of the ProActive proxy that is obtained
% through the component interface binding operation.  In the subclassed
% proxy, the same input and output interception mechanism as described
% above applies. However, the restriction is that the method call must
% not return a future, i.e. be a synchronous method call, so that the
% returned value or exception can be intercepted and be applied all
% required intents acting as output interceptor codes.
So far, to prove the feasibility of our approach, we have only  implemented
intents for security (using pairs of public/private keys
provided at intent deployment time).  As we do not implement the
PolicySet feature of the SCA intent specification, we didn't 
develop elaborated mechanisms to group alternate intent codes as
sets in codebases.


%  Our intention so far has been mainly to prove the
% technical feasability of adding SCA intents to GCM, but it is not our
% primary goal to turn GCM/ProActive as a full-fledged SCA platform like
%  e.g. FraSCAti. Indeed, GCM/ProActive aim is to act as
% a high performance and highly scalable programming  platform
% for parallel and distributed applications. While
% integrating GCM with third parties SCA components is a crucial
% feature, we consider that it is not our
% primary goal.\TODO{LH: it looks like you mean that turning into
%   full-fledged SCA will endanger performance or scalability, if it is
%   the case we must say so and explain why}

 
%However in contrast to standard GCM NF behaviours described previously
%that translate into NF components or object controllers living in the
%membrane, such interceptors whose role would be to wove intents
%(aspects) to any F call, in a synchronous manner: i.e. an incoming F
%service request has to be intercepted at the exact moment it begins to
%be served by the active object implementing the component content,
%routed to the first intent and subsequently to all the others to be
%applied some more treatment, then the thread serving the request
%should continue the normal behaviour; if the intent has to apply as
%output, ie. if the F method calls a service offered by a bound
%component, some intents may also synchronously be applied by the
%thread running the F method.  Code corresponding to the intent could
%take the form of software components, as in FraSCAti, but, it is
%important to notice that such components are of different nature than
%NF components, because they do not expose interfaces featuring some NF
%service offered by the host component to either external or internal
%components.  However, taking advantage of component technology to
%compose intents in the form of components would offer to the intents
%composer the ability to act on the scheduling of the components that
%must apply to the intercepted F call; such capability of impacting the
%order the aspects/intents have been woven through the SCA intent
%controller would be powerful and innovative, anyway this is not
%usually offered by aspect oriented programming frameworks.

% constitutes an additional and new feature compared to aspect weaving,
% and to get the possibility to reuse existing code available as NF
% traditional component by binding to them.  on the (i.e. they add a NF
% behaviour to the F




%(3) support instantiation of SCA component assemblies comprising
%GCM/SCA components, through of the SCA ADL language.
\subsubsection{SCA component instantiation}
Our goal is also to allow a component described as an SCA component
to be instantiated as a GCM one. 
GCM and SCA share many features regarding the initial description of the component
assembly forming an application. Besides the description of the SCA
assembly using an XML dialect as we will discuss later, for Java components SCA builds upon annotations
within business code to make the component oriented features of such
code explicit. At instantiation time, the code of an SCA component is introspected in order to detect the presence of
these annotations and react accordingly. To instantiate a GCM
component based on SCA annotations, we need to generate code specific to the GCM component
model.
Next table summarizes the set of basic SCA features and corresponding annotation, and their
translations in the GCM platform.
\begin{center}
  \begin{tabular}{|c|c|c|}
    \hline
    &SCA&GCM \\
    \hline\hline
    Property injection&@Property&AttributeController\\
    \hline
    Component name&@ComponentName&NameController\\
    \hline
    Required/Offered Service&@Service/@Reference&Interface Server/Client\\
    \hline
    % Introspection\&Reconfiguration API&missing&component, *Controller\\
    % \hline
  \end{tabular}
\end{center}
  For instance, if the
@reference SCA annotation is attached to a field declaration in the
business class, this means this field stores the reference of
the bound component. Consequently, a subclass of the original Java
implementation class is generated using Javassist, which adds all
GCM API features related to the  management of a binding within the
code: all methods of the GCM/Fractal BindingController NF interface
are implemented considering the field name as the bound component
reference. If the annotation regards a security intent for
instance, the code for holding the private/public key pair is also
generated in the corresponding subclass that is also required for
applying the additional needed intent code as explained previously.

%% Notice that, GCM controllers also allows the introspection and the
%% reconfiguration of the component, which is originally not supported
%% by SCA.

Concerning the SCA ADL language, it exhibits XML elements constructs
that can easily be translated into the corresponding GCM ADL
format. To this aim, we have simply added a front-end module to the
GCM ADL factory: given an SCA ADL file, this module translates this
file into a GCM ADL one and will proceed with the normal process of
instantiating the resulting GCM component.  Notice that the presence
of the GCM controllers allows the introspection and the
reconfiguration of the component, which is originally not supported by
SCA.  Furthermore, this module automatically annotates each SCA server
interface to be exposed as a web service, and adds specific SCA
controllers: for handling SCA binding types, SCA properties and SCA
intents.  When instantiating primitive component, the factory
additionally triggers the Javassist code generation mentioned so to handle SCA annotations.

Consequently, specific GCM features requiring to be exposed
through the corresponding GCM ADL file  can not be supported.
For example, as SCA ADL does not support non-functional
interfaces, nor collective interfaces, it is impossible for our SCA extended
GCM ADL factory
to parse an SCA component description that will directly turn into a GCM
component exhibiting  such kind of interfaces.
 Furthermore,
Fractal and GCM impose that all interfaces forming the component type
even if optionnally implemented, are described at component
instantiation time; it is thus impossible  to add those GCM specific 
interfaces dynamically. However, it seems possible, in the future, to extend GCM in
order to support dynamic
interface creation and thus  % instantiate IT is not a problem of instantiation
 eventually obtain
full-fledged  GCM 
high-performance autonomic components generated from SCA ADL assemblies
and SCA annotated business code. 
%% \TODO{ ADDED following phrase after Bastien latest suggestion. Either we let reader
%%  consider that this SCA -> GCM ADL factory is useful for doing all we
%%  need, or we simply discard all the text that presents this transformation from SCA ADL to GCM ADL.
%%  This means we should also remove the part about SCA instantiation through
%%  annotations in the SCA code, ie the whole 533 subsection.}
An alternative and already practical solution  is to simply allow the user to manually add GCM ADL lines in the 
automatically generated GCM ADL file for the required specific GCM features,
 before proceeding to the normal
instantiation process.
  
% A feasible solution in case our goal would be
% to leverage GCM features for getting a high-performance
% and autonomic  support for an extended SCA model/platform,
%  would be to allow GCM interfaces to be dynamically
% (re)defined, so modifying the component type at runtime. Then by using the
% GCM API, it would for instance become possible to add / remove NF
% interfaces and corresponding implementation in the membrane, given
% their implementation would not impact the F code (as it would not be
% easy to dynamically regenerate the bytecode of the already deployed
% business class corresponding to the SCA component).  
%The drawback
%would be to prevent ensuring strong properties on a GCM component
%assembly, as currently done by the Vercors platform.

\bigskip %A sort of conclusion of the SCA part
This section showed how to take full advantage of GCM components in
the scope of third party SCA services. The presented extension to
GCM/ProActive enables for instance a typical integration of services to compose an
SOA application as follows: have third party SCA components, hosted by
any SCA framework (e.g., Tuscany, FraSCAti), bound to a SCA/GCM
``front-end'' component exposing its interfaces to the outside as web
services.  This SCA/GCM component is acting as an  entry/exit point of an SCA domain; it gets bound,
 through SCA references
implemented as GCM bindings, to some independently instantiated
 GCM components. Those GCM/ProActive components,
created by a GCM ADL, are able to feature self adaptation and high
performance thanks to the use of GCM/ProActive asynchronous
components.  
All these components,  especially the ``front-end'' one, can even be designed
as SCA components eventually enriched with GCM distinctive features, and  further  instantiated as GCM/ProActive ones.
Such a pattern can be useful whenever one specific
service within the SOA application
has to fulfill service level objectives regarding e.g. performance level, 
hosting cost, etc.
Such constraints may indeed require that the service itself is internally parallelized
and distributed e.g. on a private cluster, a public Cloud, etc.,  
and is self-adaptable regarding its use of other  services \cite{RBS:IARIA:2012}.


%; by binding this component to some full-fledged GCM ones,
%previously instantiated, the GCM/ProActive supported SCA domain as a
%whole is capable to self-adapt and feature high performance through
%parallelism and distribution by leveraging the ProActive supporting
%middleware.

  

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../spe2013"
%%% End: 
