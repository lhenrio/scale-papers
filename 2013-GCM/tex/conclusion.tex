\section{Comparison with Related Works}
\label{sec:comparison}

In this section, we present a comparison of our approach with some
similar approaches, either in the domain of adaptive component
systems, or of distributed programming models.


\subsection*{High-level component models for adaptive distributed systems}
First, several frameworks already exist for enabling better adaptation
of Fractal components. Among those the closest work to ours is AOKell
and FScript. Compared to those works, we think that our approach is
better adapted to simply program autonomous and distributed
components. Indeed, without our extension of FScript, distributed
execution of reconfiguration script would have to be triggered
manually, and the programmer would have to instantiate himself several
distributed FScript interpreters. 
Compared to AOKell, we have a stronger execution model which is
natively distributed and autonomous, it is also smoothly
integrated with features of the component model. Of course, a
GCM/ProActive application  could be programmed
relying on aspects and Fractal basic implementation (comprising
FractalRMI); but this would require significant additional 
design and coding efforts to end up with a solution comparable
to ours.

\smallskip

Some high-level component models targetting
adaptation for distributed systems have been recently proposed. They
introduce original approaches for expressing the needed dynamic
reconfigurations, instead of letting the adaptation loop programmer
explicitly devises them.

One of them is Kevoree (\url{kevoree.org} \cite{kevoree}). It leverages a model-driven approach, and
gains its originality by relying upon such a model even at runtime. In
the model, runtime entities are explicited, like nodes, components,
channels, or group of components that are subject to a same
synchronization policy. Kevoree approach is peculiar compared to
classical ones, as the dynamic adaptation required for any runtime
entity to apply is implicit: when a distributed entity receives a
model update that reflects the target running software architecture,
it extracts the reconfigurations that affect it and transform them
into a set of platform reconfiguration primitives.  

Another is DEECo \cite{Deeco} that targets
open-ended (e.g. ubiquitous) very dynamic distributed systems. Instead of describing explicitly an
architecture involving components, the model relies upon the notion of an
ensemble of components which is indeed a dynamic group of components,
mutually cooperating.  Bindings of components within an ensemble are
not made explicitly. Implicit bindings are inferred through the
knowledge about  exposed or required features, and also about their
reaction and adaptation abilities.
 The
membership of a component within any ensemble is conditioned to a
regularly evaluated membership condition: a given component, given
its internal state, could fulfill many ensenble membership conditions
at once.


Overall, Kevoree and DEECo are to be considered as higher-level
adaptive and distributed component oriented models, than GCM/ProActive
is. Say another way, it looks natural and more or less technically
feasible to implement each of such models as a specific GCM/ProActive
application strongly relying upon the provided open MAPE framework for
expressing the self-adaptation policies peculiar to each of such
models. The only point that sounds difficult to handle comes from the
absence of shared components in GCM (compared to Fractal)  as would
be required to allow a given component to belong to
several ensemble components of DEECo: it was a
deliberate choice not to provide this concept in the GCM specification \cite{gcm}, not
breaking the natural rule that a composite component is deemed 
the only   manager of the distributed components it
recursively embeds (and as such needs to maintain a state-full view
about its content). Notice that ensemble components of DEECo are stateless. 



\subsection*{Programming models for distributed systems}
Quite a lot of successful programming models exist for programming
distributed systems~\cite{GSTV12JISA}; among them actors and active
objects have recently gained interest and revealed very convenient way
to program active objects. Outside the ProActive world, one can
exhibit the recent successes of the actors, like in Scala
\cite{haller2009scala} and Akka~\cite{gupta2012akka}, or of the active
objects, like in ABS~\cite{HHMJL:FMCO13} and
JCobox~\cite{schafer2010jcobox}. From another point of view, several
component models exist to help the programmer design large-scale
adaptable distributed systems, like the ones \cite{Deeco,kevoree} mentioned
above. The main originality of our approach is to merge the two
worlds: we benefit from a programming model adapted to distributed
computing, based on active objects, and use it to implement a
component model adapted to large-scale deployment and adaptations.

Compared to other programming and runtime environment, we chose ProActive
because it has a good support for deployment, and it features
active-objects which is a programming abstraction more adapted to
component-oriented programming than pure actors.
Indeed, futures provide transparent communications of method results
that are more efficient than explicit callbacks of actor models. Also,
the pure actor paradigm consists in reacting to incoming messages and
possibly changing the actor behaviour, while active objects serve
incoming remote method invocations and possibly change their internal
state. 
Those two differences make the active-object programming model
particularly close to the usual abstractions of object-oriented
programming, which, to our mind, is closer to component-oriented
programming than the functional programming model encouraged by actors. 
 However an effective
implementation of GCM could also be realised based on any other actor
or active-object programming language.

\medskip
Another related research area is the work on connectors and their
automatic generation~\cite{Bures2008Connectors}. In those works,
bindings between components are automatically equipped with additional
(mostly non-functional features). Compared to our approach, the
objective is  similar; however, we chose
to encapsulate non-functional features at the level of the
component, inside its membrane, potentially as interceptors modifying
communications. 

First, encapsulating this logic at the level of the
component seems more coherent with our approach where the component is
the unit of distribution and of decision. One could argue that
sometimes the connection could be the right level where adaptation can
occur; in GCM/ProActive we chose to rely on proxy adaptation to handle
such cases, see Section~\ref{sec:web-service-based}.

The second and more important reason why we do not allow complex
adaptable connectors -- like Fractal's binding components and their
use in the Dream framework -- is that we want the bindings to
encapsulate a simple and uniform semantics. This paper advocates the
idea that autonomic components should be programmed as independent
entities with a well defined interaction semantics. This is why the
interplay between the programming model and the component architecture
is crucial.  We believe that bindings should have a uniform semantics
ensuring the independent execution of each component; in GCM/ProActive each binding
guarantees the same simple behaviour which is asynchronous requests and replies by futures.


\section{Conclusion}
\label{sec:conclusion}

The design of the GCM component model \cite{gcm},
 and its implementation on  top of the
ProActive parallel and distributed programming library and distributed
deployment framework
have  already started some years ago. Several published articles
 allowed us to focus on some particular features needed
in specific contexts (e.g. 
collective interfaces for 
SPMD parallel computing
 \cite{EuroPar09}, non-functional components 
for autonomic computing \cite{naoumenko:nfmembrane,RBS:IARIA:2012}, etc).
But this paper is original by its own: it presents GCM as 
an effective programming model, taking the opportunity to expose
why and how we have intentionnally taken benefit of a non component-oriented, yet elaborated programming model,
the active object model. Consequently, the paper gives a hopefully clear and
self-contained view of the way asynchronous and autonomous distributed
GCM components run.
The reader interested in the formalisation should refer to the work of
Henrio et al.~\cite{HKR:FMCO08}
for a formal specification of the runtime functional behaviour of GCM/ProActive.


From our recent state of the art analysis of component oriented solutions
for distributed programming, it appears that GCM/ProActive stands out for:
\begin{itemize}
\item its soundness, given that care has been taken to build upon a well-tried and
formalized underlying programming model \cite{HKL:SCP11}, and that all
component oriented specificities and associated operational semantics
are formalized in \cite{HKR:FMCO08};
\item its portability upon distributed runtime environments
thanks to the Java platform and ProActive open deployment capabilities;
\item its 
genericity and openness regarding all proposed features, both at  component model level, and at more practical levels like its interoperability capability with service oriented computing applications  through SCA and web services.
\end{itemize}

So far, numerous GCM applications, from toy ones to 
larger scope ones, have been developed. For example a message routing layer as a GCM
based middleware for federating enterprise service buses or supporting
hybrid grid/cloud distributed computing \cite{MB:CPE12}, and a peer-to-peer
system for web semantics data storage and retrieval through a
publish/subscribe approach \cite{FPBHB:AP2PS:2011,Play:D.2.1} have
been implemented relying on GCM/ProActive.  

The perspective here is to benefit not only from highly
distributed computing platforms, but also from intra node parallel hardware
as multicore and eventually GPU devices, when composing  parallel and
distributed applications.
 We are currently working on the replacement
of mono-threaded active objects by multi-active objects
\cite{HHI-coordination2013} 
to implement GCM components:  multi-threaded support of GCM method
calls  can allow GCM asynchronous components to fully benefit from
parallel hardware while keeping their loosely-coupled, asynchronous
and autonomous nature.

Formalisations on current GCM model and resulting GCM applications 
behaviour
at runtime  are also on-going work. In a complementary way, they 
use  theorem proving
techniques to formally specify what are the featured properties
 of the component model, either at the architectural level
\cite{formCoq2013}, or concerning its operational semantics \cite{HKR:FMCO08};
they also model GCM applications regarding the use they make
of GCM interfaces, including NF ones, and consequently model-check these models in order to 
detect if these applications exhibit or not some global properties, like
absence of deadlock \cite{BHHM:FACS11}. 
Next stage is to succeed to formally prove that 
GCM autonomous reconfigurations lead the GCM applications into a safe state.


% \TODO{FB to all: I m not sure , as discussed a month ago, that we
%   should come back in the conclusion to  problems raised about
%   interceptors as components role/nature; nor on the possibility to
%   work on extending GCM for being a full-fledged SCA platform as
%   fraSCAti, because, FraSCAti is here, so no real need except if we
%   would aim to influence the SCA standard which is not our goal.
% LH: I agree that there is already enough future work for a journal paper}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../spe2013"
%%% End: 
