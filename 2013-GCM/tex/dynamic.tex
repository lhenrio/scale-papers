\section{Dynamicity in GCM} \label{dynamic}

This section focuses on the ``dynamic'' part of GCM/ProActive. This is,
how an initially deployed architecture may be modified at runtime, without the
need for stopping all the system. 
Initially we provide a set of primitives and a reconfiguration language that
allows us to describe and execute reconfigurations. Later, we explain how this
can be executed autonomically by inserting components in the membrane that take
care of the autonomic behaviour and that these components can also be modified
to change dynamically the autonomic behaviour. 
In the last section we unify both features, and mention how these two features allows GCM/ProActive
to be used to build self-adaptive applications. 


\subsection{Reconfiguration}
\label{dynamic:reconf}

Like Fractal, each GCM component provides control interfaces giving
the possibility to modify, at runtime, the application
structure. Those interfaces can be invoked to add or remove components
or bindings at runtime. Generally, for consistency reasons, it is
required that the components directly impacted by a reconfiguration
action are stopped before being reconfigured. Unfortunately, directly
invoking those reconfiguration procedures can be cumbersome: accessing
components in the hierarchy requires multiple introspection calls, and
multiple  exceptions can be raised; consequently the code
actually doing the reconfiguration is just a small proportion of what
has to be written.

To ease the programming of reconfiguration procedures we designed a
framework dedicated to the reconfiguration of distributed components,
and in particular to the reconfiguration of GCM components. When
studying the adequacy of languages dedicated to the reconfiguration of
components to GCM, we realised that FScript~\cite{Fscript09} was close to be
adequate but was too much centralised.  Indeed, FScript provides the
primitives that are necessary to reconfigure a component system made
of Fractal or GCM components through reconfiguration scripts; it ensures a
much higher-level of abstraction than direct invocation to the
Fractal or GCM API, but the reconfiguration scripts were designed to be
interpreted in a centralised manner.


In a distributed component model, it seems more reasonable to
interpret reconfiguration scripts in a  distributed manner: several composite
components can be responsible for reconfiguring their sub-components
independently. This distributed approach allows parallelism, and thus
allows reconfiguration procedures to be run on large-scale
infrastructures. It is also better adapted to program autonomic
adaptation procedures, as each component can embed the adaptation
scripts necessary for its adaptation and trigger them autonomically,
when needed. Allowing each component to embed its own script
interpreter, and to interpret its own local scripts also contributes
to the loosely-coupled nature of our component model.

For this, our approach was quite simple but really effective, it is
based on two extensions of the FScript framework. The resulting
reconfiguration scripting language is called GCMScript, and is
integrated with  GCM/ProActive. 

\subsubsection*{A controller for reconfiguration}
We added a non-functional port, localised in several (possibly all)
components. This port is able to interpret reconfiguration orders.  We
called the non-functional object able to interpret reconfiguration
scripts a ~\texttt{ReconfigurationController} and embedded it inside the
membrane of the desired components. 

This controller embeds an instance of the FScript interpreter
and
provides a method {\small
  \texttt{load}} for loading reconfiguration (sub)scripts, and a method
{\small \texttt{execute}} for triggering a
reconfiguration action. 

In general, to ensure consistency and to avoid unpredictable interplay
between execution and reconfiguration of a component system, it is necessary to stop the
functional behaviour of a component while reconfiguring it.  Indeed,
the outcome of a request sending through an interface, while this
interface is being unbound, is unpredictable. In previous works, we
designed a protocol able to stop an asynchronous composite component together
with all its subcomponents in a safe way~\cite{HR:CBHPC08}. The
objective of such a protocol is to ease the design of safe adaptation
procedures: when a subsystem is entirely stopped, it can go through a
reconfiguration phase before being restarted.
As explained in Section~\ref{prog:gcm-requests}, stopping a component does not stop the membrane and thus a
stopped component can still interpret reconfiguration scripts and be
reconfigured. 

\subsubsection*{A primitive for distributed reconfiguration}
We extended FScript with primitives for triggering the remote
execution of
reconfiguration scripts.
The primitive {\small \verb|remote_execute|} triggers the execution of a
reconfiguration action on a remote component.  The target 
component is  specified as a FPath expression~\cite{Fscript09}; it is the component
that will evaluate the reconfiguration action. FPath
is a domain specific language to specify a path inside a component
assembly; it is embedded in the FScript interpreter and eases the
exploration and reconfiguration of components inside a possibly
complex assembly.
Upon remote script invocation, if no remote interpreter is available then one 
is automatically created by calling the {\small \verb|setNewEngine|} method 
on the remote reconfiguration controller.  
Then, the target component becomes in charge of interpreting 
the reconfiguration. Then the calling interpreter can continue the 
execution of its local script. Each reconfiguration script then runs
independently. The script invocation creates an asynchronous
invocation (without result).

% We also defined some helper functions that extend the FScript language
% and revealed to be interesting in the context of a distributed
% interpretation of reconfigurations. The most interesting function is a
% function \verb|evaluate| that takes as argument a string that contains
% an FPath and evaluates it locally. This function allows the programmer
% to pass an FPath as an argument of a remote script interpretation and
% interpret it at the destination side (instead of the caller side if
% the FPath was not embedded in a string).

\medskip

\begin{figure}
\centering
\includegraphics[width=.75\textwidth]{figs/GCMscript}
\caption{GCMscript interpreter: principles }
\label{dynamic:GCMscript}
\end{figure}

Figure~\ref{dynamic:GCMscript} illustrates the principles of the
distributed interpretation of reconfiguration scripts written in
GCMscript. Initially, the configuration consists of two components:
\textbf{A} and \textbf{B} which is inside \textbf{A}. Both components
are equipped with a \emph{reconfiguration controller} non-functional
interface and embed a GCMscript interpreter. When the action
\texttt{main} is invoked, the associated script is interpreted locally
at component \textbf{A}; this action consists of two instructions, the
first one (1) corresponds to line~2 of the script; it puts the
component \textbf{C} previously instantiated inside the component
\textbf{A}. For simplicity of the example suppose that the variable
\texttt{A} (resp. \texttt{C}) already contains a reference to the
component \texttt{A} (resp. \texttt{C}), but GCMscript also defines
primitives for fetching a component in the hierarchy or creating a
component.  The second instruction (2) corresponds to line~3 of the
script. It illustrates the distributed interpretation of
reconfiguration scripts enabled by our approach. It triggers the
interpretation of the action \texttt{action\_b} by the GCMscript
interpreter of component \textbf{B}. Note that the first argument of
\texttt{remote\_execute} uses an FPath expression to access the
sub-component \textbf{B}.


\medskip
From the execution model point of view, reconfiguration actions are
transmitted between components as requests and enqueued in the request
queue of the target component. When they are served, these
reconfiguration requests are 
 interpreted by a local GCMScript
interpreter embedded in the component membranes. We find this approach
particularly convenient for implementing autonomic components, as
illustrated below.

\subsection{Autonomicity}
\label{dynamic:autonomic}

We acknowledge that applications nowadays need to react quickly and
adapt themselves to changing situations. While this concern should be
included in the design of an application, it is not always clear how to
embed it along the functional task of the application, as the
capability for dynamic adaptation is a concern that may traverse
several components of the application.
It makes sense, thus, to consider the adaptation capability
as a non-functional concern.

GCM/ProActive includes a mechanism that allows us to embed this behaviour
in a transversal way in GCM applications.
This mechanism requires the capability to reconfigure the system according to the needs.
While GCMScript already allows us to modify the composition of the application
in a dynamic and distributed way, 
this reconfiguration must be triggered by an external actor, which may be
another component, or a user program.
In our vision,  components should also be able to take these decisions by themselves and 
to self-adapt to certain situations and thus, they should be made {\em autonomic}.

\subsubsection{Autonomic Computing}

In order to give  autonomic capability to GCM components, we resort to the {\em Autonomic Computing}
initiative pushed by IBM as response to the ever increasing complexity in the maintenance of
computing systems \cite{Horn:2001}.
As the abilities required for installing, configuring, and optimizing such
systems become too complex for system integrators and managers, it becomes
even more difficult to deliver decisive responses in a timely way.
%Let alone that all these activities are commonly not even part of the main
%functional objective of the system, but they are anyway crucial for obtaining
%an adequate respone.

The vision of autonomic computing is based on the idea of self-governing systems.
These systems can manage themselves given high-level objectives from an
administrator \cite{Kephart:vision:2003}. 
%The inspiration comes from the
%% autonomic nervous system of humans beings, which governs issues like heart
%% rate, body temperature, and respiration, freeing the brain from dealing with
%% those low-level, yet vital, activities.
 In complex computing systems, management tasks like
 installation, configuration, protection, optimization, are not the main
 functional objective of most applications, but if they are not properly
 addressed, the application cannot accomplish its functional task.
The proposition, then, is to promote self-governing systems that manage
all these non-functional tasks in the background, while the application and
consequently the developers can concentrate on the main functional goals.
In order to effectively free developers from the burden of programming
self-governing features for their applications, there must exist a way to
develop these concerns independently and to integrate them with the
application at a later stage.

One of the most common ways to provide autonomic behaviour is to rely on a {\em
feedback control loop} that follows four canonical activities: {\em collect},
{\em analyse}, {\em decide}, and {\em act}.
This idea evolved into a reference architecture for autonomic computing
\cite{IBM:autonomic:2006} in which the central element, called {\em autonomic
manager}, implements the activities defined by the generic feedback
control loop into an {\em autonomic control loop}.
This loop defines four phases: {\em Monitor}, {\em Analyse}, {\em Plan}, and {\em
Execute} and it is usually referred to as the {\em MAPE} autonomic control loop.
%Commonly, a central element identified as {\em Knowledge} is added
%representing the management data that can be shared by all the phases, and the model
%is called {\em MAPE-K}.
The reference architecture also considers the possibility of
arranging several autonomic managers in a hierarchical architecture
\cite{IBM:autonomic:2006}, making the autonomic behaviour more scalable and
supporting different autonomic control loops.

Following this view, we propose a framework for GCM components
that provides autonomic behaviour through MAPE loops, and that allows
these loops to interact through the hierarchical nature of GCM applications.
We describe now the details of this proposition.

\subsubsection{Autonomic managers for GCM components}
\label{dynamic:autonomicmanagers}

In order to effectively separate the autonomic concerns from the functional
part, the autonomic behaviour must be inserted in the membrane of GCM
components.
Our first approach is to consider a NF {\em autonomic manager} component which embeds
the autonomic behaviour. This component is inserted in the membrane of a GCM component
and the owner of the membrane becomes, then, a managed component.
The autonomic manager will observe the managed component and potentially
trigger adaptation actions over it.
Figure \ref{dynamic:mape}(a) shows  a composite component {\bf A} that
has been turned
autonomic by inserting an {\em autonomic manager} component in its membrane.
%The autonomic management tasks are made accesible through a NF server interface of {\bf A}
%called {\em Autonomic Management Interface}.

The autonomic behaviour is provided by a MAPE loop.
This loop may be completely implemented by a primitive autonomic manager component,
as illustrated in Figure \ref{dynamic:mape}(a).
However, that design is not flexible enough in case we need to modify only some part
of the autonomic behaviour.
% I don't say why would we need that . . .
Instead, we choose to provide a more flexible design where each phase of the MAPE loop
can be implemented in a separate way through different GCM components in the membrane.
Concretely, our autonomic manager is separated in four components: Monitoring, Analysis,
Planning, and Execution. This design intends to provide flexibility in two senses: (1)
instead of forcing every GCM component to be fully autonomic, we allow them to implement only
certain phases of the MAPE loop according to the degree of autonomic behaviour needed,
and add them or remove them at runtime;
and (2) by separating the MAPE loop in four components it becomes possible to modify the 
implementation of one specific MAPE phase without affecting the others.


Figure \ref{dynamic:mape}(b) shows the managed component {\bf A} with all four MAPE components in the membrane and 
the additional interfaces that have been added to composite {\bf A}. 
The general flow of the framework is as follows.
The {\em Monitoring} component collects monitoring data from component {\bf A}
using the specific means that {\bf A} may provide, e.g. using sensors for
reading certain parameters, intercepting requests on the functional interfaces,
or asking to the {\em Monitoring} interfaces of other components.
Using the collected monitoring data, the {\em Monitoring} component computes a
set of metrics and makes them available through an interface called {\em metrics}. 
The {\em Analysis} component provides an interface called {\em Rules} for receiving and storing
conditions that must be verified during the lifecycle of the component.
These conditions generally express desired values of some metrics.
At runtime, the {\em Analysis} component checks those conditions using the
values that it obtains from the {\em Monitoring} component. Whenever a
condition is not fulfilled, the {\em Analysis} component sends a
notification to the {\em Planning} component through an interface called {\em Alarm}.
The {\em Planning} component creates an adaptation plan
as a sequence of actions that can
modify the state of the service and take it to a desired objective state. For
taking decisions and feeding the required parameters, the planning
component can obtain information from the {\em Monitoring} component. 
The sequence of actions created by the {\em Planning} component are sent to the {\em Execution} component.
The {\em Execution} component executes the actions  using the
specific means that the component allows. As we envisage the possibility of
having actions that can be propagated to other components, the {\em Execution} component is
able to delegate some actions to the {\em Execution} component of other
services through the {\em Actions} interface.
This way, the loop is completed and the new information collected by the {\em
Monitoring} component will reflect the new state of the application.

\begin{figure}
\centering
\includegraphics[width=5.5in]{figs/gcm-mape}
\caption{(a) Primitive component implementing autonomic behaviour in the membrane of composite $A$.
         (b) Autonomic behaviour implemented as a MAPE loop with one component for each phase. 
         NF bindings to internal components have been omitted for readability.}
\label{dynamic:mape}
\end{figure}

This design has been designed as independently from the functional part as possible.
However, every implementation that intends to manage a concrete
application has to be adapted to the specific goals of the
application and to communicate with it. 
Also, the way to obtain information from the managed component, or the way
to trigger actions on it may also depend on the specific means that
the functional implementation permits.
Consequently, our design is generic for any GCM application until the point that we must
define the concrete sensors and actuators that must interact with the managed
component. Those sensors and actuators will be implementation
dependent. We propose a possible architecture of a MAPE loop with sensors and
actuators in Section~\ref{exp:mapeimplem}.


%%%--CHECK if it must go here . . .

We extended the GCM/ProActive reconfiguration API with MAPE specific
aspects. 
The new API
 allows the insertion or removal of each phase of the MAPE loop at runtime;
we also define a set of basic interfaces that each implementation of the MAPE loop
must provide and possibly extend in order to expose the autonomic management capabilities,
and to allow the communication between the MAPE loops of different components.
This results in a set of NF interfaces that must be added to each membrane, see Section~\ref{exp} for  details on this API.


%This does not limit, however, the separation of concerns that can be achieved by inserting the MAPE
%components in the membrane, as the programmer of the functional behaviour still does not
%need to be aware of the fact that the components may be rendered autonomic at runtime.
%\TODO{LH last paragraph is a little strange, I think: not sure what
%  the message is}


%In order to effectively separate the autonomic concerns from the functional
%part, we include the autonomic management task inside the membrane of GCM
%components.

%We define initially a NF {\em autonomic manager} component which embeds the autonomic
%management task. This component is inserted in the membrane of a GCM comonent
%and the owner of the membrane becomes, then, a managed component.
%The autonomic manager will observe the managed component and potentially
%trigger adaptation actions over it.
%Figure \ref{dynamic:am-comms} shows a setting where a composite {\bf A} is turned
%autonomic by inserting an {\em autonomic manager} component in the membrane.
%The autonomic management tasks are made accesible through a NF server interface of {\bf A}
%called {\em Autonomic Management Interface}.

%%----------------------------------------------------------------------------


\subsubsection{Interacting Autonomic Managers}
\label{dynamic:MAPEcomposition}

%interaction
In many situations, autonomic managers  should 
interact with the autonomic managers of other components.
These autonomic managers may belong to subcomponents in the case of a
composite, to the parent component, or to external components that are in the same
hierarchical level.
Figure \ref{dynamic:am-comms} shows the interactions of the Autonomic Manager 
of composite {\bf A}\footnote{Remember that each autonomic manager can be realised by four components
constituting the MAPE loop, see Section~\ref{dynamic:autonomicmanagers}.} 
with both subcomponents {\bf B} and {\bf C} using an internal NF client multicast interface. At the same time, the autonomic manager of {\bf B} uses
an external client NF interface to interact
with its peer component {\bf C}. Finally, component {\bf C} uses its own
client NF interface to reach the membrane of its parent component and communicate
with the autonomic manager of {\bf A}.

\begin{figure}
\centering
\includegraphics[width=3in]{figs/am-comms}
\caption{Composite GCM component with Autonomic Management bindings.
Autonomic Manager of {\bf A} is bound to Autonomic Managers of {\bf B} and {\bf C}.}
\label{dynamic:am-comms}
\end{figure}

By allowing the autonomic manager to communicate with the membranes of internal components,
we allow the construction of hierarchically organized autonomic managers.
In such a setting, a set of low-level managed resources may have an autonomic manager that
controls specific characteristics of the resource like storage space, free memory, and CPU utilization. 
A second-level of hierarchical managers may be in charge of higher level tasks like
self-optimization, or self-healing over sets of these low-level resources.
In the top of the hierarchy, an autonomic manager may have a  global view and orchestrate
the activity of the middle-level autonomic managers by giving priority to some autonomic behaviours
over anothers, or by solving conflicts. Such a hierarchy of autonomic
managers is shown on Figure~\ref{dynamic:am-comms}, where the
autonomic manager of \textbf{A} can control the autonomic managers of
components \textbf{B} and \textbf{C}.
% \TODO{CR: If necessary I could add a Figure exemplifying this
%   described hierarchy of Autonomic Manager; FB: not needed I think,
%   LH: I would just add 2 autopnomic managers insige B and C in fig
%   8. CR: Done! LH: now we should refer to them in the text, I can do
%   it}
Another setting may consider several autonomic managers  collaborating
as peers, at the same hierarchical level;
for example to perform load balancing, or by performing coordination tasks.
Both kind of architectures can be built using GCM autonomic components.
% Remember that each of the autonomic manager can be realised by the
% four components constituting the MAPE loop as described in the
% previous section.



\subsubsection{Autonomic Adaptation}

The reconfiguration language presented in Section~\ref{dynamic:reconf}
provides a convenient approach for programming the execution part of
the MAPE loop. Our asynchronous components equipped with MAPE loops
programmed thanks to GCMScript  provide a powerful framework for
easily programming components as independent entities, both from the
point of view of the execution flow, thanks to a request/reply mechanism,
and from the point of view of the component management, thanks to
componentized membranes equipped with MAPE tools. This way, both the
business code and its management are organized in a componentized
manner, where components act as loosely-coupled entities.

Section~\ref{use} illustrates the integration of all
those notions in a concrete scenario, showing that our framework
is particularly convenient
for programming autonomically adaptable components,
and that our autonomic managers are able 
to delegate adaptation decisions to sub-components, or to external components.
This scenario leverages the GCM-based MAPE framework and related
APIs. %  (detailed in \ref{exp:mapeimplem}
% in order to easily inject autonomicity concerns within component-based applications (including of the SOA nature). 



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../spe2013"
%%% End: 
