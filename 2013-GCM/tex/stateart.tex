
\section{ State of the Art: Distributed Component Models}
\label{prog:compsota}

This section reviews the crucial characteristics that should be
featured by a component model in order to support both distribution
and autonomic behaviour.
 For this we will first focus on the aspects
of the component models related to the description and evolution of
components,  especially relatively to autonomic adaptation. In a second step we will present more details about how those
basic characteristics relate to distribution management and what features are
provided by  existing distributed 
component models, with a particular focus on service-oriented components.
This section will conclude by a discussion on the programming models
for programming components as autonomous entities, making them
better adapted to distribution, easier to program and compose, and
easier to control in an autonomic manner. 
% As our objective is to target distributed systems, we will tend to
% refer to distributed component models in our examples.
% Components state-of-the-art (including GCM) Component features and how they are
% implemented on existing component framework.

% What kind of applications are best suited by certain types of component systems.
% Think what we talked from ``coarse'' SCA components to ``intermediate'' GCM
% components to ``fine''-grained Fractal components.

% Will we have a generic/plain state of the art about component, or it may be better to guide it to compare with the key features that we
% want to provide: adaptability, distribution, and autonomicity. 



%%%%%%%%%%
\subsection{General concepts}
Component models \cite{Szyperski:2002} aim to increase the code reusability,
ease of programming, and adaptability of software. They introduce well delimited software entities in the form of components with clearly defined interfaces corresponding to either the offered services or those explicitly required to fulfill them. Component based applications differentiate from other kind of applications (e.g. functional, procedural, object-oriented) by  making the resulting software architecture explicit. 

\subsubsection{Architectural description}
 In general, it is convenient to describe the initial component architecture using
 an assembly language, also named an Architecture Description Language
 (ADL), usually an XML dialect. Sometimes, the ADL includes a
 domain specific language that allows the description of the
 complete initial architecture by intention in contrast to by
 extension (concept coined as parameterized ADL, as in Darwin \cite{Darwin}).
% and could even allow to check some specific constraints beforehand, ie at
% deployment time (see SafArchie \cite{SafArchi}, ...).  
Associated to an ADL, a factory
 (generally combining a parser and component generators)  instantiates all the
 components that constitute the application. This step deploys base
 components from their source implementation and binds component instances according to the
 dependencies defined in the ADL.

\smallskip

A crucial aspect of architectural component description is the
hierarchical composition. A component model is said to be \emph{hierarchical} if components can
be glued together to form another component that itself can be used in
the composition; this is, for example, the case of SOFA \cite{Sofa2} and Fractal~\cite{fractal}. The composition hierarchy is then a tree with
``composite'' components as intermediate nodes, and basic blocks containing the
real business code as leaves. From a designer point of view, hierarchy
eases the creation of large applications by composition of
hierarchically built sub-components.
 The hierarchy underlying the software component architecture is
reflected within the ADL associated representation. 

\subsubsection{Dynamic reconfiguration}
Some component models allow the modification of the
architecture at runtime, through a specific set of APIs for component
instantiation, binding, life-cycle management, etc. Usually, the set
of allowed modifications is constrained by the types (including
component and interface types), and the architectural style of the involved components
(see ArchJava \cite{ArchJava}, Acme\cite{Acme}, SOFA 2.0 \cite{Sofa2Reconfig-HP06}
for instance).  More recently, requirements related to
an associated meta-model have been explored, e.g. \cite{Tune08}.

Besides using directly an API, the modifications can also be expressed
through scripting languages, associated to the ADL like in Darwin,
ArchJava, or built upon the API as FScript \cite{Fscript09} for
Fractal components, Lua for OpenCOM \cite{OpenCOM}, or  GCMscript (see Section~\ref{dynamic:reconf}). Allowed
modifications can also be the result of some component associated
constraints (Architecture Constraint Language) as in \cite{Tib07}, or
reconfiguration rules (Event Condition Action rules) like in
Automate \cite{Automate06}, Plastik \cite{plastik05}, Safran
\cite{David:safran:2006}, Dynaco \cite{Dynaco05}, or Rainbow
\cite{Garlan:rainbow:2004}, most of them relying upon the 
MAPE model for autonomic computing \cite{IBM:autonomic:2006}.

In this context, it is valuable to be able to check beforehand and to
ensure that the configuration resulting from a programmed or
rule-driven modification will comply to the architecture constraints
and correspond to the desired global-state. To reach this goal, some
works check in advance that some independently developed
reconfiguration rules are compatible \cite{Kilpatrick11}, or
even 
enforce the correct synchronisation of different autonomic control
loops \cite{GPR2013}.
Some approaches
check that, given a behavioural specification for each involved
component, the resulting composition features desired properties
(e.g. no deadlock can  occur even after 
reconfiguration operations).  The Vercors verification
platform~\cite{Vercors} is able to check this kind of properties for
GCM/ProActive components.  Some works ensure that in case some
reconfiguration process starts but can not fully lead the architecture
to the required state, e.g., if the reached state violates some
invariants of the component model or of
the application, then, the process is aborted and the
architecture is kept unchanged. For example \cite{Formaware} and
\cite{Leger10}  rely on transactional managers to maintain some
predefined invariants about the component model, the 
architectural style, and the application.  Some approaches regularly
check whether the current status is  compliant to some predefined
invariants and, if it is not the case, then some automatic reconfiguration is triggered.
For example, Acme/Armani~\cite{Armani} expresses architecture invariants with
first order logic.

\paragraph{Reconfiguration and Hierarchy}
Hierarchical component structure plays a particular role relatively to
reconfiguration (see \cite{Sofa2Reconfig-HP06} for a detailed
discussion on this aspect).  Not all component models makes sure that
the hierarchy is also available at runtime, once leaf components have
been instantiated.  In particular, models that do not allow
introspection and dynamic reconfiguration of the component
architecture do not consider useful to represent composite component
instances at runtime. As an example, this is what a strict
interpretation of the SCA specification \cite{sca-spec} would imply,
because SCA scope is only design time and excludes component
deployment and instantiation, leaving them as a platform-specific
concern.  On the contrary, in FraSCAti \cite{Frascati}, because it is
an SCA implementation based upon Fractal, it is useful to instantiate
FraSCAti composite components; this leads to the only SCA platform
offering run-time management of SCA assemblies.


\subsubsection{Open component models}
Component models can be considered highly configurable if it is
possible to choose the way the  application is controlled
through policies and/or meta-level artefacts constituting component
container or management level.
Some components even allow the programmer to define his own
control interfaces; for example we will see in this paper how
 controllers can be added to GCM components (see Section~\ref{dynamic:autonomic}).
Those component models that allow the customization of the set of
available  managers are called \emph{open component models}.


%   \TODO{LH: I do not understand this sentence} Besides personalized policies w.r.t. 
% standard ones regarding lifecycle management, binding
% resolution, and binding semantics (how the invocation of a service
% through the corresponding binding is enacted), etc., component models
% could also allow the programmer to add some customized additional
% policies/artefact. \TODO{LH: next sentence has no verb, what is the
%   message?}
% For example, specific controllers to monitor the
% component performances at runtime, to log, duplicate/broadcast or
% secure communications through bindings, to get a customized view of
% the current architecture, etc.  More generally, sophisticated
% management policies, like a complete autonomic control loop,
% could be added to a component in the form of a specific set of
% interacting controllers (e.g. sensors, actuators) (see
% \ref{dynamic:autonomic} for a concrete illustation of such an add-on).
% \TODO{I really do not get the message of the preceding paragraph+
%   ``open'' should be defined somewhere no?}
 
Whereas  EJB, .NET, CCA \cite{CCA} rely on quite strict
predefined containers/controller sets, open container based models like CCM \cite{CCM},
 or reflective component models like OpenCOM \cite{OpenCOM} and Fractal \cite{fractal} are highly
configurable. Ultimately, by plugging the adequate additional
controllers to such generic component models, 
the  component
architecture can be made self-reconfigurable. For example, Jade~\cite{Jade08}
is a self-repairable component framework built upon Fractal; more general-purpose
self-adaptation is provided by, for example,
Automate \cite{Automate06}, Safran \cite{David:safran:2006}, Dynaco \cite{Dynaco05}; 
the last two frameworks also built upon
Fractal. In Section \ref{exp:mapeimplem} we will illustrate how GCM has been
adequately extended to provide  self-management for
component applications.



\subsubsection{Adaptable control level} 
Moreover, the meta-level/management layer itself could be dynamically
reconfigurable. The advantage of such a paramount flexibility degree
is to allow the adaptation of the control strategies, without having
to redeploy the whole application; this is particularly useful in case
the context in which the components are running evolves so drastically that
even the control layer needs to be adapted. 

To enable the dynamic reconfiguration
of the control layer itself, this layer should be programmed in a
flexible manner.  Component orientation allows such level of
flexibility, yielding a \textit{componentized membrane} concept as
presented in \cite{Aokell}. Besides Fractal/AOKell, a Fractal implementation \cite{Aokell}, the SOFA component
model whose control part is built using microcomponents \cite{Sofa2} and Dynaco that uses full-fledged components  \cite{Dynaco05}, in GCM and in its implementation presented in this paper such flexibility is achieved by the fact that the control level is also made of components (see \ref{prog:membrane}).

An alternative to implement a non-functional concern (i.e. a code
orthogonal to the functional code) is to rely upon dynamic AOP
techniques (as in Safran~\cite{David:safran:2006}). However, in this case where the
concern is  an aspect and not a component, it suffers the known
drawback of AOP: the order aspects are executed is arbitrary (i.e. dictated by their
attachement order), and they are not able to interact.  On the contrary, a
concern implemented as a component allows it to be manipulated as a
first class entity within the component-based software architecture
forming the control layer: collaboration with other concerns to fulfill
its dedicated task becomes thus possible.

% \TODO{LH: I cannot understand  next sentence: what do you mean? what
%   is the message??}
% Offering that the control layer
% of  components be programmable using the most general and flexible 
% technique, that is in the form of software components, does not feature such limits.

%However, thinking of sophisticated component  control logic
%resulting from as elaborated strategies as autonomic ones 
%may in the end require to 
% defining component control logic may impose to 
%orchestrate/compose many non-functional concerns at once and in a maybe non sequential way. 

\subsubsection{Techniques for component adaptation}
As discussed above, either the functional or the control
part of the components can be adapted. However, one should also
distinguish the different techniques used to perform
adaptation. Depending on the chosen  technique, adaptation can be more or less
expressive and more or less intrusive on the executed code. Indeed,
three levels of intrusion can be identified: first, adaptation can
modify the component architecture by adding or removing components,
or changing the component connections; second, adaptation can
influence the behaviour of primitive components by adjusting
parameters of the components; finally, adaptation can
change the executed code itself.
This third level is generally achieved by techniques such as
AOP, like in the
Safran framework.
Note that all three levels of adaptations are meaningful both for functional and
for non-functional adaptations.

In our approach, we rely purely on
component and object techniques; this has been
sufficient to achieve adaptation in the two first levels in a quite natural and
efficient way.
%In practice, the two first levels described above can be achieved.
Regarding the third level, our implementation also relies on a
meta-object protocol allowing us to reify, intercept, and manipulate
inter-component communications. This gives us and
intermediate expressive power between pure component manipulation and
full control of the application code granted by AOP techniques. For
example, our
interception mechanism allows us to perform monitoring of business method invocations
 by generating events that are sent to the autonomic control layer.	

% In the context of adaptation and more particularily in case of autonomic
% adaptation, there is a need to  support some sort of interaction between
% business and control layers (e.g. a specific component
%  method invocation is reported  as an
% internal event towards the control layer
% so it gets logged). However, we can distinguish three different
% cases of adaptation that can be triggered, autonomously or not, on a component:  adaptation of the control part, including the autonomic strategies, adaptation of the component characteristic of the business code, 
% adaptation of the business code per se which is out of scope here because this bypasses
% the component-oriented feature of the application. \TODO{LUDO: I
%   disagree: it seems like we can only use the attribute controller to
%   change the ``characteristic'' of the component}

% %The first one may require flexibility in the way the control part is
% %implemented (see 2.1.4) but not only.
% \TODO{LUDO to FB: rephrase first sentence, I cannot read it, then I
%   will read again this paragraph} 
% As for the first, and as in some
% other works  like Safran~\cite{David:safran:2006}, the GCM autonomic control level features interfaces to
% dynamically update the autonomic behaviour (see Section \ref{dynamic:autonomic})  by adding/removing autonomic rules, including adding/removing
% some non-functional concern management (as pieces of code) as required by the action parts of the rules. As in FraSCAti, GCM allows these pieces of code
% to take the form of components. Besides, as GCM/ProActive autonomic management layer is
% itself a component-oriented implementation of a MAPE-K loop (see \ref{dynamic:autonomic}) it becomes possible to
% dynamically reconfigure the loop itself, e.g. to replace  the
% planning engine with a more efficient one, like in Dynaco \cite{Dynaco05}. 

% The second adaptation type requires an adequate interception mechanism
% to reify the needed information or to alter the functional code
% execution.\TODO{I got lost again: I thought you did not want to alter
%   the code! why do you need an interception mechanism to alter the code?}  In the case of the active object model the GCM
% implementation is based upon, meta-level programming techniques as
% method invocation reification are used \cite{metaobject}. As any service invocation on a
% functional component interface (server or client) is reified by the
% active object implementing the component, it is possible to intercept
% it so to apply any operation. Such interception mechanism, also
% offered by AOP frameworks, can trigger specific adaptation code in the form of
%  an input or output interceptor \cite{fractal}. In our approach, the interceptor
% simply consists in generating an event that will be caught by the
% monitoring part of the autonomic layer, and considered in an autonomic
% strategy, which could trigger reconfiguration actions on the component
% architecture or even alter some business-level information like component attributes, through the component
% controllers attached to the functional code (see \ref{prog:membrane}).
% Overall, leveraging such published endogeneous
% information, and gathering relevant external information about the context allow us to eventually get a comparable solution as
% self-adaptive Fractal components named Safran
% \cite{David:safran:2006} which follow an AOP approach. \TODO{LUDO to
%   FB: please clarify the message her I got lost too, I have the
%   feeling you want to speak about monitoring, but I am not sure at all}

%% An alternative exists also in GCM/ProActive, in the sense that such interceptors can be components injected in
%% the control level. As for AOP, the order the aspects are woven
%% dictates the order they are applied.  But as our aspects are
%% implemented as GCM components in the control part, there is a nice
%% perspective for future work, which is to allow to compose them with
%% the other control level components.  
%% Second, for any monitoring
%% purpose required by the autonomic adaptation process, we are able to
%% leverage the ProActive library runtime support which natively
%% publishes, through JMX events, data related to built-in active object
%% operations (sending a request,  serving
%% a request, sending a reply, etc).  Overall, not
%% using AOP but leveraging such published endogeneous information for
%% setting up self-adaptation strategies at the component control level,
%% allow us to eventually get a comparable solution as self-adaptive
%% Fractal components named Safran \cite{David:safran:2006}, except that
%% we have only focused so far on functional interface method invocations and have let aside
%% the interception of method calls of the specific component model 
%% API (component reconfiguration, access to its attributes, start/strop, etc) alas the reification of method invocation would apply the same way. 


%% Regarding the third adaptation mode, in our work but also in any
%%  of the other mentioned works, if we would require to get access to
%%  some business level data and code, we would need to instrument the
%%  business code, independently from the fact that it is or not embedded
%%  within a component. AOP would be a good approach obviously, and
%%  whatever technique would be used, the goal would be to generate a
%%  corresponding (endogeneous) event that can be consumed at the
%%  autonomic management level, and aggregated with other kind of
%%  monitored information like exogeneous events (e.g. in the form of JMX
%%  events exposing performance indicators of the supporting runtime
%%  environment like JVM memory consumption, etc). However, this type of
%% adaptation is not recommended as it breaks in some sense the concept of
%% a component which is to act as a box around the business code, protecting it
%% from external influence.


\subsection{Distributed component models}
Among the numerous component models, we focus below  on those
specifically targetting distributed computing. Also, we exclude
models such as Java Beans speciallly geared towards graphical
user oriented applications, EJB used within the application server
tiers of application servers, OSGi a centralized (mono JVM) service
oriented component model which focus on dynamic loading of service
implementation and dynamic service resolution.  In this
paper we focus on models where at least some of the components
can be deployed on several nodes.
This is exemplary of SOA
applications where component/service instances are de facto quite
coarse grained and decentralized because they can be made available by
different parties spread at the Internet scale; and of
distributed applications using several nodes  to
distribute communication and computation load. In this last case also,
the granularity of components is not very fine, because the overhead
of distribution would be too high if every single small component
could be accessed in a distributed manner.

%  compared to the component role
% within the whole application.

Distributed component models should also be convenient to represent
parallel applications. We call \emph{parallel application} an application that can
be modelled as a set of quite identical components that interact 
frequently to share data needed for the computation.
In this case, besides one-to-one interactions,  interactions
between multiple components (i.e., many-to-many interactions) are also required.
This is why models targetting parallel computing  introduce collective
interfaces including  MxN  interfaces as in CCA \cite{CCA}, GCM
\cite{gcm}.
% but as we focus on the use of GCM for SOA in this paper we will not detail this aspect too much). 

\paragraph{Components as an abstraction for Distribution}

We believe that the component abstraction is a good
programming abstraction, both at deployment-time and at runtime; this
is why we use the component as the unit of distribution: a single
primitive component should be placed on a single machine.
 Of course, a
composite component consisting of several others can be distributed
over several nodes; and similarly
 if
 the control  of a component is made of several components, then
those components can be  spread on many nodes.
Components serve as strong
structuring and deployment entities, both at the functional level, and
at the control level.

The first interest of having such an abstraction is the benefit from
hierarchy as a way to handle component distribution.
A major goal of distributed component models is to target large-scale
distributed computing, where the number of interacting distributed components 
is high.
Because these components are distributed, it is intrisically
complex to manipulate all or specific subsets of them at once. 
For example, consider a subset of components contributing to a parallel
computing application and deployed on the nodes of a given computing cluster.
Assume this cluster is going to be shutdown for maintenance purposes,  the
management layer needs to trigger the migration of the whole
component subset. Having an abstraction that gathers
all the distributed components currently deployed on the cluster would ease the
execution of the global migration.
Another example comes from SOA, where all the components that contribute
to implement a coarse-grained component oriented service, be they 
distributed or not, have to be 
considered as participating to the same entity. 
For those reasons, hierarchy eases the management of distributed components.

% , usually introduce the concept of composite
% components. Examples of distributed component models not including
% composite components are DCOM (.NET), CCA \cite{CCA04}, Creol, and to some limited extend, OpenCOM (which provides a sort of related  concept in the form of Component Framework to tightly couple a set of components). Whenever composite
% components permit to include components that are themselves composite,
% the component model is deemed hierarchical, without any limit
% regarding the number of hierarchical levels (as Fractal, ArchJava,
% Sofa).



If hierarchy is to be explicit at runtime and if components that belong to the
same composite component are distributed, then the composite component
itself should be instantiated at a chosen location. Sometimes,
especially when components feature an autonomic behaviour, the control
part of the component is so complex that it becomes useful to spread
the control part of one component on several
nodes. In this case, all software artefacts in charge of the component
management  can
themselves be also spread on many nodes.

To our knowledge, the approach we advocate in this article is the only
one to feature this expressive power: in GCM/ProActive, control and
management of a component can itself be realised by a distributed system.
Another particularity of our approach is that we adopt a strict and
well-defined execution model adapted to the distributed nature of our components.

\subsection{The special case of service oriented component models} \label{soa}
Service oriented component models and platforms make it totally
explicit that the aim of components is to offer services, which are central
in  SOA. Along
the SOA paradigm, services can exist independently of any client; they
are present in the environment, and the environment must allow clients
to dynamically discover, select, and bind to these services for further
invoking them. Of course, clients themselves can be applications built
around a component oriented model, that can, for example, express service requirements
through what is named an utility interface \cite{Sofa2}, but this is
not mandatory.

In service oriented computing, services are commonly stateless, allowing 
 many service invocations to be executed concurrently by the component 
implementing the service. However, services can also be stateful, meaning that care has
to be taken within the component-based service implementation whenever more than one service invocation is to be served.

Some component models for SOA such as the
SCA specification and its implementing platforms like Tuscany, Newton, or
FraSCAti, allow components to be implemented using any programming language, as
Java, Cobol, etc., including BPEL, the standard web services orchestration
language.
In BPEL, it is explicit that the role of any component is to
\textit{orchestrate} some required services, resulting in the provision
of new exposed services sometimes also registered for further discovery
and used by other components/services. 
Allowing heterogeneity in the way components are implemented has also
some consequences on the communication protocols allowing such
heterogeneous components to interact. This explains why SCA bindings
can be selectively bound to a particular communication protocol such
as SOAP, Java RMI, Sun JMS, etc.  As shown by
FraSCAti and also by SOFA 2.0 \cite{Sofa2SOA}, the challenge of these SCA
platforms is to support such a high variability level in the technologies available,
 in particular concerning
communication connectors that
requires a very modular and flexible infrastructure.



\subsection{Positionning: Programming Distributed Components}

Our original contribution relies on a strong interconnection 
between a component
model and a programming language.
The main strength of
GCM/ProActive is the coherence between the composition, the execution,
and the distribution structure for the application: components are
well separated entities that can be placed on different machines and
execute independently. The closest works from us are
the other formally specified programming models that provide
support for components: Creol~\cite{johnsen06tcs} and ABS
\cite{Lienhardt:2010}, which is inspired from Creol. In Creol,
components are extremely simple, non-hierarchical, and with little
support for binding and reconfiguration. In \cite{Lienhardt:2010}, the
authors take a process calculi perspective for designing components,
which is well formalised but much further from the software component
models than the approach we advocate here. In any case, the
programming languages ABS and Creol are very close to our programming
paradigm and are also formally specified (see~\cite{HKR:FMCO08} for a
formal specification of GCM/ProActive). These works also strengthen
the idea that the combination of active objects and components enable
the effective design and programming of large-scale distributed and
autonomic applications.
Most component models focus on the structural aspects, but without
information on the execution and synchronisation model. It is then
extremely difficult to predict the behaviour of a component system.
Indeed, predicting the result obtained when running an application
made of components relies on a precise knowledge of the
way these components interact and synchronise.



From a more practical point of view, every implementation of a
component model relies on one or several programming languages and
interaction paradigms. The practical choice made in real
implementations (Julia for Fractal in Java \cite{fractal}, MOCCA for a
CCA framework in Java~\cite{Mocca}) imposes at least a language
for defining the interactions between components. However, there was no
previous study on the interplay between the execution and the
composition models.
We claim that having a precise
execution model for components is crucial in order to program them
safely and efficiently. 
Of course, several component models like CCA, SCA, or CCM also allow
the connection of components written in any programming language, and
with a lot of variations on the interaction protocols. In a practical
component infrastructure, it is crucial to take into account the
connection with off-the-shelf components. That is why in GCM/ProActive
and in this paper we put a particular focus on the interconnection of
our components with other components specified as services.
Thought the interconnection of our components with the SOA world may partially
break the coherence we created in our composition and execution model,
we think this integration is necessary for programming real component systems.
We will see in Section~\ref{exp:sca} that, through SCA, we manage
to let our programming and execution model interact with service
oriented components in a convenient and effective way, while the pure
GCM/ProActive part of the application comes with a strong and
well-specified execution model.

\medskip

In conclusion, existing component models do not sufficiently consider
the interaction between the application architecture and its runtime
behaviour. Our component framework includes a
programming and runtime model
that is suitable for developing and deploying complex applications
especially in the context of autonomic, service-oriented, and
large-scale distributed computing.  The rest of this paper presents
our component model, runtime model, and reconfiguration and autonomic
framework; these contributions lead to a programming and execution
model where autonomous composition and hierarchy is present in every
concern and at every stage of the component lifetime.


% \TODO{interacting with SOA architectures is crucial, also featured by
%   GCM - compensate strict programming model}

% \subsection{Positioning}
%  GCM  is designed to be distributed; but what makes the originality of
%  our contribution is the additional use of active objects to make our
%  component autonomous.
%  The
%  resulting programming and composition model fits perfectly with the
%  notion of autonomous components not only well separated and visible
%  at design time, but also at runtime, including the fact that each
%  component is responsible for its own state and threads.

% This approach
% is, to the
% best of our knowledge, the only example of a hierarchical model with
% first class fully distributed composite components (case b above
% \TODO{check when b is clarified}) present at
% runtime.  On such a basis, we also allow for further dynamic and even
% autonomic reconfiguration operations that are then totally hierarchical and
% distributed.





%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../spe2013"
%%% End: 
