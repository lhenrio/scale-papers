\documentclass[10pt]{article}
\usepackage[latin1]{inputenc} 
\usepackage{alltt,url}
%% Old ttbox definition without frame:
\newenvironment{ttbox}{\begin{alltt}\ttbraces\small\tt}%
                      {\end{alltt}}
%the new definition of \. suppresses line breaks
\def\ttbraces{\let\.=\nobreak\chardef\{=`\{\chardef\}=`\}\chardef\|=`\\}
\newcommand\aspfun{ASP${}_{\rm fun}$\ }
\newcommand\aspfunp{ASP${}_{\rm fun}$}

\begin{document}
\section*{Response Letter to Second Review}
\begin{ttbox}
Ms. Ref. No.:  SCICO-D-09-00105R1
Title: ASPfun: A Typed Functional Active Object Calculus
Science of Computer Programming
\end{ttbox}

\subsubsection*{Letter to Reviewers and Editors}
Dear reviewers and editors,

\vspace{1ex}

we would like to thank You for a second round of constructive comments on the revised version
of our paper ``\aspfun: A Typed Functional Active Objects Calculus'' and the additional 
effort you have all put in to validate and improve our work.
In this first section, we answer directly to all points by
going through Your original comments, ticking them off as we go along by commenting
and referencing on our new second round changes.
For completeness, we have organised this second response into a larger document
containing in addition our letter responding to the first round of comments
plus our original working list from this first round that we had previously omitted.
This archive is contained as an appendix to this letter.

We will go now through the second review. First, we would like to especially address
the following important note from your letter.
\begin{quote}
Note that the second reviewer is still 
inclined to reject the paper. Since he is not strongly against acceptance, 
we think it would be a good idea to make a second attempt
to convince him of the interest and novelty of your approach [...]
\end{quote}
To address this point we have now integrated a couple of explanatory sentences in the 
second paragraph of the paper's introduction. There we directly pick up the criticism
of reviewer 2 and point out that we totally agree with him: we do not insist on the
novelty of our combination of objects and concurrency but instead believe in the value 
and novelty of our formalising and mechanising a calculus including typing for this 
combination. In fact, we did believe that the additional new subsection entitled 
``Object and Distribution: The Active Object Model'' would already have clarified this.
To further underline this motivation and our real contribution we think that
the additional phrasing now clarifies and most importantly clearly points to the
following subsection as the original background of the notion of distributed
objects we refer to in our work.
We have also added an additional paragraph at the end of the contribution section 
clarifying that the novelty lies in the formalisation in a theorem prover.

We next address the following general note from your previous letter.
\begin{quote}
It is also required to prepare a letter to reply reviewers' comments and
explain how your paper has been revised to take them into
account. 
\end{quote}
In the following, we go through the reviewers' comments step by step inlining 
short reports on our new corrections into the original comments. To emphasize the 
different origins of the texts we left the original comments in verbatim.

\begin{ttbox}
Reviewers' comments:

Reviewer 1: The authors seem to have taken almost all the comments into 
account, and the article has been improved in a significant way. A few 
typos should be corrected, eg. on page 3 "ProactiveCaromel". Therefore, 
in my opinion, the article could be accepted.

However, I should say that I am disappointed by the letter and the
answers to my comments. Some of them have not been answered at
all. The authors should have answered to all of them in the letter at
least. I do not find this very respectful of the work done by the
reviewers.
\end{ttbox}
We seriously apologize for this apparent lack of respect. However, it has not at all been
our intention to do so. On the contrary, we have appreciated all criticism 
given and have only for the sake of clarity of our previous response summarized
some of the more difficult or otherwise critical points. Wherever the criticism was
so clear and so constructive that it could be directly applied for an improvement, we
did omit it in our response. As a proof of this and our appreciation of the constructive
criticism given to us by the reviewers, we add to this letter in the appendix first our original
first response but also our previous working list where we had indeed inlined the responses to 
the criticism into the original reviewers comments but which we hadn't submitted verbatim as it
was too lengthy.


\begin{ttbox}
Reviewer 2: Although I appreciate the response from the authors and 
revisions made, I just don't see the point of the paper for the same 
reason as before.

The notions of activity and requests have been studied in concurrent
calculi for a long time (and most of them don't claim they express
distribution).  It seems that this work combines an object calculus
and a concurrent calculus in such a way that no interesting
interaction by this combination will happen.  Formalizing this
combination could be new but I don't think \_finding\_ this combination
is new.

To be fair, I see some value in their mechanized proofs, for they
might be useful for others.
\end{ttbox}
As already stated above at a more prominent position, we would like to repeat here
that we agree with this reviewers views and hope very much that the
improved version of our article suffices to clarify this. We almost literally
integrate now the above concise description of our contribution and expose it into
the most prominent position in our paper's introduction.
\begin{quote}
Distributed active objects represent an abstract notion of concurrently computing and 
communicating activities. Clearly, finding a combination of objects and concurrency 
is not new as a notion -- related notions are summarized in the following paragaraph 
-- but providing a fully formalized and mechanized calculus including typing for this 
combination is.
\end{quote}
In addition, we emphasized also in other places that our contribution lies in the 
formalisation.
More precisely, we further added to the introduction just before the contribution
paragraph "mechanised formalisation is a major contribution"; we added an additional
paragraph to the end of the contribution section (mentioned already above):
\begin{quote}
ASPfun is the first calculus to our knowledge to feature those
characteristics, however each of those characteristics exist in
some distributed programming language, and sometimes in other
calculi. In this context the main contribution of this paper is the
formalisation of these features as a single calculus, but
mainly the mechanised formalisation of this calculus in a theorem
prover. This paper will provide a complete description of our
formalisations, an analysis of the technical decisions that we have taken to
represent distributed objects in Isabelle/HOL, and an overall
conclusion on the techniques we used and the tools we provide.
\end{quote}
We also added in related works (positioning) this contribution:
\begin{quote}
 We consider that the major contribution
of this paper is the mechanical formalisation, and the precise
definition of formalisation tools that can be re-used to mechanically
formalise
other properties or languages.
\end{quote}
We finally also modified the second sentence in the conclusion to reflect
this more precise description of our contribution.
\begin{quote}
The particular impact of this work relies on the fact that it has been
entirely formalised and proved in the Isabelle
theorem prover.
\end{quote}


\begin{ttbox}
Minor comments:

Throughout the paper, author names in citations are not parenthesized,
like "the sigma-calculus Abadi and Cardelli (1996)".  Is it due to the
style file?
\end{ttbox}
We do apologize for the wrong citations in our previous version. They 
are accidental and are --  as is rightly recognized by this referee --
a result of using the wrong style file when preparing the submission 
and consequently not using the right \texttt{citet} or \texttt{citep} 
commands as necessary for a legible exposition with the 
appropriate style file for the journal ``Science of Computer Programming''.
We now use the style file \texttt{model1-num-names.bst} and correspondingly the
proper citation commands.

\begin{ttbox}
Reviewer 3: The paper has been revised in a way we consider satisfactorily. 
Only relatively minor issues remain which I list below:
\end{ttbox}
We thank this reviewer for finding further flaws and providing very
constructive advice for a substantial additional improvement. We followed the 
advice given and addressed the suggestions now as follows.
\begin{ttbox}
*) P2. "rather functional process" --> I see this as a rather unproper 
   sentence. A process is functional or not, "rather" reminds of something 
   fuzzy
\end{ttbox}
%-> functional: it would be confusing to mention actor's ability to change their behaviour
We omit ``rather'' now.
\begin{ttbox}
*) P2. "This is article is a formalization" --> I'd say it "introduces" a 
  formalization
\end{ttbox}
We have adopted that change as well.
\begin{ttbox}
*) P3. "ProactiveCaromel" --> it seems you are not using \cite{} command very 
   well.
\end{ttbox}
See also our above comment in response to reviewer 2. We use now the right bibliography style file
\texttt{model1-num-names.bst} and the appropriate ``author-year''-citation commands, i.e. \texttt{citep/citet}.
\begin{ttbox}
*) P3. Description of Creol seems too short
\end{ttbox}
Now paragraph describing work around Creol is the longest of the
related work section (except discussion on mechanical proofs) 
\begin{ttbox}
*) P3. You are not using a proper reference for the Isabelle Code. This 
   link may disappear in future years and this is not good for a journal 
   that will be read "forever". Could you set up a more institutional site 
   like a technical report page?
\end{ttbox}
We have created an INRIA GForge project to store
in an institutional way and permanently the Isabelle
code. Accessible now there \url{http://gforge.inria.fr/scm/viewvc.php/ASPfun/?root=tods-isabelle}
\begin{ttbox}
*) P3. Mention from now that only 10\% of those lines are used for the 
   language, the rest is proof.
\end{ttbox}
Thanks for helping us clarify this point. We literally mention now this ratio in the text.
\begin{ttbox}
*) P4. "Section 9".. it does not seem proper to include such a discussion 
   in Section 9
\end{ttbox}
We have now adjusted this phrase changing the inappropriate promise of ``explanation''
to ``discussion''.
\begin{ttbox}
*) It seems to me you should switch sections 7 and 8
\end{ttbox}
That is a good observation. We have exchanged the two sections taking care
of referencing which has been necessary only in the overview on page 4, the rest
automatically resolved by Latex.
\begin{ttbox}
*) I'd move Appendix at the beginning of Section 2, as a briefly 
   discussed subsection.
\end{ttbox}
We have moved this appendix as a a subsection an the end of Section 2 
briefly discussing it by a few more explanations that make the 
encodings more apprehensive.
\begin{ttbox}
*) P4. You often use too much punctuation. E.g. in second sentence of 2.1 
   you could avoid all commas. Similarly in other places.
\end{ttbox}
We have eliminated all commas in the cited sentence and have revised the 
punctuation all over the paper. There has been indeed quite an abundance in places
as has been fortunately discovered by this referee.
Since some comma rules are optional, we give here a short summary of the
rules we stick to in this paper now.
We did adopt the rule to separate all leading adverbs (e.g. like in this sentence the ``However''),
to set-off and enclose  non-essential phrases or clauses (e.g. the cat, sniffing the air, ran off.),
and also to separate dependent sentences including participial and infinitive phrases, when they come first. 
We do use the so-called ``Oxford comma'', e.g. one, two, and three (its use is sometimes criticized). 
We also use commas to separate two sentences conjoined by
``and'', ``or'', ``but'' and other conjoints but {\it only if they are independent, i.e., full
sentences} (this is correct although not obligatory). 
We have eliminated all ``comma splices'', e.g., ``I read a book, then I go'' because  
they are incorrect. However, we use ``I do something, i.e., I read'' which could be seen as
a comma splice but also as an enclosed non-essential clause. We sometimes use ``;'' to bind 
sentences that are semantically closely related and we cannot join them by comma because of the comma 
splice restriction.
We did remove now all commas when sentences {\it end} in participial or infinitive dependent phrases,
e.g., ``We describe this rule to enhance the understanding'' or ``Comma rules are strictly applied improving readability''.
%Also sometimes to separate final infinitive and gerund phrases (where it is optional to use a comma).
\begin{ttbox}
*) P29. "This rule.." which one? You should explicitly mention the above 
   one, or put it into a floating figure.
\end{ttbox}
Thanks for pointing out this unclear reference. We now explicitly mention the
above \texttt{request} rule.
\begin{ttbox}
*) P29. Same sentence. "correspond" --> "corresponds"
\end{ttbox}
Done.
\begin{ttbox}
*) P29. Mention why you are discussing rule [REQUEST] only.
\end{ttbox}
We mention now that this rule is representative for the semantics rules.
\begin{ttbox}
*) P35. From your description it seems that simpA is based on Featherweight 
Java, while it is its formalization that is based on FJ. Please update your 
reference with this one: http://dx.doi.org/10.1016/j.scico.2010.06.012
\end{ttbox}
We have adjusted the phrase and accommodated that only the formalisation of
Agents and Artifacts is based on FJ. Thank You as well for the updated reference 
that we now cite instead of the previous one.



\subsection*{Appendix: First review}
In this appendix we add the original mail containing the first response and
demand for a major revision including the comments of all three reviewers and
short inlined comments on how we dealt with them. For an easier access to the main points
of criticism, however, we had in our response to this organized our letter above
following the summary of the editors also given in the first mail below.


\subsubsection*{Response to first review}
Here is the letter we wrote as a response to the first round of reviews.
We addressed directly the major issues kindly summarized from the three
reviews by the editors and appended a selection of some of the more crucial points
from the three reviews and our responses below. Our original working document
inlining responses into the original responses is now added below.
\begin{ttbox}
Dear reviewers and editors,

First of all we would like to thank you for your constructive criticism and
the time you have taken to examine our contribution.
We would like to respond, to the summary of requested changes provided by
the editors. Concerning the detailed comments of the referees, most of them
have been summarized by the editors, others are addressed at the end of this
letter. 
Unfortunately the paper is now about 7 pages longer than before but this
has been inevitable because the first major points of criticism was that we 
did not offer enough original material wrt the FOCLASA paper.


Summary of requested changes as summarized by the editors:

1. new contributions compared to the workshop paper,

We would like to point out that with respect to the FOCLASA version of the
paper, we already had added a substantial different and fairly extensive
second example, plus a detailed account of the work-intensive Isabelle/HOL
formalisation in our original journal submission. 
Nevertheless, we have now added a new section describing a completely new
result: the very recent exchange of the base formalisation by a locally
nameless representation enabling a profound comparison.
The Isabelle/HOL sources concerning the sigma-calculus in Locally Nameless
representation have just been published in the Archive of Formal Proofs at: 
http://afp.sourceforge.net/entries/Locally-Nameless-Sigma.shtml.

To make the new original material easier to spot, we have now also pointed
out the contributions and the novelties more clearly in the beginning
(List of contributions) and in the related work section (Positioning).

2. slight extension of an existing calculus (addition of active objects
to the sigma-calculus)

We feel that this is a misunderstanding caused by not sufficiently pointing
out the differences. We have added a short explanation to the
exposition now. To summarize, the syntactical extension is minor but 90% of the
semantics and results are new, thus most of the paper inherits only
the syntax and preliminary useful results from sigma calculus.

3. interest of this work for distributed computing,

The motivating Section Introduction has now a clearly marked
subsection explaining what aspects of distribution are addressed by the
calculus. Roughly, ASPfun is distributed like an actor-language. Our
aim here is not to tackle issues such as, e.g., fault tolerance or
unreliable communication, but the notion of activity, and requests,
clearly provides a convenient abstraction for studying such
distributed aspects.

4. paper too technical

Where possible, for example when introducing the first case study, we
give a gentler introduction. Since the novel contribution of the
extended version is giving a fairly detailed experience report on the
Isabelle/HOL-formalisation, we could not avoid being technical to some
extent. We believe that we have managed to keep the technical parts as
independent as possible from the real technical implementation detail.
For example, the description of the proofs in 6.2 is almost completely
independent of Isabelle.
The current extension by the locally nameless representation shows this as
well: we have only introduced the central interesting changes to the core
syntax in verbatim but the conceptual idea of cofinite induction stays at an
abstract mathematical level, just like the already existing properties.

5. presentation should be improved.

We spent considerably effort and time on clarifying the presentation of our
paper. For example, we have improved the display of the code segments. The
organisation of the exposition has undergone various improvements taking up
the constructive criticism of the reviewers. For example, there is a
separate section on the relationship to the Actor paradigm and distributed
languages. Similarly, preliminary concepts and ideas for alternative
semantics are now arranged in a separate discussion section. Also, we
simplified the examples and added soft introductions wherever possible.


We detail below the few points that were raised by the reviewer, and
that we solved differently from the reviewers requirements
(except also comments mentioned above):

review 1
- The part of the paper which is reused and
  the one which is new has to be stated clearly. As a concrete
  example, I wondered whether parts of Theorem 1 were inherited from
  the work on the sigma calculus.
-> It seems to us that Theorem 1 is very far from sigma calculus: 
   Theorem 1 deals with absence of cycle between remote references 
   (active object and futures); this Theorem is meaningless in sigma 
   calculus, and the proof is thus independent from classical
   sigma calculus; sorry we do not see how to answer this question.


- on page 18, the authors use the word "value" to refer to a totally
  evaluated term, I would have called it "constant".
--> In AC: v_i is called result! by similarity and as value appeared to
    be confusing we changed it to result which expresses quite well the
    notion "entirely evaluated term"

- at the end of Section 5, I wondered what other properties the
  authors could have proved on the proposed calculus. If there are
  more they could be mentioned here.
->  Addressed now, but in conclusion (we could move it if required)



review 2:
- p.5: "Proving determinism for ASP_fun is difficult ..."  I'm not sure
  what you mean by "determinism" but it doesn't seem very difficult to
  show confluence.
-> switched to confluence: in fact proving confluence is not so easy
   due to technical difficulties (duplicated activities and
   futures), in a theorem prover, the proof would probably require several
    months of Isabelle development.
   Seeing that ASPfun is confluent is trivial though (we added
   a paragraph explaining this).

- p.10: map\_obj takes f, which is used as a label, as an argument!
-> Thanks for pointing out this mistake. We have corrected it by
   making the label name a fixed part of the map definition. So,
   the map is specific to the "start" method of "init" now.

- p.28: The formalized statement of Subject Reduction isn't the same as 
  the one on page 18 (there is no condition on CT').  Why?
-> We agree on this point. To clarify things,  we have now, in the new locally
   nameless formalisation explicitly
   formalized the submap relation on the configuration types and adapted
   the theorem (the witnesses provided in this existential proof are such
   that they fulfill the submap condition but we did not make this explicit
   in the previous version because it was not needed for proving progress).


review 3:
- Fig 1: I think you could show f\_k even in the right box.. as the
  "name" of the future of such an activity
-> Not addressed because this raises a question : 
   do you mean that we should change our notations and
   annotate each request with the name of the future it computes? 
   This is possible but would require to change the notations in all
   figures to be consistent.
   In fact, we did not do it for the moment because we are afraid this
   would make the figures too heavy, with a lot of additional
   annotations.
   Of course, we do not mind adding the f_ks in all figures if the
   reviewers confirm this is what they would suggest and they
   think it improves the readability of the paper.

- The main doubt I have with this operational semantics is due to rule
  [REPLY]: it seems that as soon as a method invocation is performed, 
  [REPLY] is always enabled, hence you always have REQUEST/REPLY loops. 
  Is that right? If it is, please mention this esplicitly, and discuss
  the impact of this aspect on termination, deadlock, 
  and so on more esplicitly than you do now.
-> sorry we do not understand, if it means you can have infinite loops
  in the calculus YES: it is possible to implement a while(true) loop.
  If in this while(true) you do requests you would have infinitely many
  request/replies of course but is not that the expected semantics?
  Else once a remote invocation is performed, even if the reply is
  immediate, it does not result in the same remote invocation performed
  again, but rather in the execution of the method body.
  Anyway the fact that REPLY is enabled immediately after request is
  true and has been added as suggested in the rule description.
  We will be happy to add further details on this aspect, as soon as
  we have a little detail from the reviewer.

- "We omit details.." --> if such definitions are not too long, I 
  would again incorporate them here, for the sake of self-containment
-> We have added those definition as appendix; so it is visible that
they are not the contribution of the paper but make it better
self-contained. If the reviewers find this is too long it is easy to
remove them.
\end{ttbox}


\subsection*{First review: orginal list of reviewer's comments with annotations}
For inlining our changes into the review report, we used either \texttt{XX} 
in front of a short comment or a \texttt{-> Done} after this comment to signify
that this evident change had been done, or we used \texttt{->} followed by a 
comment on our correction after a reviewer's comment whenever our change involved 
further explanation. We add this transcript here only to show that we did not ignore 
certain comments -- as it did occur to reviewer one -- but on the contrary have 
worked through the entire list.
\begin{ttbox}
We have received all the reviews for your paper submitted to the
Foclasa'09 special issue to be published in Science of Computer
Programming (Elsevier). The first results decision is "major
revision". Please find at the end of this message the detailed comments 
made by the reviewers. The main comments are the following
ones:

- new contributions compared to the workshop paper,
-> added+detailed
- slight extension of an existing calculus (addition of active objects
to the sigma-calculus),
-> syntactically YES but SEMANTICALLY, big improvement
- interest of this work for distributed computing,
-> better discussed, it is distributed in the same way as actors 
   at least it should be of interest for parallel computation
- paper too technical,
-> why is this bad? improved explanations
- presentation should be improved.
-> careful proofread and improved organisation

Please take all the comments made by the reviewers into account and
send us a revised version (using the EES system) by May 15, 2010. It
is also required to prepare a letter to reply reviewers' comments and
explain how your paper has been revised to take them into
account. Note that your revised version will go through a second round
of review before taking a final decision.

To submit a revision, please go to http://ees.elsevier.com/scico/ and login 
as an Author. Your username is: flokam
If you need to retrieve password details, please go to:
http://ees.elsevier.com/scico/automail_query.asp

On your Main Menu page is a folder entitled "Submissions Needing Revision". 
You will find your submission record there. 
If you have any comment or question, please send us an email. Thank you again 
for submitting your paper to this SCP special issue.

Reviewers' comments:

Reviewer 1: 
This paper presents a calculus of functional objects called
ASPfun. ASPfun is an extension of the sigma calculus, originally
proposed by Abadi and Cardelli. The authors not only formalise the
syntax and semantics of this language but also prove some properties
on this calculus such as the absence of cycles and the progress
property. Proofs have been carried out with the help of the
Isabelle/HOL theorem prover.

Please find below some comments: 
- at the beginning of the article, there are some sentences that
  should be clarified, here are some examples: 
XX(page 2) "ASPfun  enables the correlation ... static analysability", 
XX (page 4)
  "requests can be treated in an unordered fashion" why?, 
XX(page 4)
  "the most parallel evaluation ... in the queue", 
(page 4) 
XX"future values must be stored for ever", 
XX(page 5) "... because this would
  ...  with an active object", 
XX(page 5) "... Informally, depending
  ... is deterministic", 
~~(page 5) "... implementing
  strictly... execution paths",
-> MOVED  TO CONCLUSION

 etc. More details should be given to
  make clearer these different statements.

- as mention the authors, the extension of the sigma calculus is
  minimal (only one additional primitive). Since this extension
  concerns only one feature, what parts of the paper is really new?  I
  suppose that the whole part concerning the sigma calculus should be
  reused from other papers. 
-> Almost everything except the syntax is new, two rules are
   inherited, but adapted, as mentioned in the text the local type
   system is adapted from Abadi and Cardelli
-> ADDED Note that while the syntactic extension compared to sigma-calculus is
   minimal, the semantics that we will define in the following is
   (almost) entirely new. For example in Table 2, only
   the two first rule are an adaptation of sigma-calculus' semantics, all
   the others are specific to ASPfun. 

The part of the paper which is reused and
  the one which is new has to be stated clearly. As a concrete
  example, I wondered whether parts of Theorem 1 were inherited from
  the work on the sigma calculus.
-> NOT ADDED because Theorem 1 deals with absence of cycle between
   remote references (active object and futures); this theorem is
   meaningless in sigma calculus, sorry we do not see how to answer this question

- as regards the Foclasa'09 paper, it seems that the authors have
  added more explanations, but I do not see what part of the work is
  new. The idea of the special issue is to add new material,
  scientifically speaking, wrt. the workshop paper, and not only more
  pages in the paper. The authors should say somewhere, in the
  introduction or in the related work section for instance, what is
  new here wrt. their Foclasa'09 paper.
-> DONE

- on page 18, the authors use the word "value" to refer to a totally
  evaluated term, I would have called it "constant".
-> In AC: v_i is called result! by similarity and as value appeared to
  be confusing we changed it to result which expresses quite well the
  notion "entirely evaluated term"

- The paper (especially in the introduction) mentions several times the
  absence of deadlocks, I found it confusing and only understood on
  page 19 what the authors meant. This is just a consequence of the
  progress property. The absence of deadlocks should be either better
  explained in the introduction, or introduced later on in the paper
  when the progress property is more precisely presented.
-> DONE (it was in the paper organisation)


- at the end of Section 5, I wondered what other properties the
  authors could have proved on the proposed calculus. If there are
  more they could be mentioned here.
->  Done: restored and rewrote the future work paragraph in conclusion

- I found the discussion on binders too long, and I did not see the
  point of such a discussion. It could have been presented in a
  shorter way.
-> Done: added the new Section 6.4 on the second LN-formalisation
  that gives a good motivation for the detailed introduction of the
  different binder concepts and is a necessary basis to follow the
  comparison. 

- what is the difference between this work and the work published in a
  FACS'08 paper on futures where some deadlocks may occur and be
  detected? A short comparison should be added in the related work  section.
-> DONE
- on page 29, the authors mention 14000 lines of code. What is this
  code exactly? Does it correspond to the formalisation of the
  calculus, or are all the proofs taken into account as well? What
  part was written manually, and what part was automatically generated
  by Isabelle? How long did it take to achieve such a formalisation
  and proofs? Related to that, the sentence "the formalisation is
  relatively long" on page 32 should be clarified.
-> Done: amendment in the intro to 6: 
  "Nevertheless -- unlike model checking or other fully automated ....
   user has to find ... proofs ... himself."
-> DONE: refined 6.4.4: "An experience  ..."


- "ASPfun is in its foundation not dissimilar to the pi-calculus, they
  are different calculi" (page 31): please rewrite this sentence, it
  is not clear, and the second part does not really help.
-> Done

- "A few drawbacks could be found in the semantics...": it would be
  interesting to give a critical view of the work at hand in the
  conclusion, and explicitly mention these drawbacks.
-> Done

Reviewer 2: I appreciate the addition of detailed discussions on formal 
proofs and examples.  At the same time, however, I'm disappointed that the
present manuscript doesn't sufficiently address an important question:
how this calculus can be considered a calculus for distributed
computation.  The introduction says "The calculus is called
distributed because it ensures absence of sharing between activities
(process) allowing them to be placed on diffirent machines, ..."  but
I don't buy this argument.  I believe distributed computation is about
how we should compute in a setting where computational resources (such
as CPU, memory, and data) are distributed and localized, and not about
how we can divide a big task into small tasks that can run in parallel
(which is an interesting question, BTW).  What you say would amount to
the lambda-calculus (or any purely functional calculus) is a calculus
for distributed computation.

Given the purely functional nature of the calculus, the technical
result of subject reduction and progress is not surprising, since any
configuration has a corresponding "full local" configuration
consisting of a single location with the empty request queue.

So, although the paper is technically plausible, its contribution to
understanding distributed computation is not clear and I had to
recommend reject.

Minor comments:
XXp.2, bottom: "small step" -> "small-step"

p.5: "Proving determinism for APS_fun is difficult ..."  I'm not sure
what you mean by "determinism" but it doesn't seem very difficult to
show confluence.
-> switched to confluence


p.9: "(s.serve) (x.data)" You should note that this is a function
application, which is a derived form.
-> We added a note explaining how application is modelled by the second
   parameter 


p.9: "server \_equiv ... "  What is sigma here?
XX

p.10: map_obj takes f, which is used as a label, as an argument!
-> Done: Thanks for pointing out this mistake. We have corrected it by
   using making the label name a fixed part of the map definition. So,
   the map is specific to the "start" method of "init" now.

p.10: What is "symb" in the definition of \_Lambda?
-> Done

p.24: \_not \_exists -> \_neg \_exists
-> Done

p.28: The formalized statement of Subject Reduction isn't the same as 
the one on page 18 (there is no condition on CT').  Why?
-> We have now, in the new locally nameless formalisation explicitly
   formalized the submap relation on the configuration types and adapted
   the theorem (the witnesses provided in this existential proof are such
   that they fulfill the submap conditionbut we did not make this explicit
   before because it was not needed for progress).


XXp.34: [21] needs the information on where it appeared.

XXp.34: [27] contains the symbol for "Section".


Reviewer 3: The defined ASPfun calculus appears as a nice trade-off between the 
expressiveness required to model foundational aspects of remote method invocation, 
and the minimality needed to reason about this formally. The discussion about 
Isabelle formalisation is valuable, although it can and should be improved as 
detailed below. In particular, this paper very often becomes quite too technical, 
perhaps too much for someone not specifically acquainted with sigma-calculus. 

Abstract:

XX"well structured" --> what that mean? please rephrase

XX "This paper... . Then we provide.." --> please, use same style in this paragraph.
XX"ASPfun and its properties.. have been formalised and proved" --> "proved" works 
for properties but not for ASPfun itself.. please rephrase

Intro:

XX "The language and.." --> Please, summarise in introduction the main facts about 
your Isabelle formalisation, such as proof length.

"A proposal for a type system.." --> why a proposal? can there be many such? 
is this described later?
-> We did not think it would be confusing but nevvertheless removed proposal

Please mention the FOCLASA 2009 paper and what is new in this journal submission.
Done: two more paragraphs in 7. Positioning

Could you clarify ASP vs sigma-calculus? Where ASP comes from since
you are extending \_sigma?
-> Done (asynchronous sequential processes)

2.2:
Fig 1: I think you could show f\_k even in the right box.. as the "name" of 
the future of such an activity
-> Question : do you mean that we should change our notations and
annotate each request with the name of the future it is computed. This
is possible but would require to change the notations in all figure,
and we wonder if this would not make the figures too heavy (?)

"the calculus is functional" --> can you somewhere describe what your calculus 
is good for in the case the system to be modelled involves side-effects?
-> clarified through additional section on actors

In 2.2 you describe Fig 1, but the meaning of E[] is introduced only in 2.3.
-> REMOVED E[...]


"must be stored forever" --> that is, you cannot garbage collect anything.. has 
this practical impact as far as open systems are concerned?
-> DONE: explain that GC is possible but not done here

"The only restriction is that.." --> why? can you make an example?
-> added example DONE

"Practically, implementing strictly.." --> this does not sound good. If an actual 
implementation **must** rely on a different semantics, then the relevance of this 
paper diminishes.. Please clarify and move part of this discussion in intro.
-> moved to discussion and explained in letter

2.3
"Because it gives.. --> this kind of arguments are quite hard to follow for someone 
not acquianted with [1], please try to be self-contained.
"noFV" requirement.. .-> see above
-> Done added introductory paragraph

"To simplify the reduction.." --> If you introduce complex syntax then do so by a 
grammar and create a table for it.

~~The main doubt I have with this operational semantics is due to rule [REPLY]: it 
seems that as soon as a method invocation is performed, [REPLY] is always enabled, 
hence you always have REQUEST/REPLY loops. Is that right? If it is, please mention 
this esplicitly, and discuss the impact of this aspect on termination, deadlock, 
and so on more esplicitly than you do now.
-> sorry we do not understand: if it means you can have infinite loops
in the calculus YES: it is possible to implement a while(true) loop
fortunately.
If in this while(true) you do requests you would have infinitely many
request/replies of course but is not that the expected semantics?
Anyway the fact that REPLY is enabled immediately after request is
true and added in the rule description.

XX I would suggest creating a 2.4 on "Discussion": keep in 2.3 only 
the discussion of what your operational semantics does, and put in 2.4 
comparison with [1].

3.1 

 The example is quite hard to understand (even though I finally did
   it). Please be more gentle in its description. (Also do this for next
   example) 
-> Done: First example is introduced in a very gentle way now. Second
   example made simpler and also with verbose introduction.

3.2 

 In your definition of client I'm wondering whether parenthesis should go 
instead like: start= ..(x,s)(s.serve(x.data))
-> YES OOPS, Done

XX In server you use \_sigma.. but what is this?

XX"We omit details.." --> if such definitions are not too long, I would 
again incorporate them here, for the sake of self-containment
-> Done: no detail omitted, added an appendix for base definitions in sigma

XX"symb(client.." --> "Active(client.." ????
\end{ttbox}
\end{document}
