(TeX-add-style-hook "GCMOperationalSemantics"
 (lambda ()
    (LaTeX-add-bibliographies
     "biblio"
     "Oasis")
    (LaTeX-add-environments
     "theorem"
     "lemma")
    (LaTeX-add-labels
     "sec:GCM"
     "fig:Sem:CompositeExample"
     "sec:informal-semantics"
     "sec:component-definition"
     "sec:components"
     "fig:Primitive1Typed"
     "fig:Sem:MembraneShortcuts"
     "sec:well-form-comp"
     "sec:reconfig"
     "sec:pnets"
     "section:pNets"
     "pLTS"
     "sec:more-details-term"
     "sec:behav-sem-gcm"
     "sec:sem-prim-comp"
     "fig:Sem:PrimitiveComponent1"
     "pagePrimsem"
     "tab:SVPrim"
     "pagebody"
     "ATG:Sem:Body"
     "sec:model-future-prox"
     "fig:Sem:FutureProxies"
     "sec:semant-comp-comp"
     "fig:Sem:pNetCompositeExample"
     "pagecompsem"
     "fig:Sem:atgDelegAndPF1"
     "tab:SVComp"
     "tab:SVBind"
     "sec:advanced-features"
     "section:firstClassFutures"
     "fig:png:firstClassTransmission"
     "fig:ProxyFutureTransmission"
     "fig:ProxyFutFirstClass"
     "pagePrimsemF1"
     "tab:SVF1P"
     "tab:SVF1C"
     "sec:validation"
     "sec:theoremss"
     "def-unsynch"
     "thm:synchro"
     "sec:full-example"
     "fig:Sem:FullStructure"
     "fig:Sem:LocalHM"
     "fig:Sem:GlobalHM"
     "fig:Sem:ToplevelHM"
     "sec:related-work")
    (TeX-add-symbols
     '("proofcase" 1)
     '("PNfamily" 2)
     '("BC" 2)
     '("subst" 1)
     '("rulelabelOne" 1)
     '("rulelabel" 1)
     '("IGNOREmulticast" 1)
     '("IGNOREgathercast" 1)
     '("TODOlight" 1)
     '("Remarque" 1)
     '("TODO" 1)
     '("symb" 1)
     "rulecounterreset"
     "I"
     "Prim"
     "Comp"
     "Component"
     "QName"
     "SItf"
     "CItf"
     "Itf"
     "CName"
     "SM"
     "MC"
     "CM"
     "Let"
     "In"
     "Target"
     "MaxBinding"
     "dbllbrack"
     "dblrbrack"
     "mylangle"
     "myrangle"
     "Queue"
     "pNet"
     "Act"
     "fv"
     "iv"
     "WF"
     "GRef"
     "FC"
     "SML"
     "textlabel"
     "oldmapsto"
     "paragraphe")
    (TeX-run-style-hooks
     "ntheorem"
     "thmmarks"
     "enumitem"
     "subfigure"
     "color"
     "epsfig"
     "listings"
     "longtable"
     "psfrag"
     "url"
     "graphicx"
     "mathpartir"
     "mathtools"
     "stmaryrd"
     "amsmath"
     "amssymb"
     "inputenc"
     "utf8x"
     "latex2e"
     "elsarticle10"
     "elsarticle")))

