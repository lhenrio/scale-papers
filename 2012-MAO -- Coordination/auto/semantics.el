(TeX-add-style-hook "semantics"
 (lambda ()
    (LaTeX-add-index-entries
     "parallel reduction")
    (LaTeX-add-environments
     '("rules" 3))
    (LaTeX-add-labels
     "#2"
     "sec:semantics"
     "gls-synt-seqsyntax"
     "sec:semanticsrule"
     "sec:scheduling-requests")
    (TeX-add-symbols
     '("IS" 1)
     '("subst" 1)
     '("gris" 1)
     "FEq"
     "LR"
     "PR"
     "dbllbrack"
     "dblrbrack"
     "Rc"
     "lobj"
     "loc"
     "act"
     "request"
     "serveoldest"
     "serveoldestll"
     "eos"
     "reply"
     "update"
     "inv"
     "field"
     "selfrequest"
     "selfreply"
     "llab"
     "capt"
     "Rfut"
     "Rp")))

