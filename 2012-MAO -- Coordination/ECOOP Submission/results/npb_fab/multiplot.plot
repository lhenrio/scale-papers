set term pdf color size 10,4 font "Helvetica,10"
set output "npb.pdf"
set size 0.94,1

set multiplot

set size 0.52,0.9
set origin 0.0,0.0

set xlabel "# of threads"
set ylabel "Runtime (s)"
#set xtics (8, 16, 32)
set xtics (2, 4, 8, 16, 24, 32)
set xrange [0:33]
set yrange [0:2500]

plot "multiactive_SP_C" using 1:9:10 with yerrorline lt 1 lw 2 title "ma-SP",\
	 "original_SP_C" using 1:9:10 with yerrorline lw 2 title "SP",\
	 "multiactive_BT_C" using 1:9:10 with yerrorline lw 2 title "ma-BT",\
	 "original_BT_C" using 1:9:10 with yerrorline lw 2 title "BT",\
	 "multiactive_LU_C" using 1:9:10 with yerrorline lt 9 lw 2 title "ma-LU",\
	 "original_LU_C" using 1:9:10 with yerrorline lt 8 lw 2 title "LU"
	 
set size 0.51,0.9
set origin 0.50,0.0
set xlabel "# of threads"
unset ylabel
set xtics (2, 4, 8, 16, 24)
set xrange [0:25]
#set yrange[:1000]


plot "IS" every ::1 using 2:7:9 with yerrorline lw 2 title "ma-IS",\
	 "IS"  every ::1 using 2:3:5 with yerrorline lw 2 title "IS",\
	 "MG" every ::1 using 2:7:9 with yerrorline lw 2 title "ma-MG",\
	 "MG" every ::1 using 2:3:5 with yerrorline lw 2 title "MG",\
     "CG"  every ::1 using 2:7:9 with yerrorline lw 2 title "ma-CG",\
	 "CG"  every ::1 using 2:3:5 with yerrorline lw 2 lt 9 title "CG",\
	 "FT" every ::1 using 2:7:9 with yerrorline lw 2 title "ma-FT",\
         "FT"  every ::1 using 2:3:5 with yerrorline lw 2 title "FT"
