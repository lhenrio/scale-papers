set term postscript enhanced color "Courier" 26

set xlabel "# of peers"
set ylabel "Runtime (s)"
set xtics (4, 32, 64, 128, 256, 512)

set output "../../fig/allfrom.ps"
plot "all/aggr_sao" using 2:($3/1000000000) with linespoints title "All from one", \
	 "all/aggr_mao" using 2:($3/1000000000) with linespoints title "All from one (multi-active)", \
	 "alltwo/aggr_sao" using 2:($3/1000000000) with linespoints title "All from two" , \
	 "alltwo/aggr_mao" using 2:($3/1000000000) with linespoints title "All from two (multi-active)"

set output "../../fig/onefrom.ps"
unset ylabel
plot "center/aggr_sao" using 2:($3/1000000000) with linespoints title "Center from all" , \
	 "center/aggr_mao" using 2:($3/1000000000) with linespoints title "Center from all (multi-active)", \
	 "corner/aggr_sao" using 2:($3/1000000000) with linespoints title "Corner from all", \
	 "corner/aggr_mao" using 2:($3/1000000000) with linespoints title "Corner from all (multi-active)"
