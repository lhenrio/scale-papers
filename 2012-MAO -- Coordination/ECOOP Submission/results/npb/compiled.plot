#set term postscript enhanced color "Courier" 26
set term pdf font "Helvetica, 11"
set xlabel "# of threads"
set ylabel "Runtime (s)"
set xtics (8, 16, 32)
set xrange [6:34]
set  key left at graph 0.3,0.97

set output "../../fig/compiled_kernels.pdf"
plot "compiled_SP_MAO" using 3:11:12 with yerrorline lt 1 lw 2 title "ma-SP",\
	 "compiled_SP_ORIG" using 3:11:12 with yerrorline lw 2 title "SP",\
	 "compiled_BT_MAO" using 3:11:12 with yerrorline lw 2 title "ma-BT",\
	 "compiled_BT_ORIG" using 3:11:12 with yerrorline lw 2 title "BT",\
	 "compiled_LU_MAO" using 3:11:12 with yerrorline lt 9 lw 2 title "ma-LU",\
	 "compiled_LU_ORIG" using 3:11:12 with yerrorline lt 8 lw 2 title "LU"
