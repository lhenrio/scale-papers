synchronized (this) {
  for (int m = 0; m < num_threads; m++)
    synchronized (worker[m]) {
      worker[m].done = false;
      worker[m].notify();
    }
  for (int m = 0; m < num_threads; m++)
    while (!worker[m].done) {
      try {
        wait();
      } catch (Exception e) {}
      notifyAll();
    }
}
