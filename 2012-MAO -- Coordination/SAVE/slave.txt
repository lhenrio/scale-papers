for (;;) {
  synchronized (this) {
    while (done == true) {
      try {
        wait();
        synchronized (master) {
          master.notify();
        }
      } catch (Exception e) {}
    }
    //do work here...
    doWork();
    synchronized (master) {
      done = true;
      master.notify();
} } }
