rm aggr*
for nodes in 4 8 16 32 64 128 256 512
do 
	echo -n "MAO, $nodes, " >> aggr
	for p in 1 2 3 4 5
	do
		head -n 1 log1_*_MAO_${nodes}_50_1024.$p | tr '\n' ',' >> aggr
	done
	echo "" >> aggr
	
		echo -n "SAO, $nodes, " >> aggr
	for p in 1 2 3 4 5
	do
		head -n 1 log1_*_SAO_${nodes}_50_1024.$p | tr '\n' ',' >> aggr
	done
	echo "" >> aggr
done

cat aggr | grep MAO > aggr_mao
cat aggr | grep SAO > aggr_sao
