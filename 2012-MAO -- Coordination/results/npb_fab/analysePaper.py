from math import sqrt
import os
import glob

def stats(data):
    sum = 0.0
    for value in data:
        sum += value
    mean = sum/len(data)
    sum = 0
    for value in data:
        sum = (value - mean)*(value-mean) + sum
    variance = sum/(len(data) - 1)
    return(mean, sqrt(variance))

def analyseFile(file):
    f=open(file,'r')
    times=[]
    for line in f:
        if line.find('Time in')>0:
           # print line
            times.append(float(line.split()[5].replace(",",".")))
            #for i in times:
            #    print i
    (m, v) = stats(times)
    times.append(m)
    #times.append(',')
    times.append(v)
    #print "Mean",m, " Std Deviation ",v
    return times

def prettyPrint(output, results):
    print output
    f=open(output, 'w')
    
    keylist=results.keys()
    keylist.sort()
   # print keylist
    for key in keylist:
        print "xx%dxx" % key
        print >>f,'%d,'%key,
        for v in results[key]:
            print >>f,"%f,"%v,
        print >>f,''    


#Used to analyse files of npb, extract 
#individual time and compute the average
#and std deviation
#output is stored to a file with name
# type_bench_class
def analyseAll(filePattern):
    results={}
    output=""
    #process all files
    #check if original
    if filePattern.find('original') > 0 :
        index = 3
        output="original_"
    else:
        index = 2
        output="multiactive_"
    infile = glob.glob( os.path.join('.', filePattern) )[0]
    output+=infile.split('_')[index-1]+"_"
    output+=infile.split('_')[index+1]
    print output
    for infile in glob.glob( os.path.join('.', filePattern) ):
      
    #    print output
        print "current file is: " + infile
        nb_threads=int(infile.split('_')[index])
    #    print nb_threads
        results[nb_threads]=analyseFile(infile)
    prettyPrint(output, results)
#    print results.keys()
   
def prettyPrintPercentageWithStdDev(output, results, stddev):
    print output
    f=open(output, 'w')
    
    keylist=results.keys()
    keylist.sort()
    # print keylist
    for key in keylist:
        print "xx%dxx" %key
        print >>f,'%d,'%key,
        print >>f,'%f,' %results[key],
        print >>f,'%f' %stddev[key],
        print >>f,''


def prettyPrintPercentage(output, results):
    print output
    f=open(output, 'w')
    
    keylist=results.keys()
    keylist.sort()
    # print keylist
    for key in keylist:
        print "xx%dxx" % key
        print >>f,'%d,'%key,
        print >>f,'%f' %results[key],
        print >>f,''

#analyse SP BT and LU
#data are located in two files
#values for 32 threads are ignored
def analysePercentage_newBench(benchName):
    original_file=open("original_"+benchName+"_C",'r')
    mao_file=open("multiactive_"+benchName+"_C", 'r')
    results_original={}
    results_mao={}
    stddev_original={}
    stddev_mao={}
    for l in original_file:
        #print l
        results_original[int(l.split(',')[0])] = float(l.split(',')[8])
        stddev_original[int(l.split(',')[0])] =  float(l.split(',')[9])
    for l in mao_file:
        results_mao[int(l.split(',')[0])] = float(l.split(',')[8])
        stddev_mao[int(l.split(',')[0])] = float(l.split(',')[9])
    print "Java       : ", results_original
    print "StdDev Java: ", stddev_original
    print "MA         :", results_mao
    print "MA Java    : ",stddev_mao
    results={}  
    stddev={}
    keylist=results_original.keys()
    keylist.sort()
        # print keylist
    for key in keylist:
        if (key != 32):
            results[key]=(results_mao[key]-results_original[key])/results_original[key]*100
            stddev[key]=(results_mao[key]+stddev_mao[key]-results_original[key]-stddev_original[key])/(results_original[key]+stddev_original[key])*100
            print key, " :  ", results[key], stddev[key]
    prettyPrintPercentageWithStdDev("percentage_"+benchName+"_C",results, stddev)

def analysePercentage_oldBench(benchName):
    file=open(benchName,'r')
    results_original={}
    results_mao={}
    stddev_original={}
    stddev_mao={}
    for l in file:
        #print l
        results_original[int(l.split(',')[1])] = float(l.split(',')[2])
        stddev_original[int(l.split(',')[1])] = float(l.split(',')[4])
        results_mao[int(l.split(',')[1])] = float(l.split(',')[6])
        stddev_mao[int(l.split(',')[1])] = float(l.split(',')[8])
    print "Java       : ", results_original
    print "StdDev Java: ", stddev_original
    print "MA         :", results_mao
    print "MA Java    : ",stddev_mao
    results={}  
    stddev={}
    keylist=results_original.keys()
    keylist.sort()
    # print keylist
    for key in keylist:
        results[key]=(results_mao[key]-results_original[key])/results_original[key]*100
        stddev[key]=(results_mao[key]+stddev_mao[key]-results_original[key]-stddev_original[key])/(results_original[key]+stddev_original[key])*100
        print key, " :  ", results[key], stddev[key]
    prettyPrintPercentageWithStdDev("percentage_"+benchName+"_C",results, stddev)


#should be run after analyseAll!
def analysePercentage(benchName):
    newBench=["SP","BT","LU"]
    if (benchName in (i for i in newBench)):
        analysePercentage_newBench(benchName) 
    else:
        analysePercentage_oldBench(benchName)


analysePercentage("SP")
analysePercentage("BT")
analysePercentage("LU")
analysePercentage("IS")
analysePercentage("CG")
analysePercentage("FT")
analysePercentage("MG")


#analyseAll('*fr_original_LU_*')
#analyseAll('*fr_LU_*')

#analyseAll('*fr_original_BT_*')
#analyseAll('*fr_BT_*')

#analyseAll('*fr_original_SP_*')
#analyseAll('*fr_SP_*')


