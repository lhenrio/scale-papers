set term pdf color size 10,4 font "Helvetica,9"
set output "npbPercentage.pdf"
set size 0.94,1

set multiplot

set size 0.52,0.9
set origin 0.0,0.0

set xlabel "# of threads"
set ylabel "Overhead (%)"
#set xtics (8, 16, 32)
set xtics (2, 4, 8, 16, 24)
set xrange [0:25]
#set yrange [0:2500]
set yrange[-10:10]

set style line 1 lt 2 lw 3 pt 3 lc rgb "grey"

plot "percentage_SP_C" using 1:2 with lp lt 1 lw 2 title "SP",\
	 "percentage_BT_C" using 1:2 with lp lw 2 title "BT",\
         0 with line ls 1 notitle,\
	 "percentage_LU_C" using 1:2 with lp lt 9 lw 2 title "LU"
	 
set size 0.51,0.9
set origin 0.50,0.0
set xlabel "# of threads"
unset ylabel
set xtics (2, 4, 8, 16, 24)
set xrange [0:25]
#set yrange[:1000]
set yrange[-4:10]
set key at graph 0.88,0.97

plot "percentage_IS_C" using 1:2 with lp  lw 2 title "IS",\
	 "percentage_MG_C" using 1:2 with lp  lw 2 title "MG",\
	 "percentage_CG_C"  using 1:2 with lp  lw 2 lt 9 title "CG",\
         0 with line ls 1 notitle,\
         "percentage_FT_C"  using 1:2 with lp  lw 2 title "FT"
