#set term postscript enhanced color "Courier" 26
set term pdf font "Helvetica, 11"
set xlabel "# of threads"
#set ylabel "Runtime (s)"
set xtics (1, 2, 4, 8, 16, 24)
#set yrange[:1000]

set output "../../fig/normal_kernels.pdf"
plot "IS" using 2:7:9 with yerrorline lw 2 title "ma-IS",\
	 "IS" using 2:3:5 with yerrorline lw 2 title "IS",\
	 "MG" using 2:7:9 with yerrorline lw 2 title "ma-MG",\
	 "MG" using 2:3:5 with yerrorline lw 2 title "MG",\
     "CG" using 2:7:9 with yerrorline lw 2 title "ma-CG",\
	 "CG" using 2:3:5 with yerrorline lw 2 lt 9 title "CG",\
	 "FT" using 2:7:9 with yerrorline lw 2 title "ma-FT",\
	 "FT" using 2:3:5 with yerrorline lw 2 title "FT"
