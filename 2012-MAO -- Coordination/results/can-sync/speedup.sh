awk '{FS=","
  parall=$3
  getline < "center/aggr_sao"
  threads=$2
  single=$3
  print threads" "single/parall
} ' center/aggr_mao > center/speedup

awk '{FS=","
  parall=$3
  getline < "alltwo/aggr_sao"
  threads=$2
  single=$3
  print threads" "single/parall
} ' alltwo/aggr_mao > alltwo/speedup
