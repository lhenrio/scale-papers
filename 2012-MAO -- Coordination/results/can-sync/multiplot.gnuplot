#set term png
#set term postscript enhanced color "Helvetica" 15 size 10,4
set term pdf color size 10,4 font "Helvetica,9"
set output "can.pdf"
set size 0.94,1


set multiplot

set xlabel "# of peers"
set ylabel "Runtime (s)"
set xtics (4, 32, 64, 128, 256)
set xrange [4:256]
set yrange [0:500]
set ytics nomirror
set y2range[0:3]
set y2tics (1,2,3)
set size 0.52,0.9
set origin 0.0,0.0
set  key Left reverse left at graph +0,0.97
plot  "alltwo/aggr_sao" using 2:($3/1000000000) with lp lt 1 lw 2 title "All from two (single-active)" , \
	 "alltwo/aggr_mao" using 2:($3/1000000000) with lp lt 3 lw 2 title "All from two (multi-active)",\
         "alltwo/speedup" using 1:2 with lp lt 4 lw 2 axes x1y2 title "Speedup"

#set yrange[:250]
set xtics (4, 32, 64, 128, 256)
set xrange [4:256]
set yrange [0:800]
#set y2range[:10]
set y2range[0:16]
set y2tics (2,4,6,8,10,12,14,16)
set y2label "Speedup"

set size 0.52,0.9
set origin 0.50,0.0

set  key  left at graph 0,0.98

unset ylabel
plot "center/aggr_sao" using 2:($3/1000000000) with lp lt 1 lw 2  title "Centre from all (single-active)" , \
      "center/aggr_mao" using 2:($3/1000000000) with lp lt 3 lw 2 title "Centre from all (multi-active)",\
         "center/speedup" using 1:2 with lp lt 4 lw 2 axes x1y2 title "Speedup"
	
set nomultiplot
