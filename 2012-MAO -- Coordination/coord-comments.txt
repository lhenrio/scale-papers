--- Reviewer 1 ---

1) In a distributed setting, a question should be answered: 
   *How the local maximal thread utilization of a multi-active object can
   help maximize performance in a distributed network of active objects?* 
   There is a short implication on the evaluation section discussion but
   this should be made more clear.
ZSOLT: rewrote the beginning of the CAN experiments, to emphasis a bit
more the benefit of parallelism. Need more?

2) Authors criticize how "release points" can be overused or difficult to
   place in the program.
   The same claim holds for multi-active object when the programmer needs
   to decide which method groups should be defined compatible, doesn't
   it?

3) There seems to be related work missing.
   Erlang[3] in its recent versions has addressed more efficient
   scheduling of processes to concurrent execution.
   Scala and Akka[4] have put much effort in "thread work-stealing"
   algorithms to maximize the efficiency of thread utilization.

4) Regarding formal semantics, the order of method execution is mentioned
   in the context of concurrency and distribution. 
   It would add to the value of the work to include/discuss (even
   briefly) the concept of linearizability[1] of methods.

5) In Section 1 (Introduction), it is stated that the work is implemented
   as a Java middleware. This needs to be corrected (rephrased) because
   browsing and studying the code shows that multi-active objects is an
   extension/plugin on top of ProActive[2] framework and not a
   from-scratch implementation.
DONE


--- Reviewer 2 ---

6) There are to many references to the appendix. I have no idea how to
   cut this in case the paper gets accepted.
ZSOLT: Removed the appendix reference from the Evaluation part.

7) What is "N: in the syntax of Serve, Section 3.

--- Reviewer 3 ---

8) Property 2 is rather impossible to read. 

9) One paragraph before Section 5: "Each value amounted to 24KB". I don't
   understand  this.
DONE -- changed the wording

