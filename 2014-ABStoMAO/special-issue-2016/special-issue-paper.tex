\documentclass{lmcs}

\usepackage{enumerate}
\usepackage{hyperref}

\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{stmaryrd}
\usepackage{MnSymbol}
\usepackage{amsthm}
\usepackage{marginnote}
\usepackage{color}
\usepackage{xcolor}
\usepackage{afterpage}
\usepackage{xspace}
\usepackage{mathpartir,url,needspace}


\usepackage{listings}
\usepackage{macros-ASP}

\newcommand{\todo}[1]{\color{red} \textbf{[TODO: #1]} \color{black}}
\newcommand{\ludo}[1]{\color{blue} \textbf{[LUDO: #1]} \color{black}}
\newcommand{\justine}[1]{\color{green} \textbf{[JUSTINE: #1]} \color{black}}
\newcommand{\intro}[1]{\emph{#1}}
\newcommand{\mao}{multi-active object\xspace}
\newcommand{\maos}{multi-active objects\xspace}
\newcommand{\creol}{\textsc{Creol}\xspace}
\newcommand{\jcobox}{\textsc{JCoBox}\xspace}
\newcommand{\absl}{\textsc{ABS}\xspace}
\newcommand{\ambient}{\textsc{AmbientTalk}\xspace}
\newcommand{\encore}{\textsc{Encore}\xspace}
\newcommand{\asp}{\textsc{ASP}\xspace}
\newcommand{\proactive}{\textsc{ProActive}\xspace}
\newcommand{\multiasp}{\textsc{Multi}\asp}
\newcommand{\cobox}{\textmd{CoBox}\xspace}
\newcommand{\coboxes}{\textmd{CoBoxes}\xspace}
\newcommand{\COG}{\textsc{cog}\xspace}
\newcommand{\COGS}{\textsc{cog}s\xspace}
\newcommand{\COGClass}{\texttt{COG}\xspace}
\newcommand{\threadannot}{\code{@DefineThreadConfig}\xspace}
\newcommand{\mathsizebegin}{\begin{small}\begin{mathpar}}
\newcommand{\mathsizeend}{\end{mathpar}\end{small}}
\newcommand{\Cn}{{\nt{cn}}}

\theoremstyle{plain}
\newtheorem{proposition}{Proposition}[section]
\newtheorem{lemma}[proposition]{Lemma}
\newtheorem{theorem}[proposition]{Theorem}
\newtheorem{corollary}[proposition]{Corollary}
\newtheorem{example}[proposition]{Example}
\newtheorem{remark}[proposition]{Remark}

\DeclareMathOperator{\FollowFut}{Follow}
\DeclareMathOperator{\Method}{Method}

\definecolor{verylightgray}{RGB}{247,247,247}

% listing configuration
\lstset{
  frame=none,
  backgroundcolor=\color{verylightgray},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\ttfamily\footnotesize,columns=fullflexible,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=false,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\itshape,    % comment style
  deletekeywords={},            % if you want to delete keywords from the given language
  escapeinside={(*@}{@*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  %frame=bt,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\bfseries,       % keyword style
  language=java,                 % the language of the code
  morekeywords={@DefineGroups, Group},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=2pt,                   % how far the line-numbers are from the code
  numberstyle=\ttfamily\scriptsize, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{black},     % string literal style
  tabsize=2                      % sets default tabsize to 2 spaces
  %title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
  %xleftmargin=0.9em,
  %xrightmargin=0.6em
}

\begin{document}

% Available environments in lmcs document class
%\begin{defi}\label{D:first}\hfill
%  Definition
%\end{defi}
%
%\proof\hfill
%  Proof.\qed
%
%\begin{thm}\label{T:m}\hfill
%	Theorem
%\end{thm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Multi-active Objects and their Applications}

\author[Ludovic Henrio and Justine Rochas]{Ludovic Henrio}	%required
\address{Universit\'e C\^ote d’Azur, CNRS, I3S, France} %required
\email{ludovic.henrio@cnrs.fr} %optional

\author[]{Justine Rochas}
% addresses should be duplicated when authors share an affiliation
\address{Universit\'e C\^ote d’Azur, CNRS, I3S, France}
\email{justine.rochas@unice.fr} %optional

\keywords{Programming languages, distributed systems, active objects.}
\subjclass{~\\{D.3.2}. {Programming languages}. {Language Classifications}. Concurrent, 
	distributed, and parallel languages;
	{D.3.3}. {Programming languages}. {Language Constructs and Features}.
	Concurrent programming structures}
\titlecomment{An extended version of this article is published as a research 
report~\cite{HR-LMCSRR-2017}.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract}
  \noindent In order to tackle the development of concurrent and distributed 
systems, the active object programming model provides a high-level abstraction 
to program concurrent behaviours. There exists already a variety of active 
object frameworks targeted at a large range of application domains: modelling, 
verification, efficient execution. However, among these frameworks, very few  
 consider a multi-threaded execution of active objects. Introducing  
\emph{controlled} parallelism within active objects enables overcoming some of 
their limitations. In this paper, we present a complete framework around the 
multi-active object programming model. We present it through \proactive, the Java 
library that offers multi-active objects, and through \multiasp, the programming 
language that allows the formalisation of our developments. We then show how to  compile 
an active object language with cooperative multi-threading into 
multi-active objects. 
This paper also presents different use cases and the development support to 
illustrate the practical usability of our language. Formalisation of our work 
provides the programmer with guarantees on the behaviour of the multi-active 
object programming model and of the compiler.
\end{abstract}

\maketitle

\section{Introduction}
\label{sec:introduction}

Systems and applications nowadays run in a  concurrent and distributed  manner. In 
general, existing programming models and languages lack   
adequate abstractions for handling the concurrency of parallel programs and for writing  
distributed applications. 
Two well-known categories of concurrency bugs are associated
with synchronisation. On the one hand, deadlocks appear when
there is a circular dependency between tasks. On the other
hand, data races correspond to the fact that several  
threads access a shared resource without proper synchronisation.  In 
object-oriented  programming, allowing several threads of control to execute 
methods breaks  the encapsulation property of objects. Industrial programming 
languages generally do  not enforce by default a safe parallel execution nor 
offer a  programmer-friendly way to program concurrent applications. In Actors and active 
object languages, an application is designed as a set 
of independent entities only communicating by passing messages. This approach makes the 
programming of distributed and concurrent systems easier. 

The active object programming  model~\cite{AOSurvey2017} reconciles object-oriented 
programming and 
concurrency. Each active object has its own  local memory and cannot access the 
local memory of other active objects.  Objects can only exchange 
information through  message passing, implemented with requests sent to active 
objects. This  characteristic makes object accesses easier to trace, and thus 
it is easier to  check their safety. Also, the absence of a shared memory between 
active objects makes  them well-adapted to a distributed execution. 
%Active objects enable formalisation and verification thanks  to the framework 
%they offer: the communication pattern is well-defined, as well  as the accesses 
%to objects and variables. This facilitates static analysis and  validation of 
%active object-based programs.
Section~\ref{sec:background} will give an overview of active-object 
languages. 

Active object models however have limitations in terms of efficiency on 
multicore environments and of deadlocks. Concerning deadlocks, one solution is to allow 
the currently executing thread to be released 
and allow the current active object to handle another request. This approach is called 
cooperative multi-threading and is used in several languages like for example ABS 
(abstract behavioral specification) language~\cite{Johnsen:2010:ACL:2188418.2188430}.

However cooperative multi-threading relies on the programmer's expertise to place release 
points 
which is not always realistic. Additionally, cooperative multi-threading does not solve 
the problem of the 
inefficiency in distributed architectures made of several multicore machines.
This leads us to the 
design of a new model based on active objects: multi-active objects~\cite{Henrio2013}. 
This model enables local multi-threading inside an active object. In  active 
object models,   an activity 
is the unit of composition of the model: an activity is a set of entities evolving 
independently and asynchronously from other objects. In classical active-object models, 
an activity is a set of objects associated with a request queue and a single thread. In 
the multi-active object an 
activity is a single active object equipped with a request queue, it may serve one or 
several requests in parallel. This programming model relies on the notion of 
\emph{compatibility 
between requests}, where two compatible requests can safely be run in parallel. 
\emph{Groups} of methods are introduced to easily express compatibility between requests: 
two requests can run in parallel if they target methods belonging to two compatible 
groups.
\multiasp~\cite{Henrio2013}, is a calculus that formalises the 
multi-active objects.

Concerning the definition of \multiasp, compared to~\cite{Henrio2013}, this article 
presents a new version of the operational 
semantics of the language, and includes an extension that deals with thread 
management;  this article also presents a new debugging tool for multi-active objects.
%The most important contribution of this article is the presentation and the proof of 
%correctness of  the 
%\proactive backend for ABS, a translator from ABS into multi-active objects. 
This 
article is a follow up of the article presented at Coordination 2016~\cite{Henrio2016}. 
Compared to~\cite{Henrio2016}, one contribution of this article is a full overview on 
the 
multi-active programming model including the different programming tools that we provide 
for this programming model. The strongest improvement featured by this article 
compared 
to~\cite{Henrio2016} is the formalisation of the translation and the correctness of the 
translation from ABS programs 
into multi-active objects, and in particular the definition of the relationship between 
\absl\ configurations and their translation in \multiasp. A brief summary of the proof is 
presented in this article and the details of the proof can be found 
in~\cite{HR-LMCSRR-2017}.
%are published as a research 
%report~\cite{HR-RR-ABSMAO-2017}. 
%\footnote{The 
%appendix 
%will be 
%published in a research report.}. 
From a practical point 
of view, the multi-active object model is distributed as part of the \proactive Java 
library\footnote{\url{https://github.com/scale-proactive}} that supports the distributed 
execution of active objects. The proof allows us to provide more 
 precise conditions of applicability of the equivalence 
between ABS programs and their translation.
We summarise below our main contributions.

\begin{itemize}
 \item We introduce the multi-active object programming model, together with the library 
 that supports it, \proactive, and with its formalisation language, \multiasp. 
 This language is also equipped with advanced features regarding the scheduling of 
 requests: thread management and priority of execution. The operational semantics of 
 \multiasp is presented in two parts: the base semantics of the calculus, and an 
 extension dealing with thread management.
 \item  We show the practical usability of the language in two ways. First, we illustrate 
 the programming model with a case study based on a distributed peer-to-peer system. 
 Second, we show a tool that displays the execution of multi-active objects and that 
 helps 
 in debugging and tuning multi-active object-based applications.
 \item We present an approach for encoding cooperative active objects into multi-active 
 objects. We specify a backend translator and prove  the equivalence between the ABS 
 program and its translation in \multiasp\footnote{The backend is available at: 
 \url{https://bitbucket.org/justinerochas/absfrontend-to-proactive}}.
\end{itemize}

Section~\ref{sec:background} details the background of this paper, the active object 
programming model; it provides a
comparison of active object languages and positions our contribution on multi-active 
objects. 
Section~\ref{sec:framework} introduces our multi-active object framework, its semantics, 
and the semantics of the thread management functionality. 
Section~\ref{sec:encoding} presents the automatic translation of \absl programs 
into \proactive programs, shows the properties of the translation, and provides a 
discussion 
relating this work with the other ABS backends and other related works. 
Section~\ref{sec:conclusion} concludes this article.

\input{background.tex}

\input{framework.tex}

\input{encoding.tex}

\section{Conclusion}
\label{sec:conclusion}

This paper presents a  framework for multi-active  
objects, featuring programmer-friendly concurrent programming, advanced request 
scheduling mechanisms, and development support. We  give the guidelines of the 
programming model usage through practical  applications.  The implementation 
of multi-active objects given by \proactive offers a set of annotations that 
allow the programmer to control the  multi-threaded execution of active 
objects. The formalisation of the \proactive library in the 
\multiasp programming language  provides an operational 
semantics to reason on multi-active object executions.
Priorities and thread management, combined with  multi-active object 
compatibilities, reveal to be convenient to encode scheduling patterns. This article 
 presents the \proactive backend for 
\absl. This backend  automatically transforms 
\absl models into distributed applications. We  formalise the translation and 
 prove its correctness. We establish an equivalence 
relation between \absl and \multiasp configurations, and we prove two theorems 
that corroborate the correctness of the translation, illustrating the 
conceptual differences between the two languages.

Overall, we attach a particular interest to three objectives: usability,  
correctness, and performance of our framework. We address the complete spectrum 
of the multi-active object programming model, from design to execution. Testing 
our developments in realistic settings assesses the efficiency of our 
implementation.  On the other hand, our work is formalised and we highlight the 
properties of our model. Thus, we  believe that we reinforce the guarantees offered 
by the multi-active object programming model, on which the programmer can 
 rely. 
 
In the future, we want to investigate the verification of compatibility annotations, but 
also the dynamic tuning of thread management aspects where the thread scheduler could 
adjust at runtime  the number of threads allocated depending on the 
status of the machine and of the active object.


\section*{Acknowledgement}
Experiments presented in this paper were carried out using the Grid'5000 testbed, 
supported by a scientific interest group hosted by Inria and including CNRS, RENATER and 
several Universities as well as other organizations (see https://www.grid5000.fr).
We would like to thank the reviewers of this article for their thorough work and 
constructive comments.

\bibliographystyle{abbrv}
\bibliography{biblio}
% RR
%\newpage
%
%\appendix
%
%\input{appendix.tex}

\end{document}

