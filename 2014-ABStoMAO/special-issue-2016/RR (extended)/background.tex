%!TEX root = special-issue-paper.tex
\section{Background: Active Objects}
\label{sec:background}
After a brief introduction to active objects and actors, this section provides an 
analysis 
of the characteristics that can be used to classify active-object 
languages. Then \asp, 
ABS, and Encore are presented in more details. These languages have been chosen for the 
following reasons: 
ASP is the language that we extend in our multi-active object model, ABS is the 
language for which we provide a backend in
Section~\ref{sec:encoding}, and Encore features a few novel features including some 
controlled parallelism inside active objects.
We refer  to~\cite{AOSurvey2017} for a  
precise description of other active object models including \jcobox, AmbientTalk, and 
Creol. The section concludes by a focus on the related works featuring some form of 
multi-threaded active objects.
 
\subsection{Origins and Context}
\label{sbsc:background-origins}

The active object programming model, introduced 
in~\cite{Lavender:1996:AOO:231958.232967}, has one global objective: facilitate 
the correct programming of concurrent entities. The active object paradigm 
derives from actors~\cite{Agha:1986:AMC:7929}. Actors are concurrent entities 
that communicate by posting messages to each other. Each actor has a mailbox 
and a thread that processes the messages of the mailbox. Only the processing of 
a message can have a side effect on the actor's state. Once a message is posted 
to an actor, the sender continues its execution, without knowing when the 
message will be processed. This way the interaction between actors is 
asynchronous, allowing the different entities to perform tasks in parallel.

Active objects are the object-oriented descendants of actors. Like an actor, each active 
object has an 
associated thread and we call \emph{activity} a thread together 
with the objects that are accessible by this thread. 
Active objects communicate 
using asynchronous method invocations:
when an object invokes a 
method of an active object, this creates a \emph{request} that is 
posted in a mailbox (also called \emph{request queue})
on the callee side. On the invoker side, the execution continues while the request is 
being processed. On the callee side, the request  
is dropped in 
the request queue and waits until its turn comes to be 
executed. 

Like in object-oriented programming, the invocations of methods on 
active objects can return a result. Since these invocations are asynchronous, their 
result 
cannot be known just after the invocation. To represent the 
expected result and to allow the invoker to continue its execution asynchronously, a 
placeholder is created for the result of the request. This place holder is called a 
\emph{future}, which is a promise of response that will be later filled by the result of 
the 
request. A future is \emph{resolved} when the value of the future is computed 
and available. 
Futures have seen their early days in  
MultiLisp~\cite{Halstead:1985:MLC:4472.4478} and  
ABCL/1~\cite{Yonezawa:1986:OCP:28697.28722}. They have been  formalised 
in ABCL/f~\cite{Taura94abcl/f:a}, in~\cite{Flanagan:1999:SFA:968699.968700}, 
and, more recently, in a concurrent lambda 
calculus~\cite{Niehren:2006:CLC:1226601.1226607}, and in the Creol 
language~\cite{deBoer:2007:CGF:1762174.1762205}.  In summary, active objects 
and actors enforce decoupling of activities: each activity has its own memory 
space, manipulated by its own thread. This strict isolation of concurrent 
entities makes them locally safe and also suited to distributed systems.

\subsection{Design of Active Object Languages}
\label{sbsc:background-implementations}

Existing implementations of the active object programming model make different 
design choices for four main characteristics that we have extracted and listed 
below, as answers to design questions. 

\medskip\noindent\textit{How are objects associated to activities?}
\label{sec:activity}
We distinguish three different ways to map objects to threads/activities in 
active object languages: 

\begin{description}
 \item[\begin{small}Uniform object model\end{small}] all objects are active objects
  with their own execution
  thread. All communications between them necessarily create requests. This model is simpler to formalise and reason about, but leads to scalability issues in practice.
  
Creol~\cite{Johnsen:2006:CTO:1226608.1226611, 
	deBoer:2007:CGF:1762174.1762205} and Rebeca~\cite{DBLP:conf/csicc/SirjaniMM01}  are 
	uniform active object languages. \item[\begin{small}Non uniform object 
	model\end{small}] some objects are not
  active objects; they are called \emph{passive} objects and are only accessible by one 
  active
  object.
  This model is scalable
  as it requires less communication and
  less threads, but it is trickier to formalise and reason about because some of the 
  objects are only locally reachable and an additional mechanism is necessary to transmit 
   them between active objects.
  Reducing the number of activities also reduces the number of
   globally accessible references in the system, and thus enables the
  instantiation of a large number of objects.
Non-uniform active objects
reflect better the design of efficient distributed applications where many objects 
are created but only some of them are remotely accessible. 


ASP~\cite{Caromel:2005:TDO:1952047}, AmbientTalk~\cite{Dedecker:2006:APA:2171327.2171349, 
	Cutsem:2007:4396972}, and Joelle~\cite{Clarke:2008:MOA}  are typical 
non-uniform active-object 
languages.  
Orleans~\cite{BernsteinB16} is a  recent industrial active object language with a 
non-uniform active object model.
 \item[\begin{small}Object group model\end{small}]
	an activity is made of a set of objects sharing 
	an execution thread, but all
	objects can be invoked from another activity. This
	approach has a good scalability and propensity to formalisation, but
	the addressing of all objects in distributed settings is difficult to maintain 
	because it requires a distributed referencing system able to handle a large number of 
	objects.
	
	ABS~\cite{Johnsen:2010:ACL:2188418.2188430} and 
	\jcobox~\cite{Schafer:2010:JGA:1883978.1883996} have concurrent object groups but  
	\jcobox additionally allows data sharing for immutable objects. in \jcobox, the 
	object 
	groups are called coboxes, while they are called \COGS (concurrent object groups) in 
	ABS.
\end{description}
\needspace{2ex}

\medskip\noindent\textit{How are requests scheduled?}
We distinguish three request scheduling models:
\begin{description}
 \item[\begin{small}Mono-threaded scheduling\end{small}]
	within an active object, requests are executed sequentially without
	interleaving. This model is simple to reason about and has some strong 
	properties but is the most prone to deadlock and potential inefficiency because one 
	activity is blocked as soon as a synchronisation is required.
	
	ASP~\cite{Caromel:2005:TDO:1952047}, 
	AmbientTalk~\cite{Dedecker:2006:APA:2171327.2171349, 
		Cutsem:2007:4396972}, and Rebeca~\cite{DBLP:conf/csicc/SirjaniMM01} 
	feature Mono-threaded 
	scheduling but Rebeca and AmbientTalk perform no synchronisation  and have no
	deadlock.
	
\begin{figure}
	\centering
	\begin{subfigure}{.53\textwidth}
		\centering
		\includegraphics[height=3.6cm]{background-creol-1.pdf}
		\caption{Configuration 1.}
		\label{fig:background-creol-1}
	\end{subfigure}%	
	\begin{subfigure}{.47\textwidth}
		\centering
		\includegraphics[height=3.6cm]{background-creol-2.pdf}
		\caption{Configuration 2.}
		\label{fig:background-creol-2}
	\end{subfigure}
	\caption{Cooperative scheduling in Creol.}
	\label{fig:background-creol}
\end{figure}
	 \item[\begin{small}Cooperative scheduling\end{small}]
	a running request can explicitly release the execution thread to let
	another request progress. Requests are not processed in parallel but they
	might interleave. While no data races are 
	possible, the status of an object might be difficult to predict when a request is 
	restored after a release point if the cooperatively scheduled requests modify the 
	object state.
	
Creol~\cite{Johnsen:2006:CTO:1226608.1226611, 
		deBoer:2007:CGF:1762174.1762205}, 
		\jcobox~\cite{Schafer:2010:JGA:1883978.1883996}, 
		and ABS~\cite{Johnsen:2010:ACL:2188418.2188430} are typical examples of 
		cooperative scheduling languages.
	Figure~\ref{fig:background-creol} illustrates cooperative scheduling, based on a 
	Creol example. It shows two objects \code{a} and \code{b} each with a queue of 
	awaiting tasks (rectangles). The currently executed task is shown below the object.
	In 
	Figure~\ref{fig:background-creol-1}, object 
	\code{a} does an asynchronous method invocation on object \code{b}, and then 
	awaits for the result.	
	As the future \code{fut} is not yet resolved  when the \code{await} statement 
	executes, this suspends the execution of the current task \code{TaskA} that returns 
	to the pending task queue. At this point, another request for this object can 
	start or resume; in the example a new active task is \code{TaskX} is started. 
	Figure~\ref{fig:background-creol-2} shows a later 
	configuration, after several steps of execution;  
	\code{taskA} is still in the queue of object \code{a}, \code{taskX} is running. In 
	object \code{b}, \code{TaskY} finished and the next task has started. 
	Finally, when \code{TaskB} and \code{taskX} finish, \code{taskA} can resume 
	and retrieve the value computed by \code{TaskB} with a blocking call to 
	\code{.get()}, shown at the end 	of \code{taskA} in 
	Figure~\ref{fig:background-creol-1}.


 \item[\begin{small}Multi-threaded scheduling\end{small}] several threads are running in 
 parallel in the same actor or active object. Either several requests can be served in 
 parallel, or a data-level parallelism allows a 
 request to be processed by several threads. In this context, an \emph{activity} becomes 
 a set 
 of threads together with the objects they manipulate: there is one activity per active 
 object or per object group. Consequently, data races are 
 possible but only within an activity. An additional mechanism is 
	necessary to control which threads can run in parallel. Parallelism provides 
	efficiency 
	at the expense of possible data-races. Somehow, similarly to cooperative 
	multithreading, efficiency and expressiveness are gained at the expense of some 
	possible incoherency in the object state. However, the two approaches offer different 
	trade-offs: in 
	cooperative multithreading incoherences can be limited by removing release points, 
	while in multi-threaded scheduling incoherences can be limited by controlling which 
	threads can run in parallel. 
	%Additionally, cooperative multithreading is still 
%	limited to one thread active at a time, while multithreading can be much more 
%	efficient but may lead to low-level data-races if programmed incorrectly
On the other hand, multithreading enables parallelism internally to an active object	
which 
is more efficient in some cases like in a multicore setting.

	\multiasp and parallel actor monitors~\cite{Scholliers:2014:PAM} feature 
	multi-threaded scheduling where several 
	requests can be served in 
	parallel by the same active object. Multi-threaded actors~\cite{AzadbakhtBS-ICE16} 
	feature multi-threading, but with each thread hosted by a different active-object (an 
	actor contains several active objects in the terminology of~\cite{AzadbakhtBS-ICE16}).
	Section~\ref{sec:RW-MAO} discusses in more details multithreading in active objects 
	and actors.
\end{description}

%We call the active object languages that use a cooperative scheduling of 
%requests \emph{cooperative active objects}. We call the active object languages 
%that use a multi-threaded scheduling of requests \emph{multi-active objects}.

\medskip\noindent\textit{How much is the programmer aware of  
asynchronous aspects?}
Some active object languages use a specific syntax for asynchronous method 
calls and a specific type for futures.  This makes the programmer aware of 
where  synchronisation occurs in the program. When the asynchronous 
invocation is explicit, there often exists a special type for future objects, 
and an operator to access the future's value. On the contrary, asynchronous 
method calls and futures can also be implicit: this enables transparency of 
distributed aspects, and facilitates the transmission of future references 
(sometimes called first-class futures~\cite{Henrio:2010:FCF:2031978.2032019}). In this 
case, 
there is almost no syntactic difference between a distributed program with asynchronous 
invocations and usual objects, consequently writing simple distributed programs is 
easier. On the 
other hand, explicit manipulation of 
futures allows the programmer to better control the program execution, but  
also requires a better programming expertise. 

	Creol~\cite{Johnsen:2006:CTO:1226608.1226611, 
	deBoer:2007:CGF:1762174.1762205}, \jcobox~\cite{Schafer:2010:JGA:1883978.1883996}, 
 ABS~\cite{Johnsen:2010:ACL:2188418.2188430}, and Encore~\cite{Brandauer2015} have 
 explicit future types and explicit asynchronous invocations contrarily to 
 ASP~\cite{Caromel:2005:TDO:1952047} where there is no specific type for futures or 
 active 
 objects.
 
 \medskip\noindent\textit{How are handled the results of asynchronous method 
 invocations?}
 What distinguishes active objects from actors is 
 the fact that communication between activities is performed by invocation of methods,  
 according to the object's interface, and not through send and receive operations.  
This distinction in the terminology is also highlighted in  Akka~\cite{AkkaBook} where 
``actors'' are distinguished from 
``typed 
 actors'' that are in fact active objects. The different languages propose 
 different ways to 
 handle method results:
 \begin{description}
 	\item[No return value] In some languages the methods of an active object cannot 
 	return any value. This is for example the case of Rebeca or Scala 
 	actors~\cite{Haller:ScalaActors-2009}.
 	\item[Futures] The most classical approach is to use a future to represent the 
 	expected result of an asynchronous method call. When the returned value is needed, 
 	the program is interrupted until the value becomes available. The futures can be 
 	explicitly visible to the programmer like in ABS or transparent like in ASP. In the 
 	second case there is no need for a specific instruction to wait for the future, the 
 	current thread is automatically blocked when the value associated to a future is 
 	\emph{needed}. This mechanism is called \emph{wait-by-necessity}. In languages 
 	with cooperative scheduling, it is possible to let another request execute while the 
 	future is awaited (e.g. using the \code{await} statement in ABS).
 	\item[Asynchronous futures] Some languages use a future to represent the 
 	expected result of an asynchronous method call but provide no synchronisation on 
 	futures. Instead a continuation is triggered upon the resolution of the future. This 
 	continuation can be more or less explicit depending on the language. Akka and 
 	AmbientTalk feature asynchronous futures.  For example, in AmbientTalk,
 	 a 
 	future access creates and asynchronous 
 	invocation that will be triggered after the future has been resolved. The 
 	event-based execution of the different activities makes 
 	sequences of actions more difficult to enforce. However, an AmbientTalk program is 
 	always partitioned into separate event handlers that maintain their own 
 	execution context, making the inversion of control easier to handle.
 \end{description}
 
% 
%\subsection{A Detailed Description of Three Active Object Languages}
%\label{sbsc:background-languages}
%We review below the major active object languages, positioning them regarding the 
%aspects presented above, and highlighting their strengths and weaknesses.



%\medskip\noindent\textit{Creol}~\cite{Johnsen:2006:CTO:1226608.1226611, 
%deBoer:2007:CGF:1762174.1762205} is a uniform active object language in which 
%all objects are active objects: each object has a dedicated execution thread. 
%Consequently, objects can only communicate through asynchronous method calls 
%and futures. In \creol, asynchronous method calls and futures are explicit, but 
%futures are not first class. \creol is based on a cooperative scheduling of 
%requests through the \code{await} language construct, that can be used in order 
%to yield the execution thread if the future on which it applies is not 
%resolved.
%% Figure~\ref{fig:background-creol} shows an example of execution of a 
%%\creol program highlighting two runtime configurations.  In 
%%Figure~\ref{fig:background-creol-1}, object 
%%\code{a} does an asynchronous method invocation on object \code{b}, and then 
%%awaits for the result. When the \code{await} actually releases the execution 
%%thread because the future is not resolved, another request for this object can 
%%start (if it is in the request queue) or can resume (if it was deactivated by a 
%%previous call to \code{await}). In Figure~\ref{fig:background-creol-2} showing a later 
%%configuration, while 
%%\code{taskA} is put back in the queue of object \code{a}, \code{taskX} gets 
%%activated. In object \code{b}, \code{TaskY} finished and the next task has started. 
%%Finally, a \code{.get()} call, that is blocking, can 
%%be used to 
%%eventually retrieve a future's value, like in 
%%Figure~\ref{fig:background-creol-1} at the end of \code{taskA}.
% \creol avoids 
%data races thanks to its cooperative scheduling of requests. However, the 
%drawback of \creol is that its safety of execution strongly depends on how well 
%the programmer places the release points in the program. Placement of release points 
%should make the best use of threads, avoiding blocking, and should prevent deadlocks. 
%However, having release points in the program entails  interleaving of requests, and the 
%programmer must take into account the different modifications of the object state 
%entailed by the interleaved requests.

%\medskip\noindent\textit{JCoBox}~\cite{Schafer:2010:JGA:1883978.1883996} is 
%an active object programming model, together with its implementation in Java. 
%\jcobox mixes the non uniform and the object group model. The objects are 
%organised into groups, called \coboxes, such that each \cobox is responsible 
%for maintaining its own coherent concurrency model. At each time, each 
%\cobox has one single thread that is actively processing a request. 
%\jcobox features immutable objects that can safely be shared between active 
%objects. Like in \creol, asynchronous method calls and futures are explicit, 
%and requests are executed through a cooperative scheduling. \jcobox interleaves 
%the request execution of all the objects lying in a same \cobox, where 
%\creol only interleaves the requests of a single object. Thus, the execution of 
%active objects in \jcobox is less deterministic than in \creol. Nevertheless, 
%\jcobox better addresses practical aspects than \creol in terms of scalability. 
\subsection{Abstract Behavioural Specification.}\label{sec:ABS}
%\begin{figure}[t]
%	\centering
%	\begin{small}
%		$\begin{array}{r@{}c@{}l@{\qquad}l@{}}
%		P&~::=~& \vect{I}~\vect{C}~\{\vect{T~x~;}~s\} & \text {program} \\
%		T&~::=~& \text{\it primitive-type}~|~{\tt Fut<}T{\tt >}~|~{\tt I}  & \text{type} 
%		\\
%		I&~::=~& {\tt interface~I}~\{(\vect{S~;}\} & \text{interface} \\
%		S&~::=~& T~\m(\vect{T~x}) & \text{method signature} \\
%		C&~::=~& \class~\C(\vect{T~x})~[{\tt implements}~ \vect {\tt 
%		I}]~\{\vect{T~x~;}~\vect M\} &\text{class} \\
%		M&~::=~& S~\{\vect{T~x~;}~s\} & \text{method definition} \\
%		g&~::=~& b ~|~ x? ~|~ g \land g' & \text{guard} \\
%		s&~::=~& \eskip ~|~ x=z ~|~ \eif ess ~|~  \ewhile bs & \text{statement} \\
%		& & ~|~ \ereturn e ~|~ s;s ~|~ \await~g ~|~ {\tt suspend} & \\
%		z&~::=~& e ~|~ e.\name m(\vect e) ~|~ e!\name m(\vect e) ~|~ 
%		\enew{[local]~\C(\vect e)} ~|~ x.\tt get & \text{expression with side effects} \\
%		e&~::=~& v ~|~ x ~|~\ethis ~|~ \text{\it arithmetic-bool-exp} &\text{expression}\\
%		v&~::=~& \enull ~|~ \text{\it primitive-val} & \text{value}
%		\end{array}$
%	\end{small}
%	\caption{Syntax of the concurrent object layer of \absl.}
%	\label{fig:translation-abs-syntax}
%\end{figure}
\begin{figure}[t]
	%{\footnotesize
	\begin{center}
		\begin{small}
			$         \begin{array}{r@{}c@{}l@{}r@{}}
			g&::=& b \bnfor x? \bnfor g \land g'       &\text{guard}\\
			s&::=& \eskip\bnfor x\!=\!z\bnfor\texttt{suspend}\bnfor
			\texttt{await}\ g\bnfor\ereturn e \bnfor\eif ess  \bnfor s\!\semi\! s  
			&\text{statement} \\
			z&::=&  e \bnfor e.\name m(\vect e)\bnfor e!\name
			m(\vect e) \bnfor \enew{[\texttt{local}] \C(\vect e)}\bnfor 
			x.\text{get}&\text{\hspace{-13.5ex}expression 
				with 
				side effect}\\
			%      &|&  &
			%      \text{}\\
			e&::=& v\bnfor x\bnfor\ethis\bnfor\text{\it arithmetic-bool-exp}   
			&\text{expression}\\
			v&::=& \enull\bnfor \text{\it primitive-val} &\text{value}
			\end{array}$
		\end{small}%}
	\end{center}
	\caption{Syntax of the concurrent object layer of ABS (definition of method, class, 
	and types omitted). }\label{fig:classABS}
\end{figure}
\begin{figure}
	\begin{minipage}{0.49\textwidth}
		\begin{lstlisting}[label=lst:background-abs, caption=ABS program code.]
		BankAccount ba = new BankAccount();
		Transaction t = new local Transaction(ba); 
		WarningAgent wa = new WarningAgent();
		Fut<Balance> bfut = ba!apply(t);
		await bfut?;
		Balance b = bfut.get;
		wa!checkForAlerts(b);
		b.commit() \end{lstlisting}
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[scale=0.25]{background-abs.pdf}
		\caption{ABS program execution.}
		\label{fig:background-abs}
	\end{minipage}
\end{figure}
\textit{ABS}~\cite{Johnsen:2010:ACL:2188418.2188430} is an 
object-oriented modelling language based on active objects. \absl takes its 
inspiration in \creol for the cooperative scheduling and in \jcobox for the 
object group model. It  uses explicit asynchronous method calls and 
futures. \absl is intended for modelling and verification of distributed 
applications. The object group model of \absl is represented by Concurrent 
Object Groups (\COGS). A \COG manages a request queue and a set of tasks that 
have been created as a result of asynchronous method calls to any of the 
objects inside the \COG. Inside a \COG, only one task
 is active at any time. New objects can be 
instantiated in a new \COG with the \code{new} keyword. In order to instantiate 
an object in the current \COG, \code{new local} must be used instead. In the example, the 
transaction \code{t} is instantiated in the  \COG\ that is running the current (main) 
method.  Contrarily to 
\jcobox, \absl makes no difference on the object kind: all objects can be 
referenced from any \COG and all objects can be invoked either synchronously or 
asynchronously. Listing~\ref{lst:background-abs} shows an \absl program that 
creates two new \COGS and performs asynchronous method calls.  Note the specific 
syntax (\code{!}) for asynchronous method  invocation and the \code{await} instruction on 
Line~5 that releases the current task if the future stored in \code{bfut} is not yet 
available.
Figure~\ref{fig:background-abs} pictures the sending of the \code{apply} 
request to the remote \COG. In the illustration, \COGS are large rectangles with round 
angles, request queues are depicted at the top of \COGS, and objects are symbolised by 
circles.
The concurrent object part of the syntax of ABS is shown in Figure~\ref{fig:classABS}. 
In this syntax, x range over variable names, the overline notation ($\vect{e}$) is used 
for lists, \textit{arithmetic-bool-exp} range over arithmetic and boolean expressions, 
and \textit{primitive-val} stands for primitive (integer and boolean) values. The most 
significant statements and constructs have been described in the example. 
 Note that field access is 
restricted to the current object ($\ethis$).

\absl comes with numerous engines (see
\url{http://abs-models.org/}) for verification of concurrent and distributed 
applications: a deadlock analyser~\cite{Giachino2015}, a resource consumption 
analyser~\cite{Johnsen201567}, a termination and cost analyser 
COSTABS~\cite{Albert:2012:CCT:2103746.2103774,Albert2014}, and a 
 verifier for generic program properties
KeY-ABS~\cite{Din:August:2015,Din:November:2015}. In addition to verification 
tools, \absl tools also comprise a frontend compiler and several backends that 
translate \absl programs into  Maude, Java, or 
Haskell~\cite{Bezirgiannis2016}. This work is partially based on the Java backend.

\subsection{Asynchronous Sequential Processes.} ASP~\cite{Caromel:2004:ADO:964001.964012} 
is an active object language 
with a non uniform 
active object model; it is intended to be close to  realistic implementations of 
distributed systems. 
  ASP features \emph{mono-threaded scheduling} and futures are 
implicitly created from asynchronous remote method calls. \asp features a 
wait-by-necessity behaviour upon access to an unresolved 
future. \asp has proven determinism properties and formalises object 
 groups and software
components~\cite{Caromel:2005:TDO:1952047}. Communications ensure causal ordering of 
requests.
\proactive \cite{Baduel2006} is the Java 
library that enforces the 
\asp semantics. As the active object model of \asp is transparent, 
\proactive active objects follow as much as possible the syntax of standard Java. The 
only 
syntactic difference is the
 \code{newActive} primitive that is used to create an active object. 
Listing~\ref{lst:background-proactive} 
shows the same example as in the \absl above, written in \proactive. 
Nevertheless, the semantics of the two programs is different since in \absl 
the future is always resolved when the \code{checkForAlerts} request is sent.

\begin{lstlisting}[float, label=lst:background-proactive, caption=An example of ProActive program. \emph{node} is not defined here., emph={node}, emphstyle=\itshape]
BankAccount ba = PAActiveObject.newActive(BankAccount.class, null, node) ;
Transaction t = new Transaction(ba); 
WarningAgent wa = PAActiveObject.newActive(WarningAgent.class, null, node);
Balance b = ba.apply(t); // t is deeply copied
wa.checkForAlerts(b); // if b is a future it is passed transparently
b.commit(); // a wait-by-necessity is possible here
\end{lstlisting}

\proactive is intended for distribution, it forms a complete middleware 
that supports the deployment of applications on distributed infrastructures such 
as clusters, grids, and clouds. To this end, when an active object is created, it 
is registered in the Java RMI registry, RMI being the main communication layer 
used in \proactive. Consequently, passive objects are deeply copied when 
communicated between activities, the different copies are then handled independently and 
can be in an inconsistent state. The advantage of this approach is its  scalability 
and its coherency with the mechanism of RMI, and consequently its practical effectiveness.
Because \proactive is  a Java API, it must be integrated with the standard 
Java programming language. Proxies are created to  represent generalised  pointers 
to futures and active objects; consequently futures and active object cannot be of 
primitive type and  a method that returns a primitive type would be called 
synchronously.
%\medskip\noindent\textit{AmbientTalk}~\cite{Dedecker:2006:APA:2171327.2171349, 
%Cutsem:2007:4396972} is a distributed programming language based on 
%asynchronous method calls and futures. Like \asp/\proactive, \ambient follows a 
%non uniform model with active and passive objects. Asynchronous invocations are 
%explicit and return dynamically typed futures. \ambient follows a mono-threaded 
%scheduling model, but a program's execution never leads to a deadlock. Indeed, 
%accessing an unresolved future results in an asynchronous call that returns 
%another future. The event-based execution of the different activities makes 
%sequences of actions more difficult to enforce. However, an \ambient program is 
%always partitioned into separate event handlers that maintain their own 
%execution context, thus it provides a convenient inversion of control.

\subsection{Encore}
Encore~\cite{Brandauer2015}  has an active object 
programming model mixed with other parallel patterns. \encore is essentially 
based on a non-uniform object model but uses capacities to handle the concurrent access 
to passive objects, thus enabling shared references to passive objects. \encore uses 
cooperative scheduling of requests 
that is similar to \creol and \absl.  Futures are explicitly typed and their value must 
be 
 retrieved via a \code{get} construct.  \encore natively features parallel constructs 
 other 
than asynchronous method invocations. Internal parallelism can be spawned explicitly 
 through \code{async} blocks, or implicitly through parallel 
combinators~\cite{party-coordination}.  \encore unifies all its parallel constructs with 
the 
use of futures for 
handling asynchrony. A chaining operator \code{$\rightsquigarrow$} can add a 
callback to a future, in order to execute it when the future is resolved. Thus Encore 
features both synchronisation on futures and asynchronous futures. Cooperative 
scheduling 
and future 
chaining mix explicit and automatic synchronisation, and save the 
programmer from the burden of precisely placing all the release points in the 
program.

\subsection{Multi-threaded Actors and Active Objects}~\label{sec:RW-MAO}
In this paper, we focus on 
multi-threaded active objects~\cite{Henrio2013}, that are characterised by a controlled 
parallelism within an activity. Contrarily to Encore, parallelism does not occur 
inside a request, but between requests, and under the control of the programmer since only 
requests tagged as compatible can run in parallel. Comparing multi-threaded scheduling to cooperative scheduling is more complex as the concurrency model is much different: on one hand 
cooperative scheduling prevents race conditions that might occur within multi-threaded 
active objects (e.g. if the wrong requests are declared compatible), but on the other hand 
multi-threaded active objects provide a parallel execution that is more efficient and allow 
the programmer to control which requests  run in parallel. Section~\ref{sec:encoding} 
 shows how cooperative scheduling can be faithfully encoded with multi-threaded active 
objects and  provides a  deeper technical insight on the comparison between 
the two approaches.

In~\cite{Hayduk2013} the authors enable automatic parallel execution of requests inside 
actors. They use transactional memory to undo local side-effects that may be conflicting. 
However this work supports actors interacting only by asynchronous messages; it does not 
take into account any other form of synchronisation, like futures in 
active objects.

Other  works introduced multi-threading inside actors or active objects. 
Parallel actors monitors~\cite{Scholliers:2014:PAM} (PAM) was designed at the same time 
as \multiasp; it provides an interface to schedule the parallel service of requests in an 
actor. The framework is richer and allows the programmer to express any scheduling of 
request and fully control the request treatment. On the contrary, multi-active objects 
only allow the programmer to state which requests  can be served in parallel, possibly 
depending on dynamic criteria. Multi-active objects feature a higher level of abstraction 
where the programmer is unable to reorder request or cancel some of them, overall 
multi-active objects preserve more guarantees of the original actor and active-object 
programming model. In fact, compatibility annotations could be used to generate a 
specific PAM scheduler that would simulate the behaviour of multi-active 
objects.
More recently, \emph{multi-threaded actors} have been proposed~\cite{AzadbakhtBS-ICE16}. 
This approach is quite different from multi-active objects 
or PAM because each active object is  mono-threaded but several active objects
share the same request queue. The parallel treatment of requests in the same request 
queue is  similar to the approach presented in this paper, but the fact that those 
requests are served by different actors makes a significant difference. On one side the 
approach 
of \cite{AzadbakhtBS-ICE16} ensures the absence of race-conditions but on the other side, 
it gives no guarantee on which object will serve which request. 
This approach is well adapted to stateless objects, but not adapted to stateful objects, 
as different requests can be handled by different objects. Indeed 
consider two requests targeting a multi-threaded actor, if the first one modifies the 
state of one of the actor's object and the second request is handled by a different 
object, then the state modification will not be visible by the second request.
