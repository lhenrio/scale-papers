\subsection{Object Instanciation}\label{ao-models}
ABS and ProActive are both object-oriented languages that provide active objects. However, they are based on different active object models. We distinguish three different active object models, listed hereafter:
\begin{description}
\item \textit{Uniform Object Model.} 
In the uniform active object model, all objects are active objects. All objects have their own request queue and their own private execution thread. In consequence, all communications between objects occur by posting a request to an object. This is the object model of Creol~\cite{ref:creol}. 
\item \textit{Non Uniform Object Model.}
Alternatively, active objects can get along with passive objects\footnote{Passive objects are regular objects. They have no request queue, no dedicated execution thread and support only synchronous method calls.}. Two kind of objects then coexist, like in ProActive. A passive object cannot be directly accessed remotely; it has to be manipulated through the unique active object to which it is related, otherwise it is a copy of this object that is manipulated.
\item \textit{Object Group Model.}
The object group model gathers several objects so that they share the same request queue and execution thread, like in ABS and JCoBox~\cite{wrongjcobox}. In the ABS case, only one kind of object exists and all objects are directly accessible from any group.
\end{description}
The active object model drives the object creation process. To handle the difference between the object models of ABS and ProActive, in our ProActive backend we need to define what happens in ProActive when a new ABS object is created. In particular, we need to define the ProActive code that would be equivalent to the ABS new cog statement.

As in ABS objects should be accessible from all others, one could think that implementing all objects with a ProActive active object would meet the requirement. However, in practice, this is hardly manageable: a ProActive active object is associated a plain Java thread (not a logical thread). Thus, this solution would inevitably lead to a substantial context switch overhead, as the number of objects is large in the application, harming its overall performance. 
%Obviously, we do not want to change the object model of ABS nor of ProActive, and as it is not possible, using ProActive, to create all objects as active objects, we have to select which objects will be ProActive active objects, and which ones will be passive objects.
In our ProActive backend, only the COGs are active objects: they have a private request queue and execution thread.
%In the initial implementation of the Java backend, a COG was implemented as a regular Java object. We select then the COG objects as ProActive active objects. 
The reasons for this choice are the following. First, it is natural to consider the COG as the unit of distribution, and ProActive active objects are the unit of distribution. Second, when a ProActive active object is created, it is automatically put in a registry that is accessible through the network\footnote{This is in fact the RMI registry, as ProActive is based on RMI}. A COG object is thus accessible network-wide via an URL. Thanks to this registry, a COG object can be the entry point to all the objects it contains. All objects other than COG objects can then remain passive, thus preserving the performance of the ProActive backend.
Thus, we have a two-level hierarchical indexing of objects:
\begin{itemize}
\item A first level of network-wide accessible COG objects. This mechanism is integrated in ProActive as it is based on RMI~\cite{Wollrath:1996:DOM:1268049.1268066}.
\item A second level of locally-accessible objects. This mechanism is implemented in the COG class using a map from object identifiers to object references.
\end{itemize}

This classification implies that the application always manipulates local references except for COG objects that can be manipulated through remote references.
To actually apply this strategy, we need to implement in ProActive the translation of the \code{new cog} statement.
Consider the following ABS line of code:
\begin{lstlisting}
Server server = new cog Server();
\end{lstlisting}
We translate this single line of code into three ProActive lines of code:
\begin{lstlisting}
Server server = new Server();
COG cog = PAActiveObject.newActive(Cog.class, new Object[]{
		absRuntime, instanciatingClass, gcm}, gcm.getANode());
cog.registerObject(server);
\end{lstlisting}
The first line creates a regular server object. The second line uses the \code{newActive} primitive of ProActive to create a new COG active object. Several parameters are passed to the \code{newActive} primitive to (i) ensure that the new COG object is well-constructed and to (ii) specify onto which node it should be deployed (we will go back to this point later on). The third line is paramount: as the new COG was created remotely on another node, we need to send over the newly created server object onto the same node. Indeed, an object and its COG must always be located on the same memory space, since a COG should access its objects through local references. To send over the server object to the memory space of its COG, we use the \code{registerObject} method of the COG, giving \code{server} as parameter. As the reference to the COG is a remote reference, the \code{registerObject} call is a remote call. Consequently, the parameter of this call, the server object, will be copied away according to the ProActive object model, and all the objects created along with the server object will be copied too, if any. Then of course, to comply with the initial ABS program, all further method calls on \code{server} should be run on the remote server object, and not on the server object that was created locally. Nevertheless, we still need the local server object to be able to get back to the targeted remote server object, as we will see in the next section.






