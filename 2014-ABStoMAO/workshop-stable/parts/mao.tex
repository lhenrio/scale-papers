\subsection{MultiactiveObjects}\label{mao}
Recently, an extension of the active object model has been proposed, the multiactive object model~\cite{ref:mao}. Multiactive objects leverage deadlock issues that are likely to arise when doing reentrant calls with active objects. The principle is to execute multiple requests in an active object in parallel, while controlling the concurrency thanks to the specifications of the programmer. Thus, multiactive objects are also better adapted to multicore architectures than active objects.

The multiactive object model has been implemented as an extension of ProActive. This extension comes as a small metalanguage to allow a programmer to declare which requests can safely be executed in parallel, namely which requests are \emph{compatible}. This metalanguage is based on the Java annotation mechanism. A programmer can use these annotations on top of a class so that all objects of this class are automatically multiactive objects when instanciated with the \code{newActive} ProActive primitive\footnote{if there is no annotation, then objects crerated with the \code{newActive} primitive remain basic active objects}. 

Request compatibility can be specified following three steps:
\begin{itemize}
\item A programmer first uses the \code{@Group} annotation on top of a class to define a group of requests. A group is meant to gather requests that have the same concerns and/or the same compatibility requirements. 
\item Then, a programmer uses the \code{@MemberOf} annotation on top of a method definition to make this method belong to a particular group (previously defined).
\item Finally a programmer uses the \code{@Compatible} annotation to specify which group is compatible with which other group, so, in extension, to specify which requests can be run in parallel safely. 
\end{itemize}
Below is an example of an annotated class \code{MyClass}. The \code{selfCompatible} parameter of the \code{@Group} annotation defines whether two different requests \emph{of the same group} can safely run in parallel.
%\ttfamily\scriptsize
\begin{figure}[!h]
	\setlength\abovecaptionskip{0.25mm}
	\lstset{ numberstyle=\tiny, stepnumber=1, numbersep=2pt, basicstyle=\ttfamily\scriptsize, keywordstyle=\bfseries,
    showstringspaces=false,
    deletekeywords={true, false, public, class},
    morekeywords={@Compatible,@Group,@MemberOf}}
	\begin{lstlisting}
@DefineGroups({
  @Group(name="group1", selfCompatible=true),
  @Group(name="group2", selfCompatible=false)
})
@DefineRules({
  @Compatible({"group1", "group2"})
})
public class MyClass {
  @MemberOf("group1")
  public ... method1(...) { ... }
  @MemberOf("group2")
  public ... method2(...) { ... }
  ...
}
 	\end{lstlisting}
\caption{Example of annotated class}
\label{fig:regular_peer_class}
\end{figure}
The internal scheduling of a multiactive object automatically takes into account the compatibility specifications of the programmer. The default scheduling policy that is applied is the following: a request is executed if it is compatible with the request that are already executing \emph{and} compatible with the previous requests in the queue. The first condition ensures safety and the second condition ensures a maximum parallelism while avoiding starvation.
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{pictures/mao-scheduling.pdf}
\caption{An example of internal checking for request scheduling}
\label{fig:graph}
\end{figure}
Recently, additional features have been added in multiactive objects to better control the scheduling of requests~\cite{henrio:hal-00916293}. The new features include the possibility to set a limit on the number of threads that can be created inside a multiactive object. This can be done using an additional annotation on top a class, \code{@DefineThreadConfig}, like in the following:
\begin{lstlisting}
@DefineThreadConfig(threadPoolSize=8, hardLimit=false)
\end{lstlisting}
The \code{threadPoolSize} parameter defines the maximum number of threads in a multiactive object.
The \code{hardLimit} parameter defines the kind of limit that is applied: if it is true, it is the total number of threads that is limited by the \code{threadPoolSize} parameter. If it is false, only the number of active threads is limited by the \code{threadPoolSize} parameter. Active threads are threads that are currently executing a request and not blocked in a \emph{wait-by-necessity} state. In consequence, the number of waiting threads is not limited when the \code{hardLimit} parameter is set to false. For example, suppose, considering the above \code{@DefineThreadConfig} annotation, that the eight threads execute a request each. If one of those threads stops because it waits for a future to be resolved, then, because the \code{hardLimit} is set to false, a ninth thread will be created to execute another request. In this case, there still are eight active threads but one waiting thread in addition. Afterwards, when the request that waits for a future is ready to resumed it waits until a slot of active thread is available.

Another specific extended feature of multiactive object is the limitation of threads \emph{per group}. The \code{@Group} annotation supports two additional parameters for this purpose, as follows:
\begin{lstlisting}
@Group(name="routing", selfCompatible=true, minThreads=2, maxThreads=10)
\end{lstlisting}
The \code{minThreads} parameter reserves threads from the initial thread pool. In this example, it guarantees that the \code{routing} group will always have two threads devoted to its requests, so that at least two requests of the routing group can be run at any moment. Those reserved threads cannot be used by other groups. On the other hand, the \code{maxThreads} parameter defines the maximum number of threads that can be used at the same time by requests of this group. In the example, the \code{routing} group can have at maximum ten of its requests executed at the same time. The \code{minThreads} and \code{maxThreads} parameters can be seen as the lower and upper bound on the number of threads that a group holds at a given time.

Multiactive objects enable a high level implementation of precise scheduling policies. We use the introduced features to implement our ProActive backend.
In the following, we use the term ProActive to denote the latest version of the software with multiactive object features, if not mentioned otherwise.