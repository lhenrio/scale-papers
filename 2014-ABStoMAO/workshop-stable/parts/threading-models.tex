\subsection{Threading Models in Active Objects}
Another fundamental difference between ABS and ProActive is in their threading model.
We distinguish three different threading models in active object languages.
\begin{description}
\item \textit{Single-threaded.} 
Within an active object, requests are executed sequentially without interleaving possibilities. This is the threading model of ProActive active objects, if no annotation for multiactivity is found.
\item \textit{Cooperative.}
A running request can explicitly release the execution thread to let another request run. Requests are not processed in parallel but they might interleave. Creol, JCoBox, and ABS comply to this threading model.
\item \textit{Multi-threaded.}
Within an active object, requests are executed in parallel using different threads, without pausing nor yielding. This is the threading model of ProActive with multiactive objects.
\end{description}
Compared to ABS, ProActive does not offer the possibility to pause a request for the benefit of another request. To get a similar behavior in ProActive, we use the possibilities of multiactive objects.

Multiactive objects provide several mechanisms to control the internal scheduling, such as compatibilities and thread limits, as seen in Section~\ref{mao}.
By mean of tuning those mechanisms very carefully, we can get a precise scheduling policy. 
%More specifically, the following presents the multiactive object features we use to get the same request scheduling as ABS:
%\begin{itemize}
%\item Groups and compatibilities
%\item Global thread limitation
%\item Thread limitation per group
%
%\item Requests grouping. Requests can belong to the same group of requests thanks to the @Group annotation.
%\item Thread limitation per group. For a given group, we can specify the maximum number of threads that requests of this group can use at the same time.
%\item Thread reservation per group. For a given group, we can specify the number of threads of the thread pool that are devoted to execute requests of this group, i.e. no requests of other groups can use them even if they are free.
%\item Soft and Hard limit switch. We can specify whether the maximum number of threads of the thread pool designates the maximum number of active threads (soft limit) or if it designates the maximum number of total threads (hard limit) 
%\end{itemize}
In ABS, the cooperative threading model is materialized by the \code{await} statement. Therefore, in the ProActive backend we modify the translation of the \code{await} statement to make it interfere with the internal scheduling of multiactive objects.
To apply our strategy, we need to modify the way the \code{await} and \code{get} ABS statements are translated in the Java backend. 

\subsubsection{\code{await} statement on futures}
\paragraph{}
The \code{await} statement followed by a future should yield if the future is not resolved at time of the \code{await} evaluation. In ProActive, a special primitive is provided to interact with a future. We use this primitive as a basis of the \code{await} translation.
Consider this ABS program:
\begin{lstlisting}
Fut<Bool> ready = server!start(); (1)
await ready?;                     (2)
\end{lstlisting}
We translate the second line of code \code{(2)} in the following ProActive line of code:
\begin{lstlisting}
PAFuture.getFutureValue(ready);   (2)
\end{lstlisting}
The \code{getFutureValue} primitive of ProActive is meant to explicitly wait for a future value to be resolved, provided that the given variable is actually a future. Note that, conceptually, any method invocation on the future would trigger the \emph{wait-by-necessity} mechanism, as we need here, but for the code to be cleaner we prefer relying on the ProActive API. In our case, the issue with this call is that it is blocking. With no further configuration, this would be like doing an ABS \code{get} statement: no other request would be executed in this COG until the future is resolved. 
As we have seen in the section~\ref{ao-models}, the COG object receives all the asynchronous method calls of the objects it contains, and run them by reflection through the \code{execute} method. In consequence, all the asynchronous method calls of the entire program are executed through one method, so if we control this method, we control the way all the asynchronous methods are executed. 
Thus, in order to achieve the desired request scheduling, we configure the COG class and its \code{execute} method with multiactive object annotations in three steps:
\begin{enumerate}
\item We create a particular group of requests with the \code{@Group} annotation, named \code{scheduling}. We declare this group \code{selfCompatible}, so that several requests of this group are allowed to execute in parallel.
\item We assign the \code{execute} method to the \code{scheduling} group (\code{@MemberOf} annotation).
\item We declare with the \code{@DefineThreadConfig} that an instance of COG has a thread limit of 1 (\code{threadPoolSize = 1}), and that this limit is not a hard limit (\code{hardLimit = false}). This means that only \emph{active} threads are counted in the thread limit of 1; the threads that are waiting for the future are then not counted in this limit.
\end{enumerate}
Hereafter is the big picture of the COG class with the previous customization.
\begin{figure}[!h]
	\setlength\abovecaptionskip{0.25mm}
	\lstset{ numberstyle=\tiny, stepnumber=1, numbersep=2pt, basicstyle=\ttfamily\scriptsize, keywordstyle=\bfseries,
    showstringspaces=false,
    deletekeywords={true, false, public, class},
    morekeywords={@DefineThreadConfig,@Group,@MemberOf}}
\begin{lstlisting}
@DefineGroups({
  @Group(name="scheduling", selfCompatible=true)
})
@DefineThreadConfig(threadPoolSize=1, hardLimit=false)
public class COG {
  ...
  @MemberOf("scheduling")
  public ABSValue execute(UUID objectID, String methodName, ABSType[] args) {
  ...
  }
  ...
}
\end{lstlisting}
\end{figure}
Considering the above definition, what happens now when the program encounters the previous line of code \code{(2)} (\code{PAFuture.getFutureValue(ready)}) is that the current thread is put in the waiting state, if the future \code{ready} is not resolved at this point. This thread is now a waiting thread, and because the global thread limit only counts active threads, another thread can be created or resumed since it is not counted in the global thread limit any more.
The global thread limit being equal to 1, as we configured, there can be only one single active thread at a time, which is exactly the ABS behavior. 

Altogether, thanks to a few annotations in the COG class, we are able to make the ProActive \code{getFutureValue} primitive work like the \code{await} statement of ABS, even if they had a different behavior initially. 
	
\subsubsection{\code{get} statement}
\paragraph{}
The translation of the get statement would have been straightforward if we had not configured the COG class for the translation of the \code{await} statement. Indeed, by doing so, we have disabled the blocking aspect of the ProActive \code{getFutureValue} primitive. 
%In consequence, we cannot use this primitive by itself anymore to translate the ABS \code{get} statement. 
The \code{get} statement should block the execution thread without handing it off to another request.
So what we need to translate the \code{get} statement is to temporarily go back to the initial behavior of the ProActive \code{getFutureValue} primitive (without annotations).
In the \code{await} case, what causes another request to execute is the setting of the \code{hardLimit} parameter of the COG class to false, which releases the limit so that it counts only active thread. Therefore, to translate the \code{get} statement, we set the \code{hardLimit} parameter to true just for the time the future is waited.
This ensures that no new thread is started, and that no waiting thread is resumed, since the global limit of 1 is already reached, at least counting the current thread. When the current future is resolved, setting back the \code{hardLimit} parameter to false ensures that execution resumes normally.
Consider this new ABS program:
\begin{lstlisting}
Fut<Bool> readyFut = server!start(); (1)
Bool ready = readyFut.get;           (2)
\end{lstlisting}
We translate the second line of code \code{(2)} in the following ones:
\begin{lstlisting}
getCOG().switchHardLimit(true);
PAFuture.getFutureValue(ready);  
getCOG().switchHardLimit(false);
\end{lstlisting}
We first set the \code{hardLimit} of the current COG to true thanks to the \code{swichHardLimit} method call. Note that this call is synchronous since the retrieved COG is the current one. This way, we are sure that the limit is switched when executing the next line of code. 
Then, the future \code{ready} is waited. The next \code{switchHardLimit} call can only be executed when the future is resolved, thanks to the \emph{wait-by-necessity}, and its purpose is to reset the \code{hardLimit} to false, to restore the previous behavior.
%so that waiting threads are not counted in the global limit of 1 anymore, and can then yield execution to another thread. 

So thanks to the limit type switch, this translation offers the same behavior as the ABS \code{get} statement. 

\subsubsection{\code{await} statement on conditions}
\paragraph{}
Another usage of the ABS \code{await} statement is to use it followed by a condition. For this usage of the \code{await} statement, we cannot rely on the ProActive \code{getFutureValue} primitive at first glance, because a condition is obviously not a future variable. However, we can still wrap the condition evaluation in a method call, possibly returning a future value and waiting for it in a \code{getFutureValue} call.
 
During compilation, when such an \code{await} statement is encountered, we generate a method that lively checks if the condition is true. Then, we translate the statement by an asynchronous call to a special method in the COG class that we have added, with signature \code{awaitCondition(objectID, method, parameters[])}.
We call the \code{awaitCondition} method on the current COG, giving it as parameter the current object and the generated method corresponding to the condition, and the condition members as parameters if they are not globally defined.
Again, we wrap this call in the \code{getFutureValue} primitive, in order to wait for the result of this asynchronous call, i.e. when the condition evaluates to true.
For example, when the following ABS code is encountered:
\begin{lstlisting}
await a == 3;
\end{lstlisting}
First, a corresponding method is generated\footnote{The code is lighten for clarity reasons.}:
\begin{lstlisting}
public void cond7517d1ff7c52(int x) {
  while(x != 3) {
    Thread.sleep(500); 
  } 
}
\end{lstlisting}
Then, the ABS \code{await} statement is translated into:
\begin{lstlisting}
PAFuture.getFutureValue(
    this.getCog().awaitCondition(this.getId(), "cond7517d1ff7c52", a));
\end{lstlisting}
The \code{awaitCondition} call executes by reflection the method \code{cond7517d1ff7c52} on the object retrieved with the given identifier, passing the parameter \code{a}.
The reason why we need to rely on an additional method to perform the lively waiting is because we need to release the execution thread slot to let other request progress meanwhile.
But without any further configuration, this strategy potentially leads to a deadlock when the generated method gets executed in the COG. Indeed in this case, the \code{awaitCondition} method execution could take up the single execution thread and never release it because, as no other request could progress, the condition would never evaluate to true. In order to prevent the condition method from monopolizing the execution thread, we dedicate some new threads to the execution of condition methods only. 

We create a new group of requests, named \code{waiting}, with the \code{@Group} annotation, and we put the \code{awaitCondition} method in this group, thanks to the \code{@MemberOf} annotation. We also declare the \code{waiting} group \code{selfCompatible}, because several \code{awaitCondition} requests could evaluate conditions at the same time. To be able to execute the \code{awaitCondition} requests in parallel, we arbitrary increase the global limit size by 50 threads, to have at maximum 50 concurrent condition evaluations in one COG, plus 1 thread for processing the standard requests (from the \code{scheduling} group). 
As waiting conditions and standard requests should execute in parallel, we declare the \code{scheduling} and \code{waiting} group compatible. Nevertheless, we still must prevent the 50 new threads from executing requests of the \code{scheduling} group, because otherwise it would break the ABS cooperative scheduling. For that, we specify a minimum and a maximum of 50 threads for the \code{waiting} group in its declaration. The result of this specification is that, not only the waiting group will be able to use up to 50 threads, but also those 50 threads cannot be used by other groups. So in fact we have a clear separation between the single thread used by the \code{scheduling} group, and the threads used by the \code{waiting} group.
The COG class is thus augmented with new annotations, as follows:
\begin{figure}[!h]
	\setlength\abovecaptionskip{0.25mm}
	\lstset{ numberstyle=\tiny, stepnumber=1, numbersep=2pt, basicstyle=\ttfamily\scriptsize, keywordstyle=\bfseries,
    showstringspaces=false,
    deletekeywords={true, false, public, class},
    morekeywords={@Compatible,@Group,@MemberOf,minThreads,maxThreads,threadPoolSize}}
\begin{lstlisting}
@DefineGroups({
  @Group(name="scheduling", selfCompatible=true),
  @Group(name="waiting", selfCompatible=true, minThreads=50, maxThreads=50)
})
@DefineRules({
    @Compatible({"scheduling", "waiting"}),
})
@DefineThreadConfig(threadPoolSize=51, hardLimit=false)
public class COG {
  ...
  @MemberOf("scheduling")
  public ABSValue execute(UUID objectID, String methodName, ABSType[] args) {
  ...
  }
  @MemberOf("waiting")
  public ABSType awaitCondition(
      UUID objectID, String methodName, ABSType[] args, Class<?>[] args) {
    ...
  }
  ...
}
\end{lstlisting}
\end{figure}
This solution is fully compliant with the ABS semantic and proves the power of multiactive object annotations.
Note that, although effective, this solution could be enhanced with a callback when the condition is true, instead of having a live waiting. 


