\subsection{Asynchronous Method Calls}\label{asyncmc}
So far, object creation in the ProActive backend is compliant with the ABS active object model. But ABS and ProActive still have different ways to process asynchronous method calls. In particular, ABS features explicit asynchronous method calls with explicit futures, whereas ProActive manages them transparently.
In the initial Java backend, a synchronous method call  was distinguished from an asynchronous method call by calling a different method in each case. In our ProActive backend, we simply removed this distinction, as method calls are implicitly asynchronous on active objects.

However, in our ProActive backend, only COG objects are active objects, which means that they are the only objects on which asynchronous method call can be run. Thus it is not technically possible to directly run an asynchronous method call on a object that belongs to a different COG, as it is done in ABS. We have designed an adapted strategy to pass by this limitation.
Consider this ABS piece of code:
\begin{lstlisting}
server!start();
\end{lstlisting}
We suppose that \code{server} is a the server object created in a new COG of the previous example. 
The idea to translate this asynchronous method call in ProActive, is to run an asynchronous method call on the COG of \code{server} and then let this method retrieve the server object and run the desired method on it.
Here is the translation the ProActive backend provides:
\begin{lstlisting}
server.getCog().execute("start", new ABSType(){}, server.getId());
\end{lstlisting}
More precisely, we first retrieve a reference to the COG of the server object by using the local reference of the server object. Note that this server object is not the one on which we actually want to run the \code{start} method. However, this server object has the right remote reference to its COG (since in ProActive, active object references are unique) and it is this remote reference we get when calling the \code{getCog()} method\footnote{Actually, this returns the local reference of a proxy to the remote COG, which is equivalent to a remote reference.}.
Then, the \code{execute} method is called on the COG object returned by \code{getCog()}, and this method call is implicitly asynchronous by the nature of the COG object. The \code{execute} method has been added in the COG class in order to execute all the asynchronous method calls of the program. The call to this method drops an \code{execute} request in the request queue of the targeted COG. When the \code{execute} request is run, it uses the third parameter of the request, \code{server.getId()}, to lookup the local reference of the server object that is in the memory space of the targeted COG and run the \code{start} method synchronously on it by reflection. The first parameter of the \code{execute} method is the name of the method to run; the second parameter of the \code{execute} method is an array of the parameters of the method to run, all under the \code{ABSType} type. 

Several things can be noted about the \code{execute} methods. First, as it is a ProActive asynchronous remote call, all of its parameters are copied away to be able to correctly run the method in the remote place. For the first and third parameters, the fact that they are copied does not matter because they are immutable variables, so having many independent copies of them does not change the behavior of the program. However, for the second parameter of the \code{execute} method (that holds the parameters of the call to make), the correctness of having a copy of it might not be obvious. In the previous example, the \code{start} method had no parameters, this is why the \code{ABSType} array was empty. But consider another asynchronous method call like this one:
\begin{lstlisting}
server!start(p1, p2);
\end{lstlisting}
This asynchronous method call has two parameters, \code{p1} and \code{p2}. 
This is translated through the ProActive backend this way:
\begin{lstlisting}
server.getCog().execute("start", new Object(){p1, p2}, server.getId());
\end{lstlisting}
When running the \code{execute} method, objects \code{p1} and \code{p2} are copied to the memory space of the targeted COG. Thus, two versions of \code{p1} and \code{p2} exist in the system at runtime, and the copies of \code{p1} and \code{p2} might not reflect the latest version of \code{p1} and \code{p2} when the \code{start} method is executed. In ABS, parameters \code{p1} and \code{p2} are manipulated by reference in the \code{start} method, and not by copy as it is the case in ProActive. However, in practice, the \code{start} method can only manipulate \code{p1} and \code{p2} through asynchronous method calls if they are not from the local COG\footnote{In ABS, synchronous method calls are enabled only for objects that are in the same COG}. In consequence, any method call on the copies of \code{p1} or \code{p2} is actually delivered first, as explained before, to the COG of the initial \code{p1} and \code{p2}. So in fact, any manipulation of those copies always gets back to their initial version by global referencing, thus reflecting all modifications that occurred meanwhile. Thanks to this mechanism, we are able to reproduce the behavior by reference of ABS using the behavior by copy of ProActive.

In the end, when we copy an object from one node to another, we only need two things: its identifier, to be able to retrieve the local object in the right memory space, and a reference to its COG. All the others attributes of the objects can actually be omitted since they will not be used on the copy side. This observation allows us to optimize object copy to reduce at minimum the amount of information that is sent to a remote node. We have thus few data transfers but in return, lots of communication.
