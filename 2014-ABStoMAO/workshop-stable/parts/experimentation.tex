\section{Experimental Evaluation}\label{experimentation}
In order to validate the benefits of our approach, we conducted an experimental evaluation of the ProActive backend and we compared it with the performance of the initial Java backend. The considered use case is the pattern matching of a DNA sequence against a DNA database using the MapReduce programming model~\cite{Dean:2008:MSD:1327452.1327492}.
We implement the MapReduce programming model in ABS with a MapReduce class that is responsible for splitting the input data and dispatching \code{map} and \code{reduce} calls to several workers (class Worker). Each worker is instanciated in a new COG so that workers can work in parallel. Workers do not communicate with each other, they communicate with the MapReduce object only.

We consider a searched pattern of 250 bytes, and a database of 5 MB of DNA sequences.
The MapReduce object divides the database into 100 equal parts (50 kB each), and creates 100 pairs \code{(pattern, part)}. The pairs are distributed evenly to the workers and processed in parallel through the map method. Each map tries to match the pattern on the given sample, and returns the maximum matching sequence found in this sample.
Once all mappers are finished, all the results that are local to a map are aggregated and passed to a single reducer that outputs the global maximum matching sequence of the pattern against the whole database.
The reduce phase is negligible compared to the map phase. Indeed, the map phase is based on an exponential algorithm, showed in Algorithm~\ref{alg1}, whereas the reduce phase just has to compute the maximum of 100 entries.
\begin{algorithm}
\caption{Maximum matching sequence algorithm - map phase}
\label{alg1}  
\begin{algorithmic}
\While {$pattern$ not ended} 
	\While {$sample$ not ended}
		\While {$pattern$ matching $sample$}
			\State update maximum matching
		\EndWhile
	\EndWhile
\EndWhile
\end{algorithmic}
\end{algorithm}
We computed the execution time of the whole use case when varying the number of workers, i.e. when varying the degree of parallelism. When using the initial Java backend, we run the use case on one single machine, since it does not support distribution. When using the ProActive backend, we deploy two workers (two COGs) per machine, to fully exploit the machine hardware. For all experiments, we used the machines of a single cluster of the Grid5000 platform~\cite{ref:g5k}. All the machines have 2 CPUs of 2.6 GHz with 2 cores each, and 8 GB of RAM.
Figure~\ref{fig:graph} shows the results when using from 2 to 50 workers, therefore using from 1 to 25 physical machines in the case of the ProActive backend ("Distributed" line on the figure).
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{pictures/2workers-per-node-final.png}
\caption{Execution time for local and distributed execution per number of nodes/workers. Each measurement dot is an average of five executions.}
\label{fig:graph}
\end{figure}
The execution time of the use case with the ProActive backend decreases sharply for the ten first added machines and then continuously decreases slowly after using more than a tenth of machines. On the other hand, the initial Java backend has an optimal degree of parallelism of 4 workers, which actually is the number of computing units of the machine. Then, when the number of workers increases, the execution time increases slowly as well, whereas the degree of parallelism is augmented. This raise is due to an increasing context switching overhead, as all the threads are executing on the same machine.
On the opposite, increasing the degree of parallelism when using the ProActive backend is always gainful, even though the gain tends to decrease as the number of workers (and of machines) increases.
In summary, the speedup achieved from the initial Java backend to the ProActive backend grows linearly for the tested values. In the best case, the code produced by the ProActive backend runs 12 times faster than on 25 machines than the code produced by the initial Java backend on a single machine. More reasonably, we can point out that simply using 10 machines makes the use case complete 5 times faster, precisely in 19 minutes instead of 1 hour and 35 minutes.

Nevertheless, we can also notice that, at the lowest number of workers depicted, the regular Java backend performs better than the ProActive backend: it is 7\% faster. This is due to the overhead induced by the distribution: distributing involves more computing units but costs more in terms of serialization and communication. Thus, it is useless to use the ProActive backend for a small number of workers, because the parallelism achieved in shared memory is more efficient in this case.
This shows that the ProActive backend should be used only when the use case includes computationally intensive work, that can be split into subsets, to distribute the computational load across several machines. However, the ProActive backend is small enough to be beneficial quickly, as the size of the use case grows.