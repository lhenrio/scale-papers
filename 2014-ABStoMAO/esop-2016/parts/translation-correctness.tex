\newcommand{\Cn}{{\nt Cn}}
\section{Translation Correctness}\label{sec:proof}
To prove that our approach is correct, we rely on the translational semantics from ABS to \MultiASP given in Section~\ref{sec:translationalsem}.
Proving that \MultiASP executions exactly simulate ABS semantics is not possible by
direct bisimulation of the two semantics. Instead, we prove two different theorems stating
under which condition each semantics simulates the other.  First, we
identify activity names of \MultiASP with \COG names (ranged over by $\alpha$, $\beta$). Object
locations are valid only locally in \MultiASP and globally in ABS, their equivalent global
reference is the pair (activity name, \nt{Id} fields). To simplify the proof, we suppose
that each object name in ABS is of the form $i\_\alpha$ where $\alpha$ is the name of the \COG where
the object is created, and $i$ is unique locally in $\alpha$. The semantics then allows us to
choose \nt{Id} and $i$ so that they are equal. By convention, $\Cn$ ranges over ABS
configurations and $\underline{\Cn}$ over \MultiASP ones.


 % Indeed, our contribution is a backend for ABS allowing to
% execute an ABS program in a distributed setting, thus having
% properties at compilation time is more important than proving
% complete equivalence between the two languages (which does not exactly
% holds in this case).

\paragraph{Restrictions} Before simulating the semantics of ABS and {\MultiASP}, we define three
specific restrictions. First, \MultiASP ensures causal ordering of
communications with a rendez-vous preceding all asynchronous method calls:  the request
\emph{is dropped} in the remote request queue \emph{synchronously}. This brief
synchronisation does not exist in ABS where requests
can arrive in any order. We will reason on $\absred$, the ABS semantics with
rendez-vous communications, i.e. the semantics taken from~\cite{ref:abs} where
the message sending and reception rule is replaced by\footnote{Another
  rule has to be added in case of a message to
  the same object.}:
\vspace{-1.5ex}
\begin{small}
\begin{mathpar}
  \inferrule[Rendez-vous-Comm]
{\fresh(f)\\i'\_\beta=\llbracket e\rrbracket^A_{a+ l}\\\many{v}=\llbracket\many{e}\rrbracket^A_{a+l}\\p''=\bind(i'\_\beta,f,m,\many{v},\class(o'))}
{ob(i\_\alpha,a,\{l|x=e!m(\many{e});s\},q)~ob(i'\_\beta,a',p',q')\\\absred ob(i\_\alpha,a,\{l|x=f;s\},q)~ob(i'\_\beta,a',p',q'\cup p'')~\textit{fut}(f,\bot)}
\vspace{-2ex}
\end{mathpar}
\end{small}
where $\llbracket\rrbracket_\sigma^A$ is the expression evaluation in ABS.
Secondly, in \MultiASP, while thread activation can happen in any order, the order in which
requests are served is FIFO by default instead of the non-deterministic activation of a
thread featured by ABS semantics.
In both the existing Java backend for ABS and the developed ProActive backend, activation and request service are FIFO, although multiactive objects
support the definition of different policies~\cite{henrio:hal-00916293}.  Consequently, we only reason
on executions with a \emph{FIFO policy}, i.e. executions that serve requests and restore
them in a FIFO order.  Finally,
the proof does not deal with local synchronous method invocations; indeed this would make
the proofs more entangled and would bring no particular insight on the translation
because this part is similar to the existing Java backend for ABS.

\stepcounter{footnote}
\footnotetext{\label{fn1}We use the following
  notation: $\exists y.\, P~ \iff~ \exists x.\, Q~ \WITH~ R$  means\\ $(\exists y.\, P~
  \iff~ \exists x.\, Q) \land \forall x,y.\, P\land Q\Rightarrow R$. This allows $R$ to refer to 
  $x$ and $y$.}
\stepcounter{footnote}
\footnotetext{$\Method(f)$
  returns the method of the request corresponding to future
  $f$}
\begin{figure*}[!bt]
      \begin{small}
$\texttt{1}$        $\exists a.\,cog(\alpha,a)\in \Cn$ \iff~ $\exists o_\alpha,\sigma,p,\Rq .\,
        act(\alpha,o_\alpha,\sigma,p,\Rq)\in\underline{\Cn}$~~ \WITH\\
$\texttt{2}$
        \text{~}  $\exists\many{v},p',q.ob(i\_\alpha, \manyMap{x\!\mapsto\! v}, p', q)\!\in\! \Cn$ \iff  \,  $\exists o,\many{v'} .\,\sigma(o)\!=\![\nt{cog}\!\mapsto\!\alpha,\nt{myId}\mapsto i, \manyMap{x\!\mapsto\! v'}]$ \WITH\\
$\texttt{3}$
        \text{~}\quad $%\begin{array}[t]{l}
          \many{v}\approx_\sigma^{\Cn}\many{v'} ~~\land\\
\texttt{4}    \qquad      \left(
            \begin{array}{l}
              \exists l,s.\, p'=\{l|s\} \iff~ \exists f,i,m,\many{v''},\ell',s',\ell'',s''.
              \left( (f,execute,i,m,\many{v''})_A\mapsto\{\ell'|s'\}::\{\ell''|s''\}\in p \right. \\ \left. \land~\ell'(\ethis) = o \vphantom{(f,execute,i,m,\many{v''})}\right)
              ~\WITH~ \forall x\in dom(l)\backslash destiny.\,l(x)\approx_\sigma^{\Cn}\ell'(x)\land
              l(destiny)=f) \land s\approx_{\sigma+\ell'}^{\Cn} s'
            \end{array}\right)
          ~~\land\\
\texttt{5}   \qquad       \forall f.\left(
            \begin{array}{l}     \big(\exists l,s.(\{l|s\}\in q\land l(destiny)=f) \iff \\ 
            \exists  i,m,\many{v''},\ell',s',\ell'',s''.
              \big(\left((f,execute,i,m,\many{v''})_P\mapsto
              \{\ell'|s'\}::\{\ell''|s''\}\in p\land \ell'(\ethis) = o\right)  \\ \lor~
              \left((f,\nt{execute},i,m,\many{v''})\in\Rq  \land o_\alpha.\nt{retrieve}(i)\!=\!o\land\bind(o,m,\many{v''})=\{\ell'|s'\}\right)\big)\\
              ~~\WITH  \quad (\forall x\in dom(l)\backslash\nt{destiny}.\,l(x)\approx_\sigma^{\Cn}\ell'(x) ) \land s\approx_{\sigma+\ell'}^{\Cn}s'\big) \end{array}\right)$ %(\approx?)
        %\end{array}$

  \noindent%$\bullet$ 
$\texttt{6}$  $\textit{fut}(f,v)\in \Cn$ \iff~ $\exists v',\sigma.\, (\fut(f,v',\sigma)\in \underline{\Cn}\land\Method(f)=\nt{execute})$ \WITH~
  $v\approx_\sigma^{\Cn} v'$

  \noindent%$\bullet$ 
$7$  $\textit{fut}(f,\undefined)\in \Cn$ \iff ~$\fut(f,\undefined)\in \underline{\Cn}\land\Method(f)=\nt{execute}$
\end{small}
\vspace{.5ex}\caption{Equivalence condition of two configurations${}^{\addtocounter{footnote}{-1}\thefootnote,\,\addtocounter{footnote}{1}\thefootnote}$}\label{equivalence}
\end{figure*}

\paragraph{Equivalence} We define an equivalence relation $\Rc$ between \MultiASP and ABS
terms.  We then prove that any single step of one calculus can be simulated by a sequence
of steps in the other. This is similar to the proof in \cite{DBLP:conf/pdp/DamP14}; we use the same observation notion: processes are observed based on remote method
invocations. 
\begin{definition}[Equivalence Relation $\Rc$]\label{def:eq}
$\approx_\sigma^{\Cn}$ is an equivalence between values (or between a value and a storable) in the context of
a \MultiASP store $\sigma$ and an ABS configuration $\Cn$:
\vspace{-1ex}
\begin{small}
  \begin{mathpar}
    \inferrule{}{v\approx_\sigma^{\Cn} v}

    \inferrule{}{f\approx_\sigma^{\Cn} f}

  \inferrule{}{i\_\alpha\approx_\sigma^{\Cn} [\nt{cog}\mapsto\alpha,\nt{Id}\mapsto i,\many{x\mapsto
        v'}]}

\\
    \inferrule{v\approx_\sigma \sigma(o)}{v\approx_\sigma^{\Cn} o}

    \inferrule{\textit{fut}(f,v')\in \Cn\\v'\approx_\sigma^{\Cn}v} {f\approx_\sigma^{\Cn}  v}

  \end{mathpar}
\end{small}
\emph{Runtime values in ABS are either (global) object references, future
references or primitive values. Equivalence $\approx_\sigma^{\Cn}$ specifies that two identical values or  futures are equivalent if they are the same. Otherwise, an object
is characterised by its identifier and its \COG name. The two last cases
are more interesting and are necessary because of the difference
between the future update mechanisms of ABS and \MultiASP. First, the equivalence can follow as
many local indirections in the store as necessary. Second, the
equivalence can also follow future references in ABS: a future might
have been updated transparently in \MultiASP while in ABS the explicit
future read has not been performed yet.}

\smallskip
Furthermore, we have an equivalence on statements:
$s\approx_{(\sigma+\ell')}^{\Cn}s'$ iff \\
$\begin{array}[t]{l} \quad - ~ \text{either}~ \llbracket s\rrbracket=s' 
\\ \quad - ~ \text{or}~ s=(x=v;s_1)~ \land  ~
 s'=(x=e;\llbracket s_1\rrbracket)~ with~ v\approx_\sigma^{\Cn}\llbracket e\rrbracket_{(\sigma+\ell')} \end{array} $\\ 
\emph{In words,  two statements are equivalent if one is
the translation of the other. Or, two statements are also
equivalent if both statements start with an assignment of equivalent
values to the same variable, followed by equivalent statements.}

\smallskip
Finally, ABS configuration $\Cn$ and \MultiASP configuration $\underline{\Cn}$ are equivalent,
written $\Cn~ \Rc~ \underline{\Cn}$, iff the condition on Figure \ref{equivalence} holds.\\
\emph{
% Equivalence condition explanation begin
\noindent The equivalence condition of Figure~\ref{equivalence} considers three cases:\\
\noindent -- The first five lines deal with equivalence of \COG. This case compares both
activity content and activity requests on the ABS and \MultiASP sides:
%This case is itself decomposed into complementary parts: comparison of activities and comparison of executed requests, detailed below.
\begin{itemize}
\item[\begin{scriptsize}$\bullet$\end{scriptsize}] To compare activity content, %from ABS to activities from \MultiASP, 
we 
  rely on the fact that activities have the same name in ABS and in \MultiASP. % in ABS and in \MultiASP.
%   We then compare their content, i.e.  associated object ($o$),
%   content of the store ($\sigma$), set of executing requests ($p$), and set
%   of requests in the queue ($\Rq$). 
Each ABS object $ob$
  must correspond to one equivalent \MultiASP object in the equivalent 
  activity $\alpha$. The  object equivalent to
  $ob$ must be in the store, must reference $\alpha$ as its
  \COG, must have $i$ as identifier\footnote{Recall that the
    corresponding ABS identifier is $i\_\alpha$.}, and must have other fields 
  equivalent to the ones of $ob$.
\item[\begin{scriptsize}$\bullet$\end{scriptsize}] To compare activity requests, %  i.e. the set of tasks of the
%   two configurations, 
  we need to compare 
  the tasks that exist in ABS $ob$ terms
  to the tasks that exist in the corresponding \MultiASP $act$ terms. We consider again two cases: 
  \begin{enumerate}
  \item  Either the task is active, 
the single active task of $ob$ (in $p'$) must have exactly one equivalent active task in
\MultiASP (in $p$).
% if an object $ob$ has an active task in $p'$
%   (there can be only one), then the \COG $\alpha$ in \MultiASP must have an
%   equivalent active task in $p$. 
In \MultiASP, this task must have two
  elements in the current stack\footnote{Recall that we only deal with asynchronous method calls in the proof.}: the call to the \COG
  (the $execute$ call) and the redirected call to the targeted object $o$,
  where $o$ is equivalent to $ob$. In addition,  values of local variables must be
  equivalent, except  $destiny$  that must correspond to the
  future of the \MultiASP request. Finally, the current thread
   of the two tasks must be equivalent according to the equivalence on
  statement.
  \item   
  Otherwise the task is inactive, and two cases are possible.
%  with the corresponding future f in
%   ABS, then we have two cases to consider:
 Either the task has already started and has been interrupted:
    in this case the comparison is similar to the active task
    comparison above.
 Or the task has not started yet: there must be a
    corresponding task in the request queue $\Rq$ of the \MultiASP
    active object $\alpha$, and we check that (i) the future is equivalent,
    (ii) the invoked object $o$  is equivalent to $ob$, and
    (iii) the method body retrieved by the bind predicate is
    equivalent to the ABS task. %  (using equivalence on statement defined
%     above).
  \end{enumerate}
%   In the two cases, as previously stated, we also need to check that
%   local variables (apart from $destiny$) have equivalent values.
\end{itemize}
\noindent -- Line \texttt{6} deals with equivalence of resolved
futures. A future's value in
\MultiASP refers to its local store. Two resolved futures are equivalent if their values are equivalent.
% A resolved future in ABS is equivalent to a
% resolved future in \MultiASP if the value of one future is equivalent
% to the value of the other future. 
 In \MultiASP,
only futures from \nt{execute} method calls are considered.\\
\noindent -- Line \texttt{7} deals with equivalence of unresolved
futures. In that case, both futures must be unresolved to ensure equivalence.
}
\end{definition}
% We say that an unresolved future in ABS is equivalent to an
% unresolved future in \MultiASP if the two same futures lead to
% $\undefined$.

% Equivalence condition explanation end
%idle \iff there is no active request
% \[
% \begin{array}[t]{l}
%  \iff\\
% .\, %\\%% define cogs
% \land\\
% Ob(o\_\alpha, \manyMap{x\mapsto v}, p, q)\in \Cn.\,
% iff \exists! i,\many{v} .\,\sigma(o)=[\nt{cog}\mapsto\alpha,myId\mapsto i, \manyMap{x\mapsto v'}]\\
%   \WITH \\
% \end{array}\] 
%Note that there is  a single active execute thread!!!
% \noindent Finally, it is possible to also deduce the ABS config from the
% \MultiASP one (except for method bodies).


\noindent
\textbf{From ABS to \MultiASP.}
We first aim at proving that \MultiASP semantics simulates any ABS execution.
Let \begin{small}$P=\vect{I} \vect{C}\ \wrap{\vect{x\semi}\ s}$\end{small} be an ABS
program, let \begin{small}$\Cn_0$\end{small} be the initial ABS configuration corresponding to
this program: \begin{small}$ob(start, \emptyset,p,\emptyset)$\end{small}, where the process \begin{small}$p$\end{small} corresponds to
the activation of the program's main block: \begin{small}$p=\{l|s\}$\end{small}. The initial
\MultiASP configuration corresponding to program \begin{small}$\llbracket P\rrbracket$\end{small} is
\begin{small}
$\act(\alpha_0,o,\sigma_0[o\mapsto\emptyset],q_0\mapsto\{l|\llbracket s\rrbracket\},\emptyset)$
\end{small}.  It is easy to see that this initial
configuration has the same behavior as
\begin{small}
$\act(\alpha_0,o,\sigma_0[o\mapsto\emptyset,\nt{start}\mapsto\emptyset],$ $(f,\nt{execute},start,m)\mapsto\{l|\llbracket s\rrbracket\}::\{\emptyset|x=\bullet\},\emptyset)$.
\end{small}
We denote this new \MultiASP initial configuration \begin{small}$\underline{\Cn_0}$\end{small}, and \begin{small}$\to^*$\end{small}
denotes the transitive closure of \begin{small}$\to$\end{small}.

\vspace{-1ex}
\begin{theorem}\label{thm:abs-to-multiasp} The translation simulates all  ABS executions
  with rendez-vous communications and FIFO policy:\\
  \begin{small}
$\Cn_0\!\absredN \!\Cn$ with a FIFO policy $ \land\nexists f,f'.
\,\textit{fut}(f,f')\!\in\!\Cn~$\\\text{}\qquad$\Rightarrow~\exists\underline{\Cn}.\,\underline{\Cn_0}\!\redN\!\underline{\Cn}~\land~\Cn\,\Rc\,\underline{\Cn}$
\end{small}
\end{theorem}
\vspace{-1ex}

Appendix~\ref{sec:proof-theor-1} details the proof of Theorem~\ref{thm:abs-to-multiasp} with all the
cases except the restrictions given above.
% except local method calls and await on conditionals (see appendix for the
% justification).
 Because of the simulation technique, what matters is to decide which actions are
observable. 
As stated before, preceding works use the sending of requests as an observable. In our case, we show in the proof
that request sending is simulated exactly, as well as method return, object creation, and field
assignment. Assignment to local variables is trickier because of the existence of
temporary variables created by the translation. Consequently, assignments to ABS local variables can be
observed exactly, but it is not the case for assignments to \MultiASP local variables.
The most  striking example of an observable reduction in ABS that is not observable in
\MultiASP is the case of future's value update.
Indeed, transparency of futures and of future updates create an intrinsic difference
between the two languages. This is why, in the theorem, we exclude the possibility to have a future's value being a future in the configuration.
 In \MultiASP, there
is no process able to detect whether  the first future has been
updated or not: in ABS the  configurations (a) \begin{small}$\textit{fut}(f,f')~
\textit{fut}(f',\undefined)$\end{small} and the configuration (b) \begin{small}$\textit{fut}(f,\undefined)$\end{small} are
observationally
different, whereas in \MultiASP they are not. However,
this example is purely artificial as no information is stored in the first
future of configuration (a). Indeed, any access the value stored in a future
will have to follow indirections and eventually access the
future $f'$.
 Eliminating syntactically such programs is not possible, thus
we only reason on reductions for which the value of a future is not
a future; this is not a major restriction on expressiveness because it
is always possible to have a future value that is an object containing
 a future.
%  Other ABS reductions are not fully observable in \MultiASP translation. Local assignment are not observable because of the existence of temporary variables in the \MultiASP translation. Asynchronous method invocations are only observable for the ones that involve \code{execute} requests in \MultiASP translation.
 The identification of observability differences in active object languages is a major
 result: it gives a significant insight on the  design of programming languages. Besides, we could make this observation because we chose a faithful translation that matches one to one as much as possible 
 requests, objects and futures of ABS to the equivalent notions in \MultiASP.  

 \smallskip\noindent \textbf{From \MultiASP to ABS.} 
%We now aim at proving that the translation corresponds to a possible ABS execution.  
 In this direction, the main
 difficulty is that we introduced additional steps in the reduction. On the other
 hand, the languages have only few concurrent rules (method invocation and future
 awaiting) and, as there can be a single active thread at a time, it prevents any local concurrency. Thus,
 the sequence of additional actions never performs a wait-by-necessity, it runs until the
 end without any observable interruption. Only thread activation, thread passivation,
 method invocation, and future update can create interleavings. 

 \vspace{-1.1ex}
\begin{theorem}\label{thm:multiasp-to-abs} Any reduction of the \MultiASP translation corresponds to a  valid ABS execution:\qquad
\begin{small}
$\underline{\Cn_0}\redN \underline{\Cn} \Rightarrow\exists \Cn.\,\Cn_0\absredN \Cn \land \Cn\,\Rc\,\underline{\Cn}$
\end{small}
\end{theorem}
\vspace{-1.1ex}

Proving this direction is a little trickier as the translated code is more operational; we can be in
intermediate states in \MultiASP that must be attached to the right state in ABS. Here,
one should first observe that the translation of an ABS primitive mostly involves a single
action that impacts the state of the equivalent ABS program. For example, for remote
invocations, solely the call to \emph{execute} impacts the request queue of the
destination and has an effect on the equivalence relation. The global idea is to see only
those actions and to enable statement translation to do the same. In the case of \emph{execute},
assignments to intermediate variables are ignored in the equivalence, and all the states
that precede the execution of the remote method invocation are considered equivalent to
the same ABS state. This is mostly handled by ignoring adequately assignments to
variables that are not part of the ABS code. Also one should notice that there is an
intermediate state when the request is served but the associated ABS method is not started
yet, one should also consider this case in the notion of equivalence: such a just started
request is similar to the case when the request still is in the queue. It is also
 important to notice that the theorem needs no restriction on futures which value is a
 future because when a future access is possible in \MultiASP it can be faithfully simulated
 in ABS: ABS allows more control on the status of futures. The details about this
new equivalence relation and the matching between \MultiASP and ABS rules can be found in
Appendix~\ref{sec:proof-theor-MA}.


Globally, we are able to prove that our semantics fully respects the
ABS semantics, and fully simulates all the executions where service
ordering is FIFO and where communications are causally ordered. 
%Note that these
%restrictions were already existing in the original Java
%backend for ABS.
These
restrictions were already existing in the original Java backend for ABS, thus the Proactive backend can certainly be used to run ABS programs in a distributed way.

%\subsection{Notes on comparison abs - MASP}
%
%concerning ABS synch calls, the semantics is tricky: it removes the
%interrupted thread and put it in queue and to compensate and ensure
%that the interrupted thread is restored at the right moment, there is
%a special \texttt{Cont} construct that detects the end of a synch
%calls and a special rule for returning to the right \texttt{f.get}.
%
%Contrarily to \cite{cooperative-task-continuation}, \TODO{montrer difference avec ce papier: on prend en compte les COGs, pas eux -- on l'a implemente, pas eux}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
 