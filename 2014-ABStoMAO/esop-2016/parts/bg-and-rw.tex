\section{Background and Related Works}\label{background}

The actor model
was one of the first to schematically consider concurrent entities 
evolving independently and communicating via asynchronous messages.
Later on, active objects have been designed as the object-oriented
counterpart of the actor model.
The principle of active objects is to have a thread associated to them.
We call this notion \emph{activity}: a thread
together with the objects managed by this thread.  Two objects in
different activities communicate with remote method invocations: when an
object invokes a method on an object in another activity, this creates
a \emph{request}; the invoker continues its execution while the
invoked object will serve the request in an asynchronous manner.
Requests wait in a \emph{request queue} until they are executed.
In order to allow the invoker to continue its execution, a
placeholder for the expected result is created. \emph{Futures}
play this role: an empty object that will later be filled
by the result of the request. We say that a future is \emph{resolved}
when its value is known.  
Futures have first
appeared in the Multilisp language \cite{Halstead:1985:MLC:4472.4478} and in ABCL/1~\cite{Yonezawa:1986:OCP:28697.28722}, a pioneer of active objects. Futures
have been formalised in \cite{Flanagan:1995:SFU:199448.199484} afterwards.  
%From
%those seminal works, many active object languages were created and
%implemented in modern platforms such as in
%Android\footnote{http://source.android.com} to provide asynchronous
%background services.

Active objects and actors enforce decoupling of activities: each
activity has its own address space manipulated by its own thread. This
strict isolation of concurrent entities makes them suited to distributed systems. Programming higher-level features
of distributed systems, like distributed components, process migration,
or fault tolerance, is made easier because the notion of activity
provides a convenient abstraction~\cite{ref:asp}. 
However, disparity of implementations of those models often makes them only adapted to restricted settings.
In \cite{Tasharofi:2013:WSD:2524984.2525001}, the authors show
that missing features in the actor model or in its implementation is the main reason for mixing concurrency paradigms in industrial applications, which exhibits a gap between what is offered and real needs.

\subsection{Design choices for active object-based languages}
Each existing active object implementation adopts a different point of
view on three main characteristics that we have extracted and listed below.

\smallskip\noindent\textbf{How are objects associated to activities?}
\label{sec:activity}
We distinguish three different ways to map objects to threads/activities in active object languages and models: 

\begin{itemize}
\item \textit{Uniform Object Model.}  All objects are active objects
  %with their own request queue 
  with their own execution
  thread. All communications between objects involve requests. This model is simpler to formalise and reason about, but leads to scalability issues in practice.
  
\item \textit{Non Uniform Object Model.}  Some objects are not
  active and are only accessible by a single active
  object.
  %, i.e. they are part of its state. 
%An activity contains one active and possibly several passive objects.
  This model is scalable
  as it requires less communication and
  less concurrent threads, but is trickier to formalise and reason about.
  %Reducing the number of activities also reduces the number of
  %references that are globally accessible in the system, and thus enables the
  %instantiation of a large number of objects.

\item \textit{Object Group Model\footnote{This model is also similar to  \emph{group membership}~\cite{Miller05concurrencyamong}.}.}
An activity is made of a set of objects sharing 
%a request queue and 
an execution thread, but all
objects can be invoked from another activity. This
approach has a good scalability and propensity to formalisation, but
the addressing of all objects in distributed settings is difficult to maintain.
%it is still difficult to create a lot of objects as all of them must
%be registered so that they are accessible from any other
%activity. 
%In the ABS case, only one kind of object exists and all objects are directly accessible from any group.
\end{itemize}


\smallskip\noindent\textbf{How are requests scheduled?}
We distinguish three  threading models that occur in active object languages and models:
\begin{itemize}
\item \textit{Mono-threaded.} 
Within an active object, requests are executed sequentially without
interleaving possibilities. 
%Data races and race conditions are avoided.
% This is the threading model of ProActive active objects if no annotation for multiactivity is found.

\item \textit{Cooperative scheduling.}
A running request can explicitly release the execution thread to let
another request progress. Requests are not processed in parallel but they
might interleave. Data races are avoided as a single thread is running at each instant. % Creol, JCoBox, and ABS comply to this threading model.

\item \textit{Multi-threaded.}
Within an active object, requests are executed in parallel using
different threads, without yielding. % This is the threading model of ProActive with multiactive objects.
Data races are possible but only within an activity. 
%This model is
%called a \emph{multiactive} object model.
\end{itemize}


\smallskip\noindent\textbf{Is the programmer aware of distributed aspects?}
Some active object languages use a specific syntax for asynchronous
method calls and a specific type for futures.
%: a different operator is used to distinguish them from
%synchronous method calls. 
This makes the programmer aware of where synchronisation occurs.
%, but requires more experienced programmer.
%the
%places where futures are created. Generally, when asynchronous
%invocation is explicit, there exists a special type for future objects.
%Additionally, the futures, when they are statically identified, can be
%accessed explicitly or implicitly. 
%In case of explicit access, an
%operation like \code{claim}, \code{get}, or \code{touch} is used to
%access a future. In addition, explicit futures allow the programmer to explicitly release the
%current thread if a future value is not available in cooperative
%scheduling models. 
%In case of implicit access, operations that need the future
%value (\emph{blocking} operations) automatically trigger
%a synchronization with the future update operation.
Asynchronous method calls and futures can also be implicit: this enables transparency of distributed aspects, and facilitates transmission of future references (first-class futures~\cite{ref:asp}).
In this case, there is almost no difference between a distributed program and usual
objects, whereas, on the other hand, explicit manipulation of futures allows the
programmer to better control execution, but requires a
better expertise. 
%It is said~\cite{ref:asp} that futures
%are \emph{first class} if future references can be transmitted between
%remote entities like any other object.

% Among them,
% AmbientTalk \cite{Dedecker:2006:APA:2171327.2171349} makes heavy use
% of futures; ASP \cite{ref:asp} enforces sequential request processing
% and waits by necessity for future values; Creol \cite{ref:creol}
% unifies objects with the active object model; JCobox
% \cite{wrongjcobox} and later on, ABS \cite{ref:abs}, balanced the
% existing approaches. 
\subsection{Overview of active object based languages}\label{overview}


\smallskip\noindent\textbf{Creol.}
Creol \cite{ref:creol} is a uniform active object language that
features cooperative scheduling based on \code{await} operations that might
release the execution thread.  Asynchronous invocations and futures are
explicit, but not transmitted between activities.  De Boer et al. provide the semantics of a
language based on Creol in \cite{SDE:BoerCJ07}.
In Creol, like in all cooperative scheduling languages,
while no data race is possible, interleaving of the
different request services triggered by the different release points
makes the behavior more difficult to predict than for the mono-threaded model.
Overall, explicit future access,
explicit release points, and explicit asynchronous calls make Creol rich and precise but also more difficult to program
than the languages featuring more transparency.

 


\smallskip\noindent\textbf{JCoBox.}  JCoBox \cite{ref:jcobox} is an active object
programming model implemented in a language based on Java. It has an object group model,
called CoBox, and features cooperative scheduling.  In each CoBox a
single thread is active at a time; it can be released using \code{await()}.  JCobox
better addresses practical aspects than Creol: it is  integrated with Java and 
the object group model improves scalability. However the implementation does not
support distributed execution. Thread interleaving is similar and has the same advantages
and drawbacks as in Creol.


\smallskip\noindent\textbf{AmbientTalk.}
AmbientTalk~\cite{Dedecker:2006:APA:2171327.2171349} is an object-oriented distributed
programming language that can execute on the JVM.  The most original aspect of
AmbientTalk, inspired from the E programming language~\cite{Miller05concurrencyamong}, is
that a future access is a non-blocking operation: it is an asynchronous call that returns
another future; the method call will be performed when the invoked future is resolved.
%In the meantime, another future represents the result of this method invocation.  
Overall, two activities can only coordinate through  callbacks.  This
inversion of control has the advantage to avoid deadlocks but breaks the program into
independent procedures where sequences of instructions are difficult to enforce.


\smallskip\noindent\textbf{ABS.}\label{abs}
ABS~\cite{ref:abs} is an object-oriented programming language based on active objects that targets
modelling of distributed applications. ABS syntax is based on classes; it is showed on Figure~\ref{fig:classABS}.
\begin{figure}[t]
%{\footnotesize
\begin{center}
  \begin{small}

$         \begin{array}{r@{}c@{}l@{~~}r@{}}
   g&::=& b \bnfor x? \bnfor g \land g'       &\text{guard}\\
  s&::=& \eskip\bnfor x=z\bnfor\text{suspend}\bnfor
       \text{await}\ g  &\text{statement} \\
    &|& \ereturn e \bnfor\eif ess  \bnfor s\semi s \\
 z&::=&  e \bnfor e.\name m(\vect e)\bnfor e!\name
      m(\vect e) &\text{expression with}\\
      &|& \enew{[cog] C(\vect e)}\bnfor x.\text{get} &
      \text{side effect}\\
 e&::=& v\bnfor x\bnfor\ethis\bnfor\text{\it arithmetic-bool-exp}   &\text{expression}\\
 v&::=& \enull\bnfor \text{\it primitive-val} &\text{value}
      \end{array}$
  \end{small}%}
  \end{center}
\caption{Class-based ABS }\label{fig:classABS}
\end{figure}
ABS has an object group model
based on the notion of concurrent object
group (hereafter named \COG). 
%Object fields can only be accessed within the object methods. 
Asynchronous method calls and futures are explicit as in the following example:
%, and there is not yet support for first class futures.
% The following example is an asynchronous call that returns a future of
%parametric type \code{V}:
\lstset{frame=single} 
\begin{lstlisting}
Fut<V> future = object!method();
\end{lstlisting}
Figure~\ref{fig:cog} pictures an ABS configuration 
with a request sending between \COG.
\begin{figure}[t]
\centering
\includegraphics[scale=0.35]{pictures/cogs-final.pdf}
\caption{An example of ABS program execution}\vspace{-2ex}
\label{fig:cog}
\end{figure}
Requests are scheduled in a cooperative manner once again thanks to the \code{await} keyword, used as follows:
% that can be followed by a future or a condition, as in the two following examples:
\lstset{frame=single,
morekeywords={await}}
\begin{lstlisting}
await future?;
await a > 2 && b < 3;
\end{lstlisting}
In those examples, the execution thread is released if the future is not resolved or if the condition is not fulfilled. 
%In those cases, the execution thread is handed over to another ready request. 
%A paused request is ready to resume when the future is resolved (in \code{case ~1}) or when the condition is fulfilled (in \code{case ~2}).
ABS also features a \code{get} accessor to retrieve a future's value; it blocks the execution thread until the future
is resolved: 
\lstset{frame=single,
morekeywords={get}}
\begin{lstlisting}
V v = future.get;
\end{lstlisting}	
%The \code{get} accessor  % (and does not release the execution thread until then).
The ABS tool
suite\footnote{http://tools.hats-project.eu/} provides a wide variety of static verification engines that help
designing safe distributed and concurrent applications. Those engines
include a deadlock analyser \cite{CGM:SoSym2014}, a resource and cost analyser for cloud environments 
\cite{SACO:TACAS14,ABSresourcescost}, and general program properties verification with the ABS-Key tool~\cite{ref:key,ref:noc-abs-key}.
The ABS tool suite also includes a frontend
compiler and several backend translators into various programming
languages (Java, Haskell, Maude). 
The Java backend for ABS translates ABS programs into Java programs but is not able to
generate code that runs in a distributed manner.
%, neither in the ABS language itself nor in any of the existing backend.










%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 
