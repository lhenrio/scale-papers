\section{Introduction}\label{introduction}
Writing distributed and concurrent applications is a challenging task. In
distributed environments, the absence of shared memory makes
information sharing more difficult. In concurrent environments,
information sharing is easy but shared variables must be manipulated with caution.  Several languages
and tools have been developed to handle those two programming
challenges and make distributed and concurrent systems safe by construction.  
Among them, the active object programming model
\cite{ref:Lavender95activeobject} helps building safe multi-core applications in object-oriented programming languages.  The active
object model derives from the actor model
\cite{Agha:1997:FAC:969900.969901}, that is particularly regaining popularity with Scala and Akka\footnote{http://scala-lang.org - http://akka.io}. Active objects and actors share
common features such as asynchronous communication, simplified
concurrency, and independent behavior. 
Active objects and actors decouple method call from method execution, and
by this means enforce sequential processing of requests. Yet, actors communicate through
message passing whereas active objects perform remote method
invocation instead. In practice, those models are natively adapted to distribution, because entities do not share memory and behave independently from each other.

There exist now several programming languages implementing and enhancing in various ways the active
object and actor models. Later, we show that emerging
active object languages provide various programming abstractions or
static guarantees that help the developer designing and implementing more robust distributed systems. 
%It seems difficult to decide whether one of the
%existing solutions is better, due to the variety of aspects to be considered. 
Among
those solutions, ProActive\footnote{http://proactive.inria.fr/}, a Java
middleware implementing multi-threaded active objects, provides a holistic support for the deployment of active objects on distributed infrastructures.  The main objective of this paper is
to reconcile the different active object languages by translating most
of their paradigms into ProActive, thus benefiting from its support for deployment.  We illustrate our approach by applying it on ABS~\cite{ref:abs}, an active object language
that has a wide support for the modelling and verification of distributed applications.


Beyond the generic high-level approach to cross-translate active object languages, the practical contribution of this paper is a ProActive backend for
ABS. This compiler allows a programmer to run a verified ABS program in a distributed manner,
using ProActive underneath. For that, no change in the ABS code is
required except minimal deployment information. In addition, this paper
presents a proof of correctness of the translation and experimental settings for measuring the performance of the programs generated with the ProActive backend. When appropriate, we discuss the
generality of our approach, describing how the solution can be adapted to
other active object models. For this, we refer to the notions defined
in a preliminary categorisation of active object languages. Overall, our contribution can be summarised in four points:
\begin{itemize}[noitemsep,nolistsep]
\item We analyse programming paradigms of the different
  active object models and languages in Section~\ref{background}.
\item We provide a class-based semantics of the multi-threaded active
  objects featured in ProActive in Section~\ref{sec:transl-semanticsAndCorrectness}; it
  also formalises thread management.
\item We present a systematic strategy to translate most of the active object paradigms
  into ProActive, and present more specifically the ProActive backend for ABS in
  Section~\ref{principle}. This section also formalises the translation.
\item We finally prove the equivalence between ABS
  programs and their translation
  %, and discuss the lesson learnt from it 
  in
  Section~\ref{sec:proof}. The shallow encoding of ABS into \MultiASP and the precise
  definition of equivalence between the two semantics highlight similarities and
  intrinsic differences between the two programming models.
\end{itemize}
Finally, the paper is concluded with an experimental evaluation in Section~\ref{experimentation}.
% The paper is organised as follows. Section 2 reviews active object
% languages and discusses motivations. Section 3 explains the principles
% of the translation from ABS to ProActive. Section 4 introduces the
% semantics of ProActive with multi-threaded active objects and the translational semantics.  Section 5



%  In
% the ASP language~\cite{ref:asp}, and in
% ProActive\footnote{http://proactive.inria.fr/} its Java
% implementation, strict sequential execution of requests is guaranteed
% per active object. In other implementations, like
% Creol~\cite{ref:creol}, requests might release temporarily the
% execution thread to allow another request to progress, in what is
% known as cooperative scheduling. The active object model has also been
% applied to object groups, for example in JCoBox~\cite{wrongjcobox} and
% ABS~\cite{ref:abs}. Specifically, ABS is an object-oriented language
% that targets modeling of distributed applications, but that does not
% run in a distributed way. It provides verification tools that help
% designing safe distributed and concurrent systems. The ABS Tool
% Suite\footnote{http://tools.hats-project.eu/} provides a frontend
% compiler and several backend translators into various programming
% languages. In particular, the Java backend translates ABS programs
% into Java programs. However, to our knowledge, there is no fully
% operational backend that offers the possibility to run the program in
% a distributed way.

% On the other hand, ProActive comes as a standard Java library and
% provides support for deploying active objects on clusters and
% grids. We present in this paper a solution to distribute ABS programs
% using the ProActive library. Starting from the initial Java backend,
% we propose the ProActive backend in order to produce a code that is
% easily deployable on thousands of machines. This backend uses what we
% call multi-active objects, i.e. multi-threaded active objects.  The ProActive backend
% makes it possible to experiment ABS programs in practical settings, in addition to verify them with the existing tools. 

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 
