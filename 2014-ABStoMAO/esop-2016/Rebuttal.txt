First of all we would like to thank all the reviewers for their valuable remarks. We do not have any factual disagreement with the reviews except perhaps concerning the first question we reply below. We organise this reply in three parts: major questions in the reviews; comments on the three major concerns of the reviewm; and detailed reply to the reviewers

Major questions in the reviews
===============================
* Review 3 “Why is the speed-up when using 25 machines (50 workers) less than 13?” The reason is very simple: in ABS/Java 50 workers are placed on 4 cores on the same machine, whereas in ABS/ProActive 50 workers are allocated to 25 machines, using 50 cores: 50/4=12.5 would be the theoretical speed-up (we measure 12.7 due to thread contention in the local case). We agree that this could be better explained, but the result is satisfactory.

* Review 3 “What is the performance when running applications with less structured parallelism? How does the performance compare to .. frameworks that … are not necessarily implementations of active objects or actors?”
We did not justify the performance of a ProActive application in the paper as it was already compared to existing technologies in previous works (we agree that those work did not deal with the special case of ABS-ProActive backend):
- HPC in Java: Experiences in Implementing the NAS Parallel Benchmarks, Applied Informatics and Communications 2010 (comparison of distributed active objects with Fortran)
- Multi-threaded Active Objects, Coordination 2013 (local comparison of multi-threaded active object vs multi-threaded Java programs)

* Review 2: "experiments that validate the solution both theoretically and practically" -- what is meant here by theoretical evaluation?
Sorry for the inappropriate English. It was meant that the translational semantics validates the solution theoretically  and that the experiments validate it practically.

* Review 2: “  Section 2.1 -- can you give examples, supporting the described
dichotomy of active objects?“
As it is partially repeated in Section 2.2, we did not put the references for each description, but here is the classification:
- Uniform Object Model: Creol
- Non-uniform Object Model: AmbientTalk and ProActive
- Object Group Model: JCoBox and ABS
- Mono-threaded: AmbientTalk and first version of ProActive
- Cooperative Scheduling: Creol, JCoBox and ABS
- Multi-threaded: ProActive
- Transparent active objects and futures: AmbientTalk and ProActive
- Explicit active objects and futures: Creol, JCoBox and ABS


Comments on the three major concerns
====================================
* The main concerns of several reviewers were about the experimental evaluation section. Although the paper is not focused on experimental analysis, it seemed important for us to show that the ProActive backend was implemented and offered an acceptable performance.
We agree that this part can be removed and kept only in a long version if necessary (suggested by reviewer 3) but we are also convinced that the existence and the correct performance of the implementation contributes to bridging the gap between existing theoretical calculi and existing middlewares.
* Another general remark was about having too strong claims at the beginning of the paper. 
We will pay a particular attention to soften our claims and be more precise. However, concerning all the aspects mentioned we think that we cannot improve the results in the context of this paper, details on each overstated claim are given below.
* About novelty and impact, we believe that outside the active object community the paper also illustrates how to use (classical) theoretical development techniques to get a strong insight on the differences and similarities between languages, like the difference between transparent and explicit futures underlined by reviewer 4.


Detailed answers for other remarks
===================================
(considering the throughout work of the reviewers we think they might be interested in further remarks on their reviews)

*Review 1: “How well MultiASP describes ASP/ProActive?” We have put all the details to reflect faithfully the principles of the library but of course having a formal link seams unrealisable in practice considering the size of the library.

Thank you for the OOPSLA reference.

*Review 2: “What kind of verification can be performed on the ABS-level?”
ABS tools include generic properties on the application behaviour, cost analyses, and deadlock analysis. The verified properties apply in the ProActive translation (modulo the restrictions we identify), this is the main interest of our proof of equivalence. Our results are sufficient to exploit the deadlock analysis tools and the cost analysis tools (this second one associates cost to some executed statements). In the general case, we agree that depending on the property and on the proof system, the equivalence might reveal insufficient. 

“the code snippet on page 6 is mysterious”: we agree the code would be asier to read with  the definition of “o”, we meant that the line using “o” is not blocking neither if o is a local object (e.g. defined by new) nor if it is an active object (e.g. initialized by newActive).

“both chosen formalisms for the source and target of the translation try to reflect the OO-aspects way too precisely.” This is a strategical choice in the formalisation we agree, but having formalisms that reflect the OO notions like methods and interfaces, the imperative aspects, and the local versus remote computations is crucial here. We think that both ABS and ASP chose a formalisation level that made the calculi close to actual programming languages which makes the proofs more tedious but contribute to bridging the gap between theoretical calculi and real-life languages.

*Review 3: 
Overstated claims:
- Applicability of our solution to other languages. We think that introducing the generalization of the approach widens the impact of the paper, but the deep analysis of the translation could only focus on one particular implementation in the paper. 
- Limitation of the proof, in particular due to asynchronous vs. causally ordered communication. This is true but the difference between communication timings is a well known and unavoidable aspect of distributed systems. It is not related to the languages chosen. Also, as the needed technical background is not yet introduced at the beginning of the paper, it seems difficult to define there the restrictions of the solution.

“data races are possible but only within an activity” This was not meant to encapsulate extended active object models using Domains model, or ownership types but was meant to apply to multi-active objects similar to MultiASP. But we agree on the overall remark that this section could be extended with these models

Regarding the second evaluation, after the best degree of distribution is reached (which is an experimental equilibrium), the application spends more time transmitting requests and results than computing. The best overhead is indeed close to 0, but as the size of the use case is rather small (the computation intensiveness significantly scaled down from the first experiment), the results are much less stable which, indeed, would require a better statistical analysis.

Thank you for the updates (sometimes very recent) references

*Review 4: 
about remark on page 14, paragraph 5: it is not mentioned in the paper that we optimized most of the communications to actually embed in the object only what is relevant: the object identifier + the reference to its COG. All the other fields are transient, which mean that they are automatically not included in the object serialization. We still send the object instead of a third party structure for code consistency.

about remark on page 22, we fully agree that this is a major contribution and we should promote it better.

Thank you for the detailed comments and the updates on ABS syntax and implementation.

