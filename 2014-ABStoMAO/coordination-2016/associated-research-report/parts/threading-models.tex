\smallskip \noindent \textit{Cooperative Scheduling.}
%Another fundamental difference between ABS and ProActive is in their threading model.
%Compared to ABS, ProActive does not offer the possibility to pause a request for the benefit of another request.
%In order to have a similar scheduling the translated programs, we use the possibilities of multiactive objects.
%Multiactive objects provide several mechanisms to control their internal scheduling.
%By means of tuning those mechanisms very carefully, we can get a precise scheduling policy. 
%More specifically, the following presents the multiactive object features we use to get the same request scheduling as ABS:
%\begin{itemize}
%\item Groups and compatibilities
%\item Global thread limitation
%\item Thread limitation per group
%
%\item Requests grouping. Requests can belong to the same group of requests thanks to the @Group annotation.
%\item Thread limitation per group. For a given group, we can specify the maximum number of threads that requests of this group can use at the same time.
%\item Thread reservation per group. For a given group, we can specify the number of threads of the thread pool that are devoted to execute requests of this group, i.e. no requests of other groups can use them even if they are free.
%\item Soft and Hard limit switch. We can specify whether the maximum number of threads of the thread pool designates the maximum number of active threads (soft limit) or if it designates the maximum number of total threads (hard limit) 
%\end{itemize}
%In ABS, the cooperative threading model is materialized by the \code{await} statement. In the ProActive backend, we modify the translation of the \code{await} statement to make it use the internal scheduling of multiactive objects.
%To apply our strategy, we need to modify the way the \code{await} and \code{get} ABS statements are translated in the Java backend. 
Active object languages often support sophisticated threading models and have constructs
to impact on the scheduling of requests. Those constructs can be  translated
into adequate request scheduling of multiactive objects. For demonstration, we 
consider here the translation that the ProActive backend gives for ABS \code{await} statements (representative of
cooperative scheduling), and for ABS \code{get} statements (representative of explicit futures).

\smallskip \noindent - \code{await} statements on futures. 
An await statement on an unresolved futures  releases the execution thread, for example: 
%like with  with the following \code{startedFut} future variable in ABS:
\lstset{frame=single, backgroundcolor=\color{white},
morekeywords={await}}
\begin{lstlisting}
await startedFut?;
\end{lstlisting}
In order to have the same behavior in the ProActive translation, we force a
wait-by-necessity.
 %Any method could be chosen for that; 
 We use the \code{getFutureValue} ProActive primitive to do that:
% \footnote{The \code{getFutureValue} primitive is purely syntactic since it calls a dummy method on the future, automatically triggering a wait-by-necessity if it is not resolved.}:
 \lstset{
frame=none,
  backgroundcolor=\color{verylightgray}}
\begin{lstlisting}
PAFuture.getFutureValue(startedFut); 
\end{lstlisting}
As in ProActive a wait-by-necessity blocks the thread, 
%no other
%request could run in this \COG until the future is resolved.
% As we have seen in the section~\ref{ao-models}, the COG object
% receives all the asynchronous method calls of the objects it
% contains, and run them by reflection through the \code{execute}
% method.  When using the ProActive backend, all the asynchronous
% method calls of the entire program are executed through the
% \code{execute} method, so if we control this method, we control the
% way all the asynchronous method calls are executed.
we need to configure the ProActive
\COG class with multiactive object annotations (see Section~\ref{asp}) in order to qualify the \code{execute} method and to specify a 
soft thread limit:
	\setlength\abovecaptionskip{0.25mm}
	\lstset{
frame=none,
  backgroundcolor=\color{verylightgray},
    morekeywords={@DefineGroups, @DefineThreadConfig,@Group,@MemberOf}}
\begin{lstlisting}
@Group(name="scheduling", selfCompatible=true)
@DefineThreadConfig(threadPoolSize=1, hardLimit=false)
public class COG {
  ...
  @MemberOf("scheduling")
  public ABSValue execute(UUID objectID, String methodName, Object[] args) {...}
}
\end{lstlisting}
This configuration allows a thread to process an \code{execute} request while a 
current thread that processes another \code{execute} request is waiting for a future. Indeed, the \code{hardLimit=false} parameter ensures that the threads counted in the 
limit (of 1 thread) are only \emph{active} threads. In the example, the 
thread can be handed over to another \code{execute} request if \code{startedFut} is not resolved, just like in ABS.
%The annotations specify that \code{execute} requests can safely be executed in parallel
%and that a \COG has a limit of one thread, and that the threads blocked in
%wait-by-necessity are not counted in the limit.  Thanks to those few annotations, if the
%future is not resolved at time of \code{await}, the current thread is not counted any more
%in the thread limit. Thus, another thread can be started or resumed to
%serve another request, which actually is the expected behaviour in ABS.
%Altogether, thanks to a few annotations in the COG class, we are able to make the ProActive \code{getFutureValue} primitive work like the \code{await} statement of ABS, even if they had a different behavior initially. 

\smallskip \noindent - \code{get} statements.
%The translation of the \code{get} statement would have been straightforward if we had not configured the \COG class for the translation of the \code{await} statement. %Indeed, by doing so, we have disabled the blocking aspect of the ProActive \code{getFutureValue} primitive. 
%In consequence, we cannot use this primitive by itself anymore to translate the ABS \code{get} statement. 
The ABS \code{get} statement blocks the execution thread to retrieve a future's value, as for example on the previous future variable:
%In the \code{await} case, what causes another request to execute is the setting of the \code{hardLimit} parameter of the COG class to false, which releases the limit so that it counts only active thread. 
%Therefore, we set the \code{hardLimit} parameter to \code{true} just for the time the future is waited.
%This ensures that no new thread is started, and that no waiting thread is resumed, since the global limit of 1 is already reached counting the current thread. When the current future is resolved, setting back the \code{hardLimit} parameter to \code{false} ensures that execution resumes normally.
%Fut<Bool> readyFut = server!start();
\lstset{frame=single, backgroundcolor=\color{white},
morekeywords={get}}
\begin{lstlisting}
Bool started = startedFut.get;
\end{lstlisting}
The ProActive backend translates this ABS instruction into the following  code:
\lstset{
frame=none,
  backgroundcolor=\color{verylightgray},
    morekeywords={@Compatible,@Group,@MemberOf}}
\begin{lstlisting}
getCog().switchHardLimit(true); // the retrieved COG is local: the call is synchronous
PAFuture.getFutureValue(startedFut); 
getCog().switchHardLimit(false);
\end{lstlisting}
This temporarily hardens the
threading policy (i.e. all threads are counted in the thread limit) so that no other 
thread can start while the future is awaited.
%Since the \code{switchHardLimit} call is a local
%method invocation (the obtained \COG is the local one), it does not involve a new thread and it is effective immediately.
%We first set the \code{hardLimit} of the current \COG to
%\code{true}. Note that the \code{switchHardLimit} call is a local
%method invocation
%that does not involve a new thread. This way, we are sure that the
%limit is switched when executing the next line. 
%%Note also that, the hard or soft limit is only checked when creating a
%%new thread; 
%Note also that this change only prevents a new thread from being created or resumed;
%other threads interrupted by an \code{await} statement are still blocked and do not need to be killed; this complies to ABS semantics.
%Then, once
%\code{ready} is resolved, the next
%\code{switchHardLimit} call resets the \code{hardLimit}, to
%restore the previous behavior.
%so that waiting threads are not counted in the global limit of 1 anymore, and can then yield execution to another thread. 
%So thanks to the limit type switch, this translation offers the same behavior as the ABS \code{get} statement. 

\smallskip \noindent - Other synchronisation constructs.
We also tackled the translation of ABS \code{suspend} statements and of
\code{await} statements on conditions. 
%For the latter, we create methods on-the-fly dedicated to the evaluation of conditions, with their own multiactive object group and thread limit. 
%We use an additional multiactive object group and dedicated thread limits to process calls on those methods.
%thread limits to dedicate some threads to the
%execution of condition methods: those executions can occur in parallel
%with the main active thread of the \COG. 
%We do not present the
%implementation of this feature here as it involves too many technical details. However, 
In this paper, we only provide the formal definition of their translation in Section~\ref{sec:translationalsem}. The details of their translation into ProActive code can be found in
\cite{rochas:hal-01065072}.
%\cite{report}.


%Another usage of the ABS \code{await} statement is to use it followed by a condition. For this usage of the \code{await} statement, we cannot rely on the ProActive \code{getFutureValue} primitive at first glance, because a condition is obviously not a future variable. However, we can still wrap the condition evaluation in a method call, possibly returning a future value and waits for it in a \code{getFutureValue} call.
% 
%During compilation, when such an \code{await} statement is encountered, we generate a method that lively checks if the condition is verified. Then, we translate the statement into an asynchronous call to a special method in the COG class that we have added, with signature \code{awaitCondition(objectID, method, parameters[])}.
%We call the \code{awaitCondition} method on the current COG, giving it as parameter the current object and the generated method corresponding to the condition (as well as the condition members as parameters if they are not globally defined).
%Again, we wrap this call in the \code{getFutureValue} primitive, in order to wait for the result of this asynchronous call, i.e. when the condition evaluates to true.
%For example, when the following ABS code is encountered:
%\begin{lstlisting}
%await a == 3;
%\end{lstlisting}
%First, a corresponding method is generated\footnote{The code is lighten for clarity reasons.}:
%\begin{lstlisting}
%public ABSType cond7517d1ff7c52(int x) {
%  while(x != 3) { Thread.sleep(500); } 
%  return ABSUnit.UNIT;
%}
%\end{lstlisting}
%Then, the ABS \code{await} statement is translated into:
%\begin{lstlisting}
%PAFuture.getFutureValue(
%    this.getCog().awaitCondition(this.getId(), "cond7517d1ff7c52", a));
%\end{lstlisting}
%The \code{awaitCondition} call executes by reflection the method \code{cond7517d1ff7c52} on the object retrieved with the given identifier, passing the parameter \code{a}.
%%The reason why we need to rely on an additional method to perform the lively waiting is because we need to release the execution thread slot to let other request progress meanwhile.
%%But without any further configuration, this strategy potentially leads to a deadlock when the generated method gets executed in the COG. Indeed in this case, the \code{awaitCondition} method execution could take up the single execution thread and never release it because, as no other request could progress, the condition would never evaluate to true. In order to prevent the condition method from monopolizing the execution thread, 
%We use multiactive object group thread limits to dedicate some new threads to the execution of condition methods only. 
%We create a new group of requests, \code{waiting}, and we put the \code{awaitCondition} method in this group. We also declare the \code{waiting} group \code{selfCompatible}, because several \code{awaitCondition} requests could evaluate different conditions at the same time. To be able to execute the \code{awaitCondition} requests in parallel, we arbitrary increase the global limit size.
%%, to have at maximum 50 concurrent condition evaluations in one COG, plus 1 thread for processing the standard requests (from the \code{scheduling} group).
%We specify a minimum and a maximum number of threads for the \code{waiting} group so that it remains only one thread for the \code{scheduling} group.
%%As waiting conditions and standard requests should execute in parallel, 
%We declare the \code{scheduling} and \code{waiting} groups compatible. 
%%Nevertheless, we still must prevent the 50 new threads from executing requests of the \code{scheduling} group, because otherwise it would break the ABS cooperative scheduling. 
% %The result of this specification is that, not only the waiting group will be able to use up to 50 threads, but also those 50 threads cannot be used by other groups. 
% %So in fact we have a clear separation between the single thread used by the \code{scheduling} group, and the threads used by the \code{waiting} group.
%The COG class is thus enlarged with new annotations, as follows\footnote{Note that, although effective, this solution could be enhanced with a callback when the condition is true, instead of having a live waiting.}:
%%\begin{figure}[!ht]
%	\setlength\abovecaptionskip{0.25mm}
%	\lstset{ numberstyle=\tiny, stepnumber=1, numbersep=2pt, basicstyle=\ttfamily\scriptsize, keywordstyle=\bfseries,
%    showstringspaces=false,
%    deletekeywords={true, false, public, class},
%    morekeywords={@Compatible,@Group,@MemberOf,minThreads,maxThreads,threadPoolSize}}
%\begin{lstlisting}
%@DefineGroups({
%  @Group(name="scheduling", selfCompatible=true),
%  @Group(name="waiting", selfCompatible=true, minThreads=50, maxThreads=50) })
%@DefineRules({  @Compatible({"scheduling", "waiting"}) })
%@DefineThreadConfig(threadPoolSize=51, hardLimit=false)
%public class COG {
%  ...
%  @MemberOf("waiting")   public ABSType awaitCondition(
%      UUID objectID, String methodName, ABSType[] args, Class<?>[] args) {...}
%}
%\end{lstlisting}
%\end{figure}
%This solution is fully compliant with the ABS semantic and proves the power of multiactive object annotations. 

%%%%%%%%%%\medskip
%%%%%%%%%%
%%%%%%%%%%To summarise, \code{await} and \code{get} constructs, and cooperative scheduling, are
%%%%%%%%%%used in many active object languages; we proposed here a high-level
%%%%%%%%%%translation for those constructs with controlled multi-threading, making cross-translation
%%%%%%%%%%of active object models and languages affordable. Overall, we should underline that we
%%%%%%%%%%provided a faithful translation where futures, requests, and objects of ABS are translated
%%%%%%%%%%into the same objects in ProActive; this will be even more visible in rest of this section and
%%%%%%%%%%in the proof of correctness  (Section~\ref{sec:proof}).
%we illustrated above how cooperative scheduling can be obtained
%from a multiactive objects threading model. This can be applied to all
%cooperative scheduling languages, i.e. ABS, JCoBox, and Creol.

%ASP features a single-threaded threading model. On the
%other side, 
% multiactive objects feature a multi-threaded model, and
% this section illustrated how cooperative scheduling can be obtained
% from multiactive objects.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 
