\section{Background and Related Works}\label{background}

The actor model
was one of the first to schematically consider concurrent entities 
evolving independently and communicating via asynchronous messages.
Later on, active objects have been designed as the object-oriented
counterpart of the actor model.
The principle of active objects is to have a thread associated to them.
We call this notion \emph{activity}: a thread
together with the objects managed by this thread.  Objects from
different activities communicate with remote method invocations: when a method is invoked
on a remote active object, this creates
a \emph{request} in the remote activity; the invoker continues its execution while the
invoked active object serves the request asynchronously.
Requests wait in a \emph{request queue} until they are executed.
In order to allow the invoker to continue execution, a
placeholder for the expected result is created, known as \emph{future}~\cite{Flanagan:1995:SFU:199448.199484}:
 an empty object that will later be filled
by the result of the request. When the value of a future is known, we say that it is \emph{resolved}.
  
%Futures have first
%appeared in the Multilisp language \cite{Halstead:1985:MLC:4472.4478} and in ABCL/1~\cite{Yonezawa:1986:OCP:28697.28722}, a pioneer of active objects. Futures
%have been formalised in \cite{Flanagan:1995:SFU:199448.199484} afterwards.  
%From
%those seminal works, many active object languages were created and
%implemented in modern platforms such as in
%Android\footnote{http://source.android.com} to provide asynchronous
%background services.

%Active objects and actors enforce decoupling of activities: each
%activity has its own address space manipulated by its own thread. This
%strict isolation of concurrent entities makes them suited to distributed systems. Programming higher-level features
%of distributed systems, like distributed components, process migration,
%or fault tolerance, is made easier because the notion of activity
%provides a convenient abstraction~\cite{ref:asp}. 
%However, disparity of implementations of those models often makes them only adapted to restricted settings.
%In \cite{Tasharofi:2013:WSD:2524984.2525001}, the authors show
%that missing features in the actor model or in its implementation is the main reason for mixing concurrency paradigms in industrial applications, which exhibits a gap between what is offered and real needs.

\subsection{Design choices for active object-based languages}
Implementing active objects raises the three following questions:

\smallskip\noindent\textit{How are objects associated to activities?}
\label{sec:activity}
In uniform active object models, all objects are active and have their own execution 
thread (e.g. Creol \cite{ref:creol}). This model is distinguished from non uniform active 
object models which feature 
active and passive objects (e.g. ASP \cite{ref:asp}). Each passive object is a normal 
object not equipped with any 
thread nor request queue; there is no race condition on the access to passive object 
because each of them is accessible by a single active object. In practice, non uniform 
active object models are more scalable, but they are trickier to formalise than uniform 
active object models. A trade-off between those two models appeared with 
JCoBox~\cite{ref:jcobox} that introduced the active object group 
model, where all objects are accessible from any object, but where objects of the same group
% active but where several of them 
share the same execution thread.

\smallskip\noindent\textit{How are requests scheduled?}
The way requests are executed in active objects depends on the threading model used.
In the original programming model, active objects are mono-threaded. 
With cooperative scheduling like in Creol, requests in execution can be paused on some 
condition (e.g. 
awaiting on the resolution of a future), letting another request progress in the 
meantime. In all cooperative active object languages,
while no data race is possible, interleaving of the
different request services (triggered by the different release points)
makes the behaviour more difficult to predict than for the mono-threaded model.
Still, the previous models are inefficient on multi-cores and can lead to deadlocks due to 
reentrant calls and/or inadequately placed release points.  
Newest active object models like multiactive objects~\cite{ref:mao} and 
Encore~\cite{ref:encore} feature controlled multi-threading.
Such active object models succeed in maximising local parallelism while avoiding 
communication overhead, thanks to shared memory between the different 
threads~\cite{ref:mao}. Also, controlled multi-threading prevents many deadlocks in 
active object executions. 

\smallskip\noindent\textit{Is the programmer aware of distributed aspects?}
Existing implementations of active objects either choose to hide asynchrony and distribution 
or, on the contrary to use an explicit syntax for handling asynchronous method calls 
and to use an explicit type for handling futures. This makes the programmer aware of 
where synchronisation occurs, but 
consequently requires more expertise. The choice of transparency also impacts the 
language possibilities, like future reference transmission: it is easier  to transmit 
futures between active objects when no specific future type is used, and the programmer 
does not have to know how many future indirections have to be unfolded to get the final value.
 
\subsection{Overview of active object-based languages}\label{overview}

\smallskip\noindent\textit{Creol}~\cite{ref:creol} is a uniform active object language that
features cooperative scheduling based on \code{await} operations that can
release the execution thread.  In this language, asynchronous invocations and futures are
explicit, and futures are not transmitted between activities. 
 De Boer et al. formalised such futures based on Creol in \cite{SDE:BoerCJ07}.
Overall, explicit future access,
explicit release points, and explicit asynchronous calls make Creol rich and precise but also more difficult to program
than the languages featuring more transparency.

 


\smallskip\noindent\textit{JCoBox}~\cite{ref:jcobox} is an active object
programming model implemented in a language based on Java. It has an object group model,
called CoBox, and also features cooperative scheduling.  In each CoBox, a
single thread is active at a time; it can be released using \code{await()}.  JCoBox
better addresses practical aspects than Creol: it is  integrated with Java and 
the object group model improves thread scalability, however JCoBox does not
support distributed execution. Thread interleaving is similar and has the same advantages
and drawbacks as in Creol.


\smallskip\noindent\textit{AmbientTalk}~\cite{Dedecker:2006:APA:2171327.2171349} is an object-oriented distributed
programming language that can execute on the JVM. One original aspect of AmbientTalk is
that a future access is a non-blocking operation: it is an asynchronous call that returns
another future; the call will be performed when the invoked future is resolved.
%In the meantime, another future represents the result of this method invocation.  
The AmbientTalk future model forces two activities to coordinate only through callbacks. This
inversion of control has the advantage to avoid deadlocks but also breaks the program into
independent procedures where sequences of instructions are difficult to enforce.


\smallskip\noindent\textit{ABS}\label{abs}~\cite{ref:abs} is an  active object-based language that targets
modelling of distributed applications. The fragment of the ABS syntax regarding the 
concurrent object layer is shown on Figure~\ref{fig:classABS}.
\begin{figure}[t]
%{\footnotesize
\begin{center}
  \begin{small}
$         \begin{array}{r@{}c@{}l@{~~}r@{}}
   g&::=& b \bnfor x? \bnfor g \land g'       &\text{guard}\\
  s&::=& \eskip\bnfor x=z\bnfor\text{suspend}\bnfor
       \text{await}\ g  &\text{statement} \\
    &|& \ereturn e \bnfor\eif ess  \bnfor s\semi s \\
 z&::=&  e \bnfor e.\name m(\vect e)\bnfor e!\name
      m(\vect e) \bnfor \enew{[cog] C(\vect e)}\bnfor x.\text{get}&\text{~~expression 
      with 
      side effect}\\
%      &|&  &
%      \text{}\\
 e&::=& v\bnfor x\bnfor\ethis\bnfor\text{\it arithmetic-bool-exp}   &\text{expression}\\
 v&::=& \enull\bnfor \text{\it primitive-val} &\text{value}
      \end{array}$
  \end{small}%}
\vspace{-2ex}
  \end{center}
\caption{ Class-based syntax of the concurrent object layer of ABS. Field access is 
restricted to current object ($\ethis$).}\label{fig:classABS}
\end{figure}
ABS has an object group model, like JCoBox,
based on the notion of concurrent object
group (hereafter \COG). 
%Object fields can only be accessed within the object methods. 
Asynchronous method calls and futures are explicit:
%, and there is not yet support for first class futures.
% The following example is an asynchronous call that returns a future of
%parametric type \code{V}:
\lstset{frame=single} 
\begin{lstlisting}
Fut<V> future = object!method();
\end{lstlisting}
Figure~\ref{fig:cog} pictures an ABS configuration 
with a request sending between \COGs.
\begin{figure}[t]
\centering
\includegraphics[scale=0.35]{pictures/cogs-final.pdf}
\caption{An example of ABS program execution}\vspace{-2ex}
\label{fig:cog}
\end{figure}
Requests are scheduled in a cooperative manner thanks to the \code{await} keyword, inspired from Creol and JCoBox and used as follows:
% that can be followed by a future or a condition, as in the two following examples:
\lstset{frame=single,
morekeywords={await}}
\begin{lstlisting}
await future?;  await a > 2 && b < 3;
\end{lstlisting}
In those examples, the execution thread is released if the future is not resolved or if the condition is not fulfilled. 
%In those cases, the execution thread is handed over to another ready request. 
%A paused request is ready to resume when the future is resolved (in \code{case ~1}) or when the condition is fulfilled (in \code{case ~2}).
ABS also features a \code{get} accessor to retrieve a future's value; it blocks the execution thread until the future
is resolved: 
\lstset{frame=single,
morekeywords={get}}
\begin{lstlisting}
V v = future.get;
\end{lstlisting}	
%The \code{get} accessor  % (and does not release the execution thread until then).
The ABS tool
suite\footnote{http://abs-models.org/} provides a wide variety of static verification engines that help
designing safe distributed and concurrent applications. Those engines
include a deadlock analyser \cite{CGM:SoSym2014}, a resource and cost analyser for cloud environments 
\cite{SACO:TACAS14}, and general program properties verification with the ABS-Key 
tool~\cite{ref:key}. % LUDO removed: ,ABSresourcescost ,ref:noc-abs-key for space reason
The ABS tool suite also includes a frontend
compiler and several backend translators into various programming
languages. 
The Java backend for ABS translates ABS programs into concurrent Java 
code that runs on a single machine. 
%\TODO{Point 6a begin}
The Haskell backend for ABS~\cite{Bezirgiannis2016} performs the translation into 
distributed Haskell code. The ABS semantics is preserved thanks to the thread 
continuation support of Haskell, which is not supported on the JVM. % \TODO{Point 6a end}
%, but is not able to
%generate code that runs in a distributed manner.
%, neither in the ABS language itself nor in any of the existing backend.










%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 
