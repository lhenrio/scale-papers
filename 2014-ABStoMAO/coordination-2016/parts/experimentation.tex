\section{Experimental Evaluation}\label{experimentation}
% We give in this section practical results for the ProActive backend.
In order to test the ProActive backend, we 
first use four
example programs given in the ABS tool
suite~\cite{abs-tutorial}. These  examples are:
a \textit{\small{Bank account}} program that consists of 167 lines of ABS and that creates 3
\COG; a \textit{\small{Leader election}} algorithm over a ring (62 lines, 4 \COG); a
\textit{\small{Chat}} application (324 lines, 5 \COG), and a \textit{\small{Deadlock}} example that
hangs by circular dependencies between activities (69 lines, 2 \COG).
% We translate those programs using the ProActive backend in one hand and using the existing Java backend on the other hand.
% Through the ProActive backend, these programs use one JVM per
% ABS \COG; through the Java backend, a single JVM is used to run each program. 
For all examples, we observe that the behavior of the program translated with the
ProActive backend is the same as the one translated 
with the Java backend. % More precisely, the \textit{\small{Bank
%   account}}, \textit{\small{Leader election}} and \textit{\small{Chat}}
% applications give the same result. For the
% \textit{\small{Deadlock}} example, both programs hang as expected. 
Thus, the
scheduling policy enforced in ProActive faithfully respects the one
of the reference implementation. These examples  run in a few milliseconds; they are inadequate for
performance analysis but they allow us to check that the ProActive backend
behaves correctly.

% aa cog is correctly in the ProActive program.

% We compared the result produced by the program
% obtained with the initial Java backend and with the ProActive backend.
% We ran the produced ProActive programs on a single machine, resulting
% in a pseudo distributed mode (there are as many Java Vitual Machines
% as cogs in the program). Results are listed in the following table:
% \begin{table}[h]
% \begin{tabular}{|c|c|c|}
% \hline
% \textbf{\begin{tabular}[c]{@{}c@{}}Example \\ program\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}\small{Number of cogs} \\ \small{= number of} \\ \small{JVM in ProActive}\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}\small{Result with the}\\ \small{ProActive backend}\end{tabular}} \\ \hline
% Deadlock                                                            & 2                                                                                                  & Hangs                                                                                \\ \hline
% Bank account                                                        & 3                                                                                                  & \begin{tabular}[c]{@{}c@{}}Gives\\ initial result\end{tabular}                       \\ \hline
% Leader election                                                     & 4                                                                                                  & \begin{tabular}[c]{@{}c@{}}Gives\\ initial result\end{tabular}                       \\ \hline
% Chat                                                                & 5                                                                                                  & \begin{tabular}[c]{@{}c@{}}Gives\\ initial result\end{tabular}                       \\ \hline
% \end{tabular}
% \end{table}
% For all example programs, the behavior of the program produced with the ProActive backend is the same as the behavior of the program produced with the initial Java backend. More precisely, the \textit{Bank account}, \textit{Leader election} and \textit{Chat} example applications give the same result in the two cases. For the \textit{Deadlock} example application, the program obtained with the ProActive backend hangs, as the initial one, which also means that the scheduling policy enforced in a cog is correctly represented in this case in the ProActive program.

We have also conducted an experimental performance evaluation of an ABS distributed application translated with the ProActive backend and distributed on a cluster. We compare it to the performance of the same ABS application translated with the Java backend, run on a single machine. The considered use case is the pattern matching of a DNA sequence using the MapReduce programming model~\cite{Dean:2008:MSD:1327452.1327492}.
%We implement the MapReduce programming model in ABS with a MapReduce class that is responsible for splitting the input data and dispatching \code{map} and \code{reduce} calls to several workers (class Worker). Each worker is instanciated in a new COG so that workers can work in parallel. Workers do not communicate with each other, they communicate with the MapReduce object only.
Map instances (workers) are created in their own \COG to make them work in parallel.
We consider a searched pattern of 250 bytes, and a database of 5 MB of DNA sequences.
Each map searches for the maximum matching sequence of a chunk.
%Each map receives 100 (pattern, database chunk) pairs and searches for the maximum matching sequence of the pattern on the given database chunk, and returns it.
Then, a reducer outputs the global maximum matching sequence.
%The map algorithm is shown in Algorithm~\ref{alg1}.
%\begin{algorithm}
%\caption{Maximum matching sequence algorithm - map phase}
%\label{alg1}  
%\begin{algorithmic}
%\While {$pattern$ not ended} 
%	\While {$sample$ not ended}
%		\While {$pattern$ matching $sample$}
%			\State update maximum matching
%		\EndWhile
%	\EndWhile
%\EndWhile
%\end{algorithmic}
%\end{algorithm}
We compute the execution time of the whole use case when varying the number of workers for
both generated applications. In the case of ProActive translation, the number
of workers is twice the number of physical machines used 
(two workers run on each machine). In the case of the Java translation, all
workers run in parallel on a single machine (no support for distribution).
%When
%using the initial Java backend, we run the use case on one single
%machine, since it does not support distribution. When using the
%ProActive backend, we deploy each time two \COG (i.e. two active objects) per machine. 
All used machines have 2 dual
core CPUs at 2.6GHz, and 8 GB of RAM\footnote{We use a cluster of Grid5000 platform: \url{http://grid5000.fr}}.

\addtolength{\textfloatsep}{-.5ex}
\stepcounter{footnote}
\footnotetext{Each measurement is an average of five executions.}
\begin{figure}[t]
		\begin{subfigure}[b]{0.50\linewidth}
                 \includegraphics[scale=0.31]{pictures/usecase-final.png} 
                \caption{Against existing solution}
                \label{fig:bench-usecase}
        \end{subfigure}
        \begin{subfigure}[b]{0.50\linewidth}
                  \includegraphics[scale=0.31]{pictures/native-final.png} 
                \caption{Against native code}
                \label{fig:bench-native}
        \end{subfigure}
 \caption{Execution time of DNA-matching ABS application${}^{\thefootnote}$} 
\label{fig:graph}
\end{figure}

Figure~\ref{fig:bench-usecase} shows execution time of both ProActive and Java translations of the ABS application from 2 to 50 workers, therefore using from 1 to 25 physical machines with ProActive. 
%Figure~\ref{fig:bench-usecase} compares the use case generated with the ProActive backend and the use case produced with the original Java backend. Both versions correspond to the same ABS code, apart from the node names added for the distributed case.
The execution time of the application stemming from the ProActive backend is sharply decreasing for the first added machines and then decreases at a slower rate. On the other hand, the application stemming from the Java backend has an optimal degree of parallelism of 4 workers (which actually is the number of cores of the machine) and then cannot benefit from higher parallelism; execution time even increases afterwards due to context switching overhead.
%Then, when the number of workers increases, the execution time slightly increases, although the degree of parallelism is augmented. This raise is due to an increasing context switching overhead, as all the threads are executing on the same machine.
On the opposite, increasing the degree of parallelism for the application stemming from the ProActive backend gives a linear speedup, because it balances the load between machines. 
%An exception to this rule is the first experimented value, where the distributed version runs slower than the local one: it actually represents the overhead of distribution. 
%Distribution should indeed be avoided when enough parallelism can be achieved locally.
%it is overkill to distribute when the desired degree of parallelism can be achieved locally.
%In the other cases, the speedup from the initial Java backend to the ProActive backend grows linearly with the number of machines. The use case completes up to more than 12 times faster with the ProActive backend. 
%We can highlight that simply using 10 machines makes the use case complete 5 times faster, precisely in 19 minutes instead of 1 hour and 35 minutes.
%In the best case, the code produced by the ProActive backend runs 12 times faster on 25 machines than the code produced by the original Java backend on a single machine.

Figure~\ref{fig:bench-native} compares now the performance of the generated ProActive code to a hand-written version. 
%For the sake of this experiment, 
In the generated program, we have manually replaced the translation of functional ABS types 
%(primitive types and data structures)
(integers, booleans, lists, maps) with corresponding standard Java types. Indeed, our point is to evaluate the additional communication cost of our translation, not the performance of ABS types compared to standard Java types\footnote{As can be noticed on the generated ProActive of Figures~\ref{fig:bench-usecase} and~\ref{fig:bench-native}, there is an order of magnitude of difference depending on the types used; fixing this is 
%in order to fairly compare generated and native code (itself using standard Java types)
%a more efficient implementation of ABS types in Java is 
on-going~\cite{ref:ABS-Java-translate}.}. In this context, we can see that the overhead introduced by the ProActive backend is rather low since it is generally kept under 10\%. %for the best use of distribution. 
At the biggest stage, the application translated with the ProActive backend starts to have a higher overhead because it involves too much communication. 
%Indeed, as observed with a debugging tool specially developed for multiactive objects\footnote{\url{https://hal.inria.fr/view/index/docid/1212170}}, object instantiation is much slower with the backend because it requires more communications, that delay the burst of work.
%Thus, instantiation of workers becomes more substantial than the business computation, slowing down the overall use case. 
%Indeed the ProActive backend introduces a lot of communications, that are multiplied when adding more entities (more \COGs).
In conclusion, thanks to the ProActive backend, we were able to turn a local ABS application into a high-performance distributed application. 
%when the distribution degree is adapted to the application, the ProActive backend for ABS is extremely good at reducing execution time for free, compared to a local solution. 
In addition, the code generated by the ProActive backend maintains a low overhead compared to a native solution.

%And in the best case, the code produced by the ProActive backend runs 12 times faster on 25 machines than the code produced by the original Java backend on a single machine.
%However, the distributed code performs slower than the local one for the first experimented value, because of the overhead introduced by distribution: a minimum degree of parallelism must be established to make the distribution beneficial.
%Nevertheless, we can also notice that, at the lowest number of workers depicted, the regular Java backend performs better than the ProActive backend: it is 7\% faster. This is due to the overhead induced by the distribution: distributing involves more computing units but costs more in terms of serialization and communication. Thus, it is useless to use the ProActive backend for a small number of workers, because the parallelism achieved in shared memory is more efficient in this case.
%This shows that the ProActive backend should be used only when the
%use case includes computationally intensive work, that can be split
%into subsets, to distribute the computational load across several
%machines. However, the ProActive backend is small enough to be
%beneficial quickly, as the size of the use case grows.
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 
