\section{Background}\label{background}

Many programming languages embed constructs to safely and gracefully
handle parallelism. The actor model \cite{Agha:1997:FAC:969900.969901}
was the first to schematically consider concurrent entities that 
evolve independently and communicate via asynchronous messages. The
actor model is implemented in modern languages such as Scala through
the Akka framework\footnote{http://scala-lang.org - http://akka.io}.
Later on, active objects were designed as the object oriented
counterpart of the actor model where communication is performed by
remote method calls, using futures for handling responses.  Futures first
appeared in the Multilisp language \cite{Halstead:1985:MLC:4472.4478} and in ABCL/1~\cite{Yonezawa:1986:OCP:28697.28722}, a pioneer of active objects. Later on, futures
were formalized in \cite{Flanagan:1995:SFU:199448.199484}.  From
those seminal works, many active object languages were created and
implemented in modern platforms such as in
Android\footnote{http://source.android.com} to provide asynchronous
background services.


The principle of active objects is to associate a thread to an object,
or to a set of objects. We call this notion \emph{activity}: a thread
together with the objects managed by this thread.  Two objects in
different activities communicate by remote method invocation: when an
object invokes a method on an object in another activity, this creates
a \emph{request}; the invoker continues its execution while the
invoked object will serve the request in an asynchronous manner.
Requests wait in a \emph{request queue} until they are executed. Like
in any object-oriented language, method invocations return a
result. In order to allow the invoker to continue its execution, a
placeholder for the expected result must be created. \emph{Futures}
play this role: a future is an empty object that will later be filled
by the result of a request. We say that a future is \emph{resolved}
when its value is known.  
%Overall, the active object paradigm makes
%programming of distributed applications more natural, especially when
%applications are made of computational entities that may have a state.
By nature, active objects enforce decoupling of activities: each
activity has its own address space manipulated by its own thread; this
strict isolation of concurrent entities is what makes active objects
adapted to distributed systems. Then, programming higher-level features
of distributed systems, like distributed components, process migration,
or fault tolerance, is made easier because the notion of activity
provides an abstraction that is convenient for programming those
features~\cite{ref:asp}.  
\subsection{Classification of active object based languages}
Each active object implementation adopts a different point of
view on the following characteristics: the way active objects are
created, the way they perform asynchronous method calls and
synchronization on futures, and the way they manage their threads. We
analyze these characteristics below.

\paragraph{How are objects associated to activities?}
\label{sec:activity}
We distinguish three different ways to map objects to threads/activities: 

\noindent \textit{-- Uniform Object Model.}  All objects are active objects
  with their own request queue and their own execution
  thread. All communications between objects occur by posting
  a request. This model is simple to formalize but is more difficult to apply as it must rely on logical threads
  to allow scalability.
  
\noindent \textit{-- Non Uniform Object Model.}  Some of the objects are not
  active, in which case they are only accessible by a single active
  object, i.e. they are part of its state. 
An activity contains one active and possibly several passive objects.
Non-uniform active object models
  are much more efficient as they require less communications and
  less concurrent threads than models where each object is
  active. Reducing the number of activities also reduces the number of
  references that are globally accessible in the system, and thus enables the
  instantiation of a large number of objects.

\noindent \textit{-- Object Group Model.}
In this model, an activity is made of a set of objects sharing a
request queue and an execution thread, but all
objects can be invoked from another activity. This
approach improves scalability as it reduces the number of threads, but
it is still difficult to create a lot of objects as all of them must
be registered so that they are accessible from any other
activity. This model is also similar to  \emph{group membership}~\cite{Miller05concurrencyamong}.
%In the ABS case, only one kind of object exists and all objects are directly accessible from any group.


\paragraph{How are requests scheduled?}
We distinguish three  threading models in active object languages.

\noindent \textit{-- Mono-threaded.} 
Within an active object, requests are executed sequentially without
interleaving possibilities. % This is the threading model of ProActive active objects if no annotation for multiactivity is found.

\noindent \textit{-- Cooperative scheduling.}
A running request can explicitly release the execution thread to let
another request run. Requests are not processed in parallel but they
might interleave. Data races are avoided. % Creol, JCoBox, and ABS comply to this threading model.

\noindent \textit{-- Multi-threaded.}
Within an active object, requests are executed in parallel using
different threads, without pausing nor yielding. % This is the threading model of ProActive with multiactive objects.
Data races are possible but only within an activity. This model is
called a \emph{multiactive} object model.


\paragraph{Is the programmer aware of distributed aspects?}
Some active object languages use a specific syntax for asynchronous
method calls: a different operator is used to distinguish them from
synchronous method calls. This makes the programmer aware of the
places where futures are created. Generally, when asynchronous
invocation is explicit, there exists a special type for future objects.
Additionally, the futures, when they are statically identified, can be
accessed explicitly or implicitly. In case of explicit access, an
operation like \code{claim}, \code{get}, or \code{touch} is used to
access a future. In addition, explicit futures allow the programmer to explicitly release the
current thread if a future value is not available in cooperative
scheduling models. In case of implicit access, operations that need the future
value (\emph{blocking} operations) automatically trigger
a synchronization with the future update operation.
Implicit future creation enables transparency of distributed aspects:
there is almost no difference between a distributed program and usual
objects, whereas explicit manipulation of futures allows the
programmer to better control execution, but requires a
better expertise. It is said~\cite{ref:asp} that futures
are \emph{first class} if future references can be transmitted between
remote entities like any other object.

% Among them,
% AmbientTalk \cite{Dedecker:2006:APA:2171327.2171349} makes heavy use
% of futures; ASP \cite{ref:asp} enforces sequential request processing
% and waits by necessity for future values; Creol \cite{ref:creol}
% unifies objects with the active object model; JCobox
% \cite{wrongjcobox} and later on, ABS \cite{ref:abs}, balanced the
% existing approaches. 
\subsection{Overview of active object based languages}\label{overview}
We present below the main active object languages, described accordingly to the
classification given above.


\paragraph{Creol.}
Creol \cite{ref:creol} is a uniform active object language that
features cooperative scheduling based on \code{await} operations (that might
release the active thread).  Asynchronous invocations and futures are
explicit, but there is no first class future.  De Boer et al.
\cite{SDE:BoerCJ07} provide the semantics of a
language based on Creol.
In Creol, like in all cooperative scheduling languages,
while no data race is possible, interleaving of the
different request services triggered by the different release points
makes the behavior more difficult to predict than for the mono-threaded model.
Overall, explicit future access,
explicit release points, and explicit asynchronous calls make Creol rich and precise but also more difficult to program
than the languages featuring more transparency.

 

\paragraph{JCoBox.}
JCoBox \cite{ref:jcobox} is an active object programming model
implemented in a language based on Java. It has an object group model
based on coboxes and features cooperative scheduling.  Similarly to
Creol, in each cobox, a single thread is active at a time,
it can be released using \code{await()}. The
practical implementation and the object group model makes the language more
applicable than Creol, and more scalable; however JCoBox
implementation does not support distribution. Thread interleaving is similar and has
the same advantages and drawbacks as Creol.

\paragraph{ABS.}\label{abs}
ABS~\cite{ref:abs} is an object oriented language that targets
modeling of distributed applications. ABS has an object group model
based on the notion of concurrent object
group (\COG). Object fields can only be accessed within the object methods. Asynchronous calls and futures are explicit.
%, and there is not yet support for first class futures.
 The
following example is an asynchronous call that returns a future of
parametric type \code{V}:
\lstset{frame=single} 
\begin{lstlisting}
Fut<V> future = object!method();
\end{lstlisting}
Figure~\ref{fig:cog} pictures an ABS configuration 
with a request sending between {\COG}s.
\begin{figure}[tb]
\centering
\includegraphics[scale=0.35]{pictures/cog-final.pdf}
\caption{An example of ABS program execution}\vspace{-2ex}
\label{fig:cog}
\end{figure}
Requests are scheduled in a cooperative manner thanks to the \code{await} keyword that can be followed by a future or a condition, as in the two following examples:
\lstset{frame=single,
morekeywords={await}}
\begin{lstlisting}
await future?;
await a > 2 && b < 3;
\end{lstlisting}
Those examples cause the execution thread to yield if the future is not resolved or if the condition is not fulfilled. 
%In those cases, the execution thread is handed over to another ready request. 
%A paused request is ready to resume when the future is resolved (in \code{case ~1}) or when the condition is fulfilled (in \code{case ~2}).
The \code{get} accessor is applied on a future  and retrieves
 its value; it blocks the execution thread until the future
is resolved: 
\lstset{frame=single,
morekeywords={get}}
\begin{lstlisting}
V v = future.get;
\end{lstlisting}	
%The \code{get} accessor  % (and does not release the execution thread until then).
The ABS tool
suite\footnote{http://tools.hats-project.eu/} provides a wide variety of static verification engines that help
designing safe distributed and concurrent systems. Those engines
include deadlock analysis \cite{CGM:SoSym2014} and resource/cost analysis 
\cite{SACO:TACAS14}.
ABS tools also include a frontend
compiler and several backend translators into various programming
languages. 
The Java backend produces  Java programs that run
in a single memory space without support for distributed execution.
%, neither in the ABS language itself nor in any of the existing backend.




\paragraph{AmbientTalk.}
AmbientTalk is an object oriented distributed programming language that can execute on a JVM. 
The most original aspect of
AmbientTalk~\cite{Dedecker:2006:APA:2171327.2171349}, inspired from
E~\cite{Miller05concurrencyamong}, is that a future access is a non-blocking operation: it is an asynchronous
call that returns another future.  Consequently, there is no wait-by-necessity upon
a method call on a future, instead the method call will be performed
when the future is resolved. In the meantime, another future represents
the result of this method invocation.  There is no implicit synchronization, so
programming can become tricky as 
the synchronization of two processes must occur by explicit
callbacks. 
This inversion of control avoids deadlocks but
breaks the program into  several independent procedures where sequence of instructions are difficult to enforce.






%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../ABStoPA"
%%% End: 
