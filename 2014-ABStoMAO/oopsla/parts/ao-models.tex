\subsection{Object Instantiation}\label{ao-models}
%ABS and ASP/ProActive are both object-oriented languages that provide a form of active objects. 
% \TODO{what do we do of this?}

% One of the characteristics of ABS is that all objects are accessible
% from all other objects, regardless of whether they are in the same \COG
% or not. The programmer always manipulates a direct reference to an
% object. When running an application in a distributed manner, an
% application-level index must be maintained in order to access objects
% that are not in the same memory space, and in general to access
% objects throughout the network. To understand the challenge, consider
% the following ABS program:
% \begin{lstlisting}
% Server server = new cog Server();
% Fut<Int> portFut = server!getPort();
% Int port = portFut.get;
% \end{lstlisting}
% This program executes in a \COG\footnote{An ABS piece of code is always
%   executed in a \COG, which can be the default one.}, and creates a new
% \COG containing a new object \code{server}. The server's port is then
% retrieved through an asynchronous method call and further designated
% by the local variable \code{port}. According to the ABS semantics,
% \code{port} must be a direct reference to the \code{port} object. But
% as the \code{port} object is supposed to be in another memory space
% (in another \COG), an
% indexing system must be set to remotely reference the \code{port}
% object. The problem is that then all objects should appear in the
% indexing system, as they should be referenced as if they were local.
% This would doubtlessly lead to a scaling problem.

% In contrast, in ProActive, a direct remote reference can only be a
% reference to an active object. For the other objects, a copy of the
% object is accessed instead. The reason for that is to keep the
% footprint of the global indexing system low. In order to get a
% scalable and efficient ProActive backend, it is necessary to comply
% with this general rule.




The characteristics of the active object model drives the object
creation process. To handle the differences between two active object
languages, we need first to define what happens when a new object is created
(active or not). In ABS, all objects are accessible from all others whereas in
JCoBox, some objects can be private. 
We target here the translation to a non-uniform active
object model;
%In particular, we need to define the ProActive code that would be equivalent to the ABS new cog statement.
%Since in ABS objects should be accessible from all others, 
one could think that implementing all objects with an active object
would meet the requirement. However, in practice, this is hardly
manageable: active objects from non-uniform active object model are
usually associated to a plain thread (not a logical thread). Thus,
this solution inevitably leads to a substantial context switch
overhead, as the number of objects is large in an application, harming
its overall performance. Additionally in ABS and JCoBox, objects of
the same \COG share the same thread; simulating this cooperative
multi-threading with different active objects would require tricky
synchronizations and would be inefficient. 
%Obviously, we do not want to change the object model of ABS nor of ProActive, and as it is not possible, using ProActive, to create all objects as active objects, we have to select which objects will be ProActive active objects, and which ones will be passive objects.
We adopt an alternative solution: we put several objects under the control of one
active object. In  ProActive, we introduce a new class \COG for
instantiating ABS \COGs; only the objects of this class are active.
%In the initial implementation of the Java backend, a COG was implemented as a regular Java object. We select then the COG objects as ProActive active objects. 
%The reasons for this choice are the following. First, it is natural to consider the COG as the unit of distribution, and ProActive active objects are the units of distribution. Second, when a ProActive active object is created, it is automatically put in a registry that is accessible through the network\footnote{This is in fact the RMI registry, as ProActive is based on RMI.}. A COG object is thus accessible network-wide via a URL. Thanks to the registry, a COG object can be the entry point to all the objects it contains. All objects other than COG objects can then remain passive, thus preserving the performance of the ProActive backend.
%Thus, we have a two-level hierarchical indexing of objects:
%\begin{itemize}
%\item A first level of network-wide accessible COG objects. This mechanism is integrated in ProActive as it is based on RMI~\cite{Wollrath:1996:DOM:1268049.1268066}.
%\end{itemize}
This choice implies that the application always manipulates local references except for \COG objects that can be manipulated through remote references.
To apply this strategy,  we need to implement the translation of the ABS \code{new cog} statement.
Consider the following ABS  code:
\lstset{frame=single, backgroundcolor=\color{white},
morekeywords={cog}}
%[belowskip=0.2 \baselineskip]
\begin{lstlisting}
Server server = new cog Server();
\end{lstlisting}
We translate it into four ProActive lines of code:
\lstset{
frame=none,
  backgroundcolor=\color{verylightgray},
    emph={deployer,absRuntime}, emphstyle=\itshape,
    deletekeywords={cog}}
\begin{lstlisting}
Server server = new Server();			
COG cog = PAActiveObject.newActive(COG.class, 
	   new Object[]{absRuntime, Server.class, deployer},
	   deployer.getANode());			
server.setCog(cog);				
cog.registerObject(server);			
\end{lstlisting}
Line \code{1} creates a regular server object. Lines
\code{2-4} uses \code{newActive} to create
a new \COG active object. Additionally to the constructor parameters, we
 specify onto which node it should be deployed.
%(we will go back to this point in Section~\ref{distribution})
Line \code{5} makes the local server aware of its \COG.
%(all objects inherit from the \code{ABSObject} class that has a \code{setCog} method)
Finally, the new object is registered in the newly created \COG in line \code{6}, making
it accessible in the local memory space of the remote \COG.  
For that, the \COG  uses a map from object identifiers to object
references. Each ABS object created by \code{new} is
registered in the \COG of the creator object. 

 To be more efficient in a distributed
setting,  ProActive features a non-uniform active
object model: a single object in each activity needs to be remotely accessible by a global reference; this improves the
scalability as a lot of passive objects can be instantiated without
overhead. To allow ABS programs to remotely access any object, we use a
two-level reference system: each \COG is accessible by a
ProActive global reference; and each ABS object has an identifier,
local to its \COG, that is used to 
access the ABS object inside its \COG.
The pair (\COG, identifier) is a unique reference for each
object, and the local reference used in the non-distributed implementation  is
replaced by this two-level reference.
%Once the object has been created, it can then be used and transmitted
%between cogs. However, 
When objects
are transmitted between \COG (as parameters of remote method invocations), a copy is transmitted by the
ProActive framework; this copy will  be used as a proxy (a
reference to the original ABS object) and
only the ABS object initially registered in its \COG is accessed and modified.


The same strategy can be applied to JCoBox in which a cobox groups
objects.  For uniform active object models like Creol, one
would have the choice between creating one active object per active
object which eases the translation but limits scalability, or grouping
several objects behind a same active object.
Indeed making all the objects remotely accessible (e.g. by registering
them in a RMI registry) would not scale. To scale, one should group
several active objects in a single container and access them by a
two-level reference system.  Then, preserving the semantics of Creol,
with one thread per object, is a question of local management of
threads and their interleaving.


% TO REUSE IN CONCLUSION
% In the end, object
% creation in different active object models should consider the
% location of objects: should objects be grouped? If yes how and under
% which control? Answering those questions helps defining and
% translating active object languages.


% Indeed, an object and its COG must always be located on the same memory space, since a COG should access its objects through local references. To send over the server object to the memory space of its COG, we use the \code{registerObject} method of the COG, giving \code{server} as parameter. As the reference to the COG is a remote reference, the \code{registerObject} call is a remote call. Consequently, the parameter of this call, the server object, will be copied away according to the ProActive object model, and all the objects created along with the server object will be copied too, if any. Then of course, to comply with the initial ABS program, all further method calls on \code{server} should be run on the remote server object, and not on the server object that was created locally. Nevertheless, we still need the local server object to be able to get back to the targeted remote server object, as we will see in the next section.







%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../ABStoPA"
%%% End: 
