\section{Introduction}\label{introduction}
Writing distributed and concurrent applications can be challenging. In
distributed environments, the absence of shared memory makes
information sharing more difficult. In concurrent environments,
information sharing is easy but might lead to inconsistent states if
shared variables are not manipulated with caution.  A set of languages
and tools have been developed to handle those two programming
challenges.  Among them, the active object programming model
\cite{ref:Lavender95activeobject} has shown to be convenient to handle
concurrency and distribution in object oriented languages.  The active
object model derives from the actor model
\cite{Agha:1997:FAC:969900.969901}. Active objects and actors share
common goals such as asynchronous communication, simplified
concurrency, and ease of distribution.  Yet, actors communicate through
message passing whereas active objects perform remote method
invocation.
Active objects decouple method calls from method execution, and by
this means enforce sequential processing on the target side. In
practice, this model is adapted to distribution because active
objects do not share memory and behave independently from each other.

There exist different programming languages implementing the active
object model. In Section~\ref{background} we will show that different
active object models provide various programming abstractions or
static guarantees. It seems difficult to decide whether one of the
existing solutions is better concerning those aspects. However, among
them, ProActive\footnote{http://proactive.inria.fr/}, a Java middleware
implementing multi-threaded active objects, provides the best support for deployment.
The main objective of this paper is somehow to reconcile the different
active object languages by translating the different paradigms into ProActive code. This
way, we can provide a platform to efficiently run programs made of
active objects on a large-scale distributed infrastructures.
We illustrate our approach by 
providing a ProActive backend for ABS, the active object language that
has the best support for specification and verification. 
ABS being radically different from ProActive, this illustrates the
generality of the solution.
Our contribution is in this sense threefold:
\begin{itemize}
\item we  analyse and classify the programming paradigms used in the different
  active object models.
\item we explain in details our strategy to translate those paradigms
  into ProActive, and present  the ProActive backend for ABS.
\item we provide a class-based semantics of multi-threaded active
  objects featured in ProActive and show the equivalence between ABS programs and
  their translation.
\end{itemize}
The paper is organised as follows. Section 2 reviews active object
languages and discusses motivations. Section 3 explains the principles
of the translation from ABS to ProActive. Section 4 introduces the
semantics of ProActive with multi-threaded active objects and the translational semantics.  Section 5
shows an experimental evaluation of the ProActive backend.



%  In
% the ASP language~\cite{ref:asp}, and in
% ProActive\footnote{http://proactive.inria.fr/} its Java
% implementation, strict sequential execution of requests is guaranteed
% per active object. In other implementations, like
% Creol~\cite{ref:creol}, requests might release temporarily the
% execution thread to allow another request to progress, in what is
% known as cooperative scheduling. The active object model has also been
% applied to object groups, for example in JCoBox~\cite{wrongjcobox} and
% ABS~\cite{ref:abs}. Specifically, ABS is an object-oriented language
% that targets modeling of distributed applications, but that does not
% run in a distributed way. It provides verification tools that help
% designing safe distributed and concurrent systems. The ABS Tool
% Suite\footnote{http://tools.hats-project.eu/} provides a frontend
% compiler and several backend translators into various programming
% languages. In particular, the Java backend translates ABS programs
% into Java programs. However, to our knowledge, there is no fully
% operational backend that offers the possibility to run the program in
% a distributed way.

% On the other hand, ProActive comes as a standard Java library and
% provides support for deploying active objects on clusters and
% grids. We present in this paper a solution to distribute ABS programs
% using the ProActive library. Starting from the initial Java backend,
% we propose the ProActive backend in order to produce a code that is
% easily deployable on thousands of machines. This backend uses what we
% call multi-active objects, i.e. multi-threaded active objects.  The ProActive backend
% makes it possible to experiment ABS programs in practical settings, in addition to verify them with the existing tools. 

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../ABStoPA"
%%% End: 
