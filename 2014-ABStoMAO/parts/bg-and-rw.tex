\section{Background}\label{background}

\subsection{Overview of actor and active object systems}\label{overview}

Many programming languages embed constructs to safely and gracefully
handle parallelism. The actor model \cite{Agha:1997:FAC:969900.969901}
was the first to schematically consider concurrent entities that could
evolve independently and communicate via asynchronous messages. The
actor model is implemented in modern languages such as Scala through
the Akka framework\footnote{http://scala-lang.org - http://akka.io}.
Later on, active objects were designed as the object oriented
counterpart of the actor model where communication is performed by
remote method calls, using futures for handling responses.  Futures first
appeared in the Multilisp language \cite{Halstead:1985:MLC:4472.4478}
and were formalised in \cite{Flanagan:1995:SFU:199448.199484}.  From
those seminal works, many active object languages were created and
implemented in modern platforms such as in
Android\footnote{http://source.android.com} to provide asynchronous
background services.


The principle of active objects is to associate a thread to an object, or to a set of
objects. We call \emph{activity} this notion: a thread and the objects
managed by this thread.  Two objects in different activities 
communicate by remote method invocation: when an object invokes a
method on an object in another activity, this creates a
\emph{request}; the invoker continues its execution while the invoked
object will serve the request in an asynchronous manner. Requests wait for execution in a \emph{request queue}. Like in any
object-oriented language, method invocations can return results. In
order to allow the invoker to continue its execution, a placeholder
for the expected result must be created. \emph{Futures} play this
role: a future is an empty object that will later be filled by the
result of a request. We say that a future is \emph{resolved} when its
value is known.
 Overall, the active object paradigm makes programming of distributed
 applications more natural, especially when applications are made of
computational entities that may have a state like objects, and that
execute in a decoupled manner because they are distributed.
While all active object models rely on the previously introduced notions, their
detailed characteristics vary in practice, which we present below:

\subsubsection{How are objects associated to activities?}
\label{sec:activity}
We distinguish three different ways to map objects to threads/activities: 
\begin{description}
\item \textit{Uniform Object Model.}  All objects are active objects
  with their own request queue and their own private execution thread.
  In consequence, all communications between objects occur by posting
  a request. This model is simple to formalise but its implementation
  is more difficult as it must rely on the creation of logical thread
  to allow scalability.
\item \textit{Non Uniform Object Model.}  Some of the objects are not
  active, in which case they are only accessible by a single active
  object, they are part of its state. 
An activity contains one active and several passive objects.
Non-uniform active object models
  are much more efficient as they require less communications and
  less concurrent threads than models where each object would be
  active. Reducing the number of activities also reduces the number of
  references that are globally accessible in the system, and thus enables the
  instantiation of a large number of objects.

\item \textit{Object Group Model.}
In this model, an activity is made of a set of objects sharing a
request queue and an execution thread, but all
objects can be invoked from another activity. This
approach improves scalability as it reduces the number of threads, but
it is still difficult to create a lot of objects as all of them must
be registered so that they are accessible from any other activity.
%In the ABS case, only one kind of object exists and all objects are directly accessible from any group.
\end{description}


\subsubsection{How are requests scheduled?}
We distinguish three  threading models in active object languages.
\begin{description}
\item \textit{Single-threaded.} 
Within an active object, requests are executed sequentially without
interleaving possibilities. % This is the threading model of ProActive active objects if no annotation for multiactivity is found.
\item \textit{Cooperative scheduling.}
A running request can explicitly release the execution thread to let
another request run. Requests are not processed in parallel but they
might interleave. Data-races are avoided. % Creol, JCoBox, and ABS comply to this threading model.
\item \textit{Multi-threaded.}
Within an active object, requests are executed in parallel using
different threads, without pausing nor yielding. % This is the threading model of ProActive with multiactive objects.
Some data-races inside the activity is possible. This is
called a \emph{multiactive object} model.
\end{description}


\subsubsection{Is the programmer aware of distributed aspects?}
Some active object languages use a specific syntax for asynchronous
method calls: a different operator is used to distinguish them from
synchronous method calls. This makes the programmer aware of the
places where futures are created. Generally, when asynchronous
invocation is explicit, there exists a special type for future objects.
Additionally, the futures, when they are statically identified, can be
accessed explicitly or implicitly. In case of explicit access,
operations like \emph{claim}, \emph{get} and \emph{touch} are used to
access the future. This is particularly convenient for releasing the
current thread if a future is not available in case of cooperative
scheduling (\emph{await} statement). For implicit access, operations that need the real
value of an object (\emph{blocking} operations) automatically trigger
synchronisation with the future update operation.
Implicit future creation allows transparency of distributed aspects:
there is almost no difference between a distributed program and usual
objects, whereas explicit manipulation of futures allows the
programmer to better control its execution, but requires from him a
better expertise. We say that futures
are \emph{first class} if future references can be transmitted between
remote entities without requiring the future to be resolved.

% Among them,
% AmbientTalk \cite{Dedecker:2006:APA:2171327.2171349} makes heavy use
% of futures; ASP \cite{ref:asp} enforces sequential request processing
% and waits by necessity for future values; Creol \cite{ref:creol}
% unifies objects with the active object model; JCobox
% \cite{wrongjcobox} and later on, ABS \cite{ref:abs}, balanced the
% existing approaches. 
We present below the main active object languages, described accordingly to the
classification given above.




\subsubsection{Creol.}
Creol \cite{ref:creol} is a uniform active object language that
features cooperative scheduling based on \code{await} operations (that might
release the active thread).  Asynchronous invocations and futures are
explicit, but there is no first-class future.  De Boer et al.
\cite{SDE:BoerCJ07} provide the semantics of a
language based on Creol.
In Creol, like in all cooperative scheduling languages,
while no data race condition is possible, interleaving of the
different request services triggered by the different release points
makes the behaviour more difficult to predict.
Overall, explicit future access,
explicit release points, and explicit asynchronous method calls make the Creol
programming model rich and precise but also more difficult to program
than the models featuring more transparency.

 

\subsubsection{JCoBox.}
JCoBox \cite{ref:jcobox} is an active object programming model
implemented in a language based on Java. It has an object group model
based on coboxes and features cooperative scheduling.  Similarly to
Creol, in each cobox a single thread is active at a time, but this
thread can be released thanks to the \code{await()} call. The practical implementation and the group model made
the language more applicable than Creol. For the other aspects,
it is similar and has the same advantages and drawbacks as Creol.

\subsubsection{ABS.}\label{abs}
ABS~\cite{ref:abs} is an object-oriented language that targets
modelling of distributed applications. ABS has an object group model
based on the notion of concurrent object
groups (\COGs). Asynchronous method calls and futures are also explicit, and
there is not yet support for first-class futures.
 The
following example is an asynchronous call that returns a future of
parametric type \code{V}: \lstset{language=java, numberstyle=\tiny,
  stepnumber=1, numbersep=2pt, basicstyle=\ttfamily\scriptsize}
\begin{lstlisting}
Fut<V> future = object!method();
\end{lstlisting}
Figure~\ref{fig:cog} shows an ABS configuration.
\begin{figure}[tb]
\centering
\includegraphics[scale=0.3]{pictures/cogs2.pdf}\vspace{-2ex}
\caption{An example of ABS program execution with two \COGs and six objects}\vspace{-2ex}
\label{fig:cog}
\end{figure}
The requests are scheduled in a cooperative manner thanks to the \code{await} keyword; \code{await}  can be followed by a future or by a condition as in the two examples:
\lstset{language=java, numberstyle=\tiny, stepnumber=1, numbersep=2pt, basicstyle=\ttfamily\scriptsize}
\begin{lstlisting}
await future?;  	// case #1
await a > 2 && b < 3;   // case #2
\end{lstlisting}
Those examples cause the execution thread to yield if (\code{case \#1}) the future is not resolved or if (\code{case \#2}) the condition not fulfilled. 
%In those cases, the execution thread is handed over to another ready request. 
%A paused request is ready to resume when the future is resolved (in \code{case ~1}) or when the condition is fulfilled (in \code{case ~2}).
The \code{get} accessor can be used on a future variable to retrieve
the future's value; it blocks the execution thread until the future
is resolved: 
\lstset{language=java, numberstyle=\tiny, stepnumber=1, numbersep=2pt, basicstyle=\ttfamily\scriptsize}
\begin{lstlisting}
V v = future.get;
\end{lstlisting}	
%The \code{get} accessor  % (and does not release the execution thread until then).
The ABS Tool
Suite\footnote{http://tools.hats-project.eu/} provides a wide variety of static verification engines that help
designing safe distributed and concurrent systems. Those engines
include deadlock analysis \cite{CGM:SoSym2014} and resource and
cost analysis 
\cite{SACO:TACAS14}.
ABS tools also include a frontend
compiler and several backend translators into various programming
languages. 
The Java backend produces Java code from ABS. Such Java programs run
in a single memory space without support for distributed execution.
%, neither in the ABS language itself nor in any of the existing backend.




\subsubsection{AmbientTalk.}
The most original aspect of AmbientTalk\cite{Dedecker:2006:APA:2171327.2171349} is
that future access is a non-blocking operation, it is an asynchronous
call that returns another future.  There is no wait-by-necessity upon
a method call on an unresolved future, instead the method call will be performed
when the future becomes available: in the meantime, another future represents
the result of this method invocation.  This approach avoids the
possibility of a deadlock as there is no synchronisation, but
programming can be tricky as there is no way to synchronise two
processes or to infer whether a computation has finished.






%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../ABStoPA"
%%% End: 
